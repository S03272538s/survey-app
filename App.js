// =======================================================================
// Init/State Componant
// =======================================================================

import React, { useState, useEffect } from 'react';
import moment from 'moment';
// import { Platform } from 'react-native';
import FormLogin from './src/login-out/funcFormLogin';
import CameraCheckIn from './src/component/checkIn/funcCameraCheckIn'
import NavigationMain from './src/component/funcNavigationsMain';
import Tutorial from './src/component/tutorialApp/funcTutorial';
import ModalAlertLogin from './src/login-out/funcModalAlertLogin';
import Constants from 'expo-constants';
import ViewFeedDetail from './src/component/home/viewFeedDetail';
import UploadFile from './src/component/uploadFile/funcUploadFile';

// =======================================================================
// Navigations
// =======================================================================

import { CommonActions } from '@react-navigation/native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
const RootNavigationStack = createStackNavigator();

// =======================================================================
// Repository Data
// =======================================================================

import { userLogin } from './src/repository/funcPostData';
import { getCurrentPosition } from './src/repository/funcCurrentPosition';
import { getData } from './src/repository/funcGetData';

// =======================================================================
// SQLite Base On Device
// =======================================================================

import * as SQLite from 'expo-sqlite';
import {
  addSession,
  updateSession,
  addSessionChekIn,
  updateSessionChekIn,
  addSessionDataOnDevice,
  updateSessionDataOnDevice
} from './src/repository/funcSQLite';

const db = SQLite.openDatabase("Survey.db");
db.transaction(tx => {
  tx.executeSql(
    'CREATE TABLE IF NOT EXISTS SurveyLogStatus (id integer primary key not null,username ,password ,name ,id_application, statusLogin, statusTutorial, versionApp)'
  );
  tx.executeSql(
    'CREATE TABLE IF NOT EXISTS DataChkIn (id integer primary key not null, name, id_application, statusChekIn, date)'
  );
  tx.executeSql(
    'CREATE TABLE IF NOT EXISTS dataDevice (id integer primary key not null, version_app)'
  );
});

// =======================================================================
//  Rendering App Index Page
// =======================================================================

function LoginScreen({ navigation, route }) {

  const [txtVer, setTxtVer] = useState();
  const [data, setData] = useState();
  const [dataUser, setDataUser] = useState();
  const [showPage, setShowPage] = useState(false);
  const [statusVisible, setStatusVisible] = useState(false);
  const [currentPosition, setCurrentPosition] = useState({ latitude: 0, longitude: 0, });
  const [dataLogin, setDataLogin] = useState({ username: '', password: '', remember: false });
  const dataChekIn = {
    id_application: '',
    name: '',
    statusCheck: false,
    date: moment(new Date).format('YYYY-MM-DD')
  }

  const dataDevice = {
    version: Constants.__unsafeNoWarnManifest.version,
  }
  // Get Data On Base Device
  const fetchData = () => {
    db.transaction(tx => {
      tx.executeSql('SELECT * FROM SurveyLogStatus', null,
        (tx, { rows: { _array } }) => {
          setData(_array[0]);
          if (_array[0] !== undefined) {
            if (_array[0].statusLogin !== 0 || _array[0].statusLogin !== undefined) {
              setShowPage(_array[0].statusLogin);
              setDataUser({
                name: _array[0].name,
                statusLogin: _array[0].statusLogin,
                statusTutorial: _array[0].statusTutorial,
                id_application: _array[0].id_application,
                username:  _array[0].username,
                password:  _array[0].password
              });
            }
          }
        }, (tx, error) => console.warn('Error :: ', error)
      )
      tx.executeSql('SELECT * FROM DataChkIn', null,
        (tx, { rows: { _array } }) => {
          if (_array[0] === undefined) {
            addSessionChekIn(dataChekIn);
          }
        }, (tx, error) => console.warn('Error :: ', error)
      )

      tx.executeSql('SELECT * FROM dataDevice', null,
        (tx, { rows: { _array } }) => {
          if (_array[0] === undefined) {
            addSessionDataOnDevice(dataDevice);
          } else {
            setTxtVer(_array[0].version_app);
          }

        }, (tx, error) => console.warn('Error :: ', error)
      )
    })
    // chkversion();
  }
  
  const chkversion = () => {
    if (txtVer) {
      if (txtVer != Constants.__unsafeNoWarnManifest.version) {
        const dataChekInUpdate = {
          id: 1,
          id_application: '',
          name: '',
          statusCheck: false,
          date: moment(new Date).format('YYYY-MM-DD')
        }
        const dataDeviceUpdate = {
          id: 1,
          version: Constants.__unsafeNoWarnManifest.version,
        }
        updateSessionChekIn(dataChekInUpdate);
        updateSessionDataOnDevice(dataDeviceUpdate);
      }
    }
  }

  useEffect(() => {
    fetchData();
    getPosition();
  }, []);

  const getPosition = async () => {
    const callBackCurrentPosition = await getCurrentPosition();
    setCurrentPosition(callBackCurrentPosition);
  }
  // Chack Login
  const onLogin = async (user, pass) => {

    dataLogin.username = user;
    dataLogin.password = pass;
    setDataLogin({ ...dataLogin });
    const postLogin = await userLogin(dataLogin);
    if (postLogin.status_login == true) {
      setShowPage(postLogin.status_login);
      setDataUser({ 
        name: postLogin.name, 
        id_application: postLogin.id_application,
        username:  user,
        password:  pass
      });

      if (data) {
        if (data.statusTutorial == 1) {
          postLogin.statusTutorial = 1;
        }
        updateSession(data.id, postLogin, dataLogin)
        // updateSessionChekIn( dataChekIn )
      } else {
        addSession(postLogin, dataLogin)
        // addSessionChekIn(dataChekIn)
      }
    } else { setStatusVisible(true, { ...statusVisible }) }
  }

  const handleOnClickMapCallback = () => { setStatusVisible(false) }

  const handleClickLogout = async (data) => {
    setShowPage(false, { ...statusVisible })
    navigation.dispatch(
      CommonActions.reset({
        index: 1,
        routes: [{
          name: 'Login',
        }],
      })
    )
  }
  const handleClickFinishTuto = async () => {
    setShowPage(true, {...statusVisible })
    navigation.dispatch(
      CommonActions.reset({
        index: 1,
        routes: [{
          name: 'Login',
        }],
      })
    )
  }
  return (
    <>
      {showPage == true ?
        data !== undefined && dataUser !== undefined && 
          data.statusTutorial == 1 && txtVer!== undefined ?
          <NavigationMain dataUser={dataUser} currentPosition={currentPosition} handleClickLogout={handleClickLogout} version={txtVer} />
          :
          <Tutorial dataUser={dataUser} handleClickLogout={handleClickLogout} dataLogin={dataLogin} handleClickFinishTuto={handleClickFinishTuto} />
        :
        <FormLogin onLogin={onLogin} />
      }
      <ModalAlertLogin logAlert={'ข้อมูลไม่ถูกต้อง'} statusVisible={statusVisible} toggle={handleOnClickMapCallback} />
    </>
  );
}
// =======================================================================
//  Rendering App Index Page
// =======================================================================

export default function App() {
  return (
    <NavigationContainer>
      <RootNavigationStack.Navigator screenOptions={{
        headerShown: false
      }}>
        <RootNavigationStack.Screen name="Login" component={LoginScreen} />
        <RootNavigationStack.Screen
          name="FeedDetail"
        >
          {(props) => <ViewFeedDetail navigation={props.navigation} param={props.route.params} />}
        </RootNavigationStack.Screen>
        <RootNavigationStack.Screen
          name="CameraCheckInPage"
        >
          {(props) => <CameraScreen navigation={props.navigation} />}
        </RootNavigationStack.Screen>
        <RootNavigationStack.Screen
          name="UploadFile"
        >
          {(props) => <UploadFile navigation={props.navigation} param={props.route.params}/>}
        </RootNavigationStack.Screen>
      </RootNavigationStack.Navigator>
    </NavigationContainer>

  );
};

// =======================================================================
//  Rendering CameraScreen Page
// =======================================================================

function CameraScreen({ navigation }) { return <CameraCheckIn navigation={navigation} /> }