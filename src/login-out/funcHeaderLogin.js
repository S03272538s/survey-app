// =======================================================================
// Init/State Componant
// =======================================================================

import React from 'react';
import { Image } from 'react-native';
import { StyleContain } from '../component/style/funcStyle';
const style = StyleContain();

// =======================================================================
// Render Header Login
// =======================================================================

export default function HeaderLogin(){
    return(
        <>
            <Image
                style={style.logoLogin}
                source={{ uri: 'http://1.2.175.220/public/img_default_mobile/logo-login.png' }}
            />
        </>
    );
};