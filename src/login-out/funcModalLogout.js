// =======================================================================
// Init/State Componant
// =======================================================================

import React from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    TouchableWithoutFeedback 
} from 'react-native';
import Modal from 'react-native-modal';
import { Button } from 'react-native-paper';
import { StyleContain } from '../component/style/funcStyle';
const styles = StyleContain();

function ModalAlertLogout({ logTxt, statusVisible, toggle, funcCallBack }) {
    return (
        <Modal
            style={{ alignItems: 'center' }}
            isVisible={statusVisible}
            animationType="slide"
            animationInTiming={500}
            animationOutTiming={500}
            backdropOpacity={0.5}
            propagateSwipe={true}
            customBackdrop={
                <TouchableWithoutFeedback onPress={() => toggle(false)}>
                    <View style={{ flex: 1, backgroundColor: '#000' }} />
                </TouchableWithoutFeedback>
            }
        >
            <View style={styles.modalAlretView}>
            <View style={styles.modalAlretContant}>
                    <Text style={styles.modalAlretTxt}>{logTxt}</Text>
                </View>
                <View style={styles.viewTwinBtnAlret}>
                    <Button
                        mode="outlined"
                        style={styles.btnTwinAlretStyle}
                        onPress={() => toggle(false)}
                        color="#ff4a4a"
                    >
                        <Text style={styles.btnAlretTxt}>ยกเลิก</Text>
                    </Button>
                    <Button
                        mode="outlined"
                        style={styles.btnTwinAlretStyle}
                        onPress={() => toggle(true)}
                        color="#3ae062"
                    >
                        <Text style={style.btnAlretTxt}>ยืนยัน</Text>
                    </Button>
                </View>
            </View>
        </Modal>
    )
}
export default ModalAlertLogout;

const style = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        paddingBottom: 50,
        width: '70%'
    },
    modalView: {
        backgroundColor: "white",
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        justifyContent: 'center',
        height: 70,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
    },
    button: {
        borderRadius: 20,
        elevation: 2
    },
    textStyleModal: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center",
    },
    modalText: {
        textAlign: "center"
    },
    btn_txt: {

        fontWeight: "bold",
        textAlign: "center"
    },
    btn_footer: {
        flexDirection: 'row',
        backgroundColor: '#FFF',
        height: 50,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,

    },
    background: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        height: 2,
    },
    btn_alert: {
        height: 500,
        borderRadius: 50
    }
});