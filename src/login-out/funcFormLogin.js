// =======================================================================
// Init/State Componant
// =======================================================================

import React, { useState } from 'react';
import HeaderLogin from './funcHeaderLogin';
import { LinearGradient } from 'expo-linear-gradient';
import { Button, TextInput, Card } from 'react-native-paper';
import { ScrollView, View, StyleSheet, Text } from 'react-native';
import Constants from 'expo-constants';
import { StyleContain } from '../component/style/funcStyle';
const style = StyleContain();

// =======================================================================
// Init/State Componant
// =======================================================================

export default function FormLogin({ onLogin }) {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    return (
        <ScrollView style={style.viewLoginContainer}>
            <View style={style.viewLoginHeader}>
                <HeaderLogin />
            </View>
            <LinearGradient
                colors={style.bgLoginLinearColor}
                style={style.bgLoginLinearStyle}
            />
            <View style={style.viewContainer}>
                <Card style={style.viewCardLogin}>
                    <Card.Content>
                        <TextInput
                            theme={style.styleInput}
                            style={style.inputStyle}
                            label="ชื่อผู้ใช้งาน"
                            value={username}
                            onChangeText={(username) => setUsername(username)}
                        />
                        <TextInput
                             theme={style.styleInput}
                             style={style.inputStyle}
                            label="รหัสผ่าน"
                            value={password}
                            onChangeText={(password) => setPassword(password)}
                            secureTextEntry={true}
                        />
                    </Card.Content>
                </Card>
                <LinearGradient
                    colors={style.bgBtnLoginLinearColor}
                    style={style.btnLogin}>
                    <View style={style.viewBtnLoginStyle}>
                        <Button
                            mode="outlined"
                            style={style.btnLoginStyle}
                            onPress={() => onLogin(username, password)}
                            color="#4c669f"
                        >
                            <Text style={style.btnTxtLoginStyle} onPress={() => onLogin(username, password)} >เข้าสู่ระบบ</Text>
                        </Button>
                    </View>
                </LinearGradient>
            </View>
            <View style={style.viewTxtFooterLogin}>
                <Text style={style.txtFooterLogin}> © MISTERCOFFEESHOP..COM ALL RIGHTS RESERVED. Ver.{Constants.manifest.version} </Text>
            </View>
        </ScrollView>
    );
}
