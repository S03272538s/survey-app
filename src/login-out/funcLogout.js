// =======================================================================
// Init/State Componant
// =======================================================================

import React, { useState, useEffect } from 'react';
import { Text, View, TouchableHighlight } from 'react-native';
import ModalLogout from './funcModalLogout';
import { LogoutSession } from '../repository/funcSQLite';
import { StyleContain } from '../component/style/funcStyle';
import { CommonActions } from '@react-navigation/native';
import { MaterialCommunityIcons } from '@expo/vector-icons';

const style = StyleContain();

// =======================================================================
// Render Button Logout
// =======================================================================

export default function Logout({ navigation, dataUser , showModal }) {
    const [statusVisible, setStatusVisible] = useState(showModal);

    const chkStatusLogout = () => {
        setStatusVisible(true, { ...statusVisible })
    }

    useEffect(() => {
        chkStatusLogout();
    }, [])
    const statusLogout = (status) => {
        setStatusVisible(false, { ...statusVisible });
        if (status === true) {
            LogoutSession(dataUser.id_application);
            handleClickLogout();
        }
        if(status === false){
            navigation.goBack()
        }
    }
    console.log(statusVisible);
    const handleClickLogout = async (data) => {
        navigation.dispatch(
            CommonActions.reset({
                index: 1,
                routes: [{
                    name: 'Login',
                }],
            })
        )
    }

    return (
        <>  
            <TouchableHighlight
                underlayColor={'#8a8a8a'}
                onPress={() => { chkStatusLogout() }}
                style={{ marginLeft: 20, marginRight: 20, marginBottom: 20, backgroundColor:'red'}}
            >
                <View style={{ flex: 1, alignItems: 'flex-end', height: 40, flexDirection: 'row' }}>
                    <MaterialCommunityIcons name="logout" size={24} color="#292929" style={{ paddingRight: 20 }} />
                    <Text style={style.txtTitleContent}>
                        ออกจากระบบ
                    </Text>
                </View>
            </TouchableHighlight>

            <ModalLogout
                logTxt='คุณต้องการออกจากระบบ หรือไม่'
                statusVisible={statusVisible}
                toggle={statusLogout}
            />
        </>
    );
};