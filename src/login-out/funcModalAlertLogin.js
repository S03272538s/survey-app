// =======================================================================
// Init/State Componant
// =======================================================================

import React from 'react';
import {
    View,
    Text,
    TouchableWithoutFeedback
} from 'react-native';
import Modal from 'react-native-modal';
import { Button } from 'react-native-paper';
import { StyleContain } from '../component/style/funcStyle';
const styles = StyleContain();

// =======================================================================
// Render Modal Alert Login 
// =======================================================================

export default function ModalAlertLogin({ logAlert, statusVisible, toggle }) {
    return (
        <Modal
            style={{ alignItems: 'center' }}
            isVisible={statusVisible}
            animationType="slide"
            animationInTiming={500}
            animationOutTiming={500}
            backdropOpacity={0.5}
            propagateSwipe={true}
            customBackdrop={
                <TouchableWithoutFeedback onPress={toggle}>
                    <View style={{ flex: 1, backgroundColor: '#000' }} />
                </TouchableWithoutFeedback>
            }
        >
            <View style={styles.modalAlretView}>
                <View style={styles.modalAlretContant}>
                    <Text style={styles.modalAlretTxt}>{logAlert}</Text>
                </View>
                <View style={styles.viewBtnAlret}>
                    <Button
                        mode="outlined"
                        style={styles.btnAlretStyle}
                        onPress={toggle}
                        color="#ff4a4a"
                    >
                        <Text style={styles.btnAlretTxt}>ยืนยัน</Text>
                    </Button>
                </View>

            </View>
        </Modal>
    )
}