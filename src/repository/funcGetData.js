// =======================================================================
// Init/State Componant
// =======================================================================

import Ajax from "../api-call/ajax.js";
const ajax = new Ajax();
ajax.url = "http://1.2.175.220/Survey/";

// =======================================================================
// Get Data Form API Ajax
// =======================================================================

export async function getData(mode, id) {
    let arrData = []
    const getData = await new Promise((r, j) => { ajax.get(mode, id, r) });
    const callBackDataApi = await getData;
    if (callBackDataApi === false) {
        return false;
    }
    callBackDataApi.forEach((element) => {
        arrData.push(element);
    });
    return arrData;
};

export async function getPosition(dataUser, mode, id) {
    if (dataUser !== undefined) {
        const getDetailPosition = await new Promise((r, j) => { ajax.get(mode, dataUser.id_application, r) });
        const callBackPosition = await getDetailPosition;
        return callBackPosition[0];
    }
};
export async function getAccessRight(dataUser, mode, id) {
    var accessRight = '';
    const getData = await new Promise((r, j) => { ajax.get(mode, id, r) });
    const callBackDataApi = await getData;
    if (dataUser) {
        callBackDataApi.forEach((element) => {
            if (element.id_application == dataUser.id_application) {
                accessRight = false;
            } else {
                accessRight = true;
            }
        });
    }
    return accessRight;
};

export async function getTimeline(dataUser, mode, id) {
    const getPosi = await getPosition(dataUser, 'getDetailEmp', '')
    const getDetailPosition = await new Promise((r, j) => { ajax.get(mode, getPosi.id_position, r) });
    const callBackPosition = await getDetailPosition;
    const jsonTimeline = await JSON.parse(callBackPosition[0].periodTime);
    let mon = [], tue = [], wed = [], thu = [], fri = [], sat = [], sun = [];
    for (let index = 1; index <= 10; index++) {
        await jsonTimeline.forEach((weekTimeline, idx) => {
            if (weekTimeline.id === 'time_' + index + '_1') {
                mon.push({ time: weekTimeline.time.substring(0, 5), description: weekTimeline.value, title: 'จ. TimeLine : ' + index });
            }
            if (weekTimeline.id === 'time_' + index + '_2') {
                tue.push({ time: weekTimeline.time.substring(0, 5), description: weekTimeline.value, title: 'อ. TimeLine : ' + index });
            }
            if (weekTimeline.id === 'time_' + index + '_3') {
                wed.push({ time: weekTimeline.time.substring(0, 5), description: weekTimeline.value, title: 'พ. TimeLine : ' + index });
            }
            if (weekTimeline.id === 'time_' + index + '_4') {
                thu.push({ time: weekTimeline.time.substring(0, 5), description: weekTimeline.value, title: 'พฤ. TimeLine : ' + index });
            }
            if (weekTimeline.id === 'time_' + index + '_5') {
                fri.push({ time: weekTimeline.time.substring(0, 5), description: weekTimeline.value, title: 'ศ. TimeLine : ' + index });
            }
            if (weekTimeline.id === 'time_' + index + '_6') {
                sat.push({ time: weekTimeline.time.substring(0, 5), description: weekTimeline.value, title: 'ส. TimeLine : ' + index });
            }
            if (weekTimeline.id === 'time_' + index + '_7') {
                sun.push({ time: weekTimeline.time.substring(0, 5), description: weekTimeline.value, title: 'อา. TimeLine : ' + index });
            }
        });
    }
    mon.push({ time: '18:00' });
    tue.push({ time: '18:00' });
    wed.push({ time: '18:00' });
    thu.push({ time: '18:00' });
    fri.push({ time: '18:00' });
    sat.push({ time: '18:00' });
    sun.push({ time: '18:00' });
    const weekly = [mon, tue, wed, thu, fri, sat, sun];
    return weekly;
};

export async function getAccessRightDev(dataUser, mode, id) {
    var accessRight = [];
    const getData = await new Promise((r, j) => { ajax.get(mode, id, r) });
    const callBackDataApi = await getData;
    if (dataUser) {
        callBackDataApi.forEach((element) => {
            if (element.id_application == dataUser.id_application) {
                if (element.id_positions === '124') {
                    accessRight = false;
                }
            } else {
                accessRight = true;
            }
        });
    }
    return accessRight;
};

export async function getAccessJobWeek(dataUser, mode) {
    const getPosi = await getPosition(dataUser, 'getDetailEmp', '');
    const getAccess = await new Promise((r, j) => { ajax.get(mode, getPosi.id_position, r) });
    return await getAccess[0].accessRightWeekTable === '1' ? true : false;
};
export async function getLeave(dataUser, mode) {
    var timeLeave = [];
    // const getPosi = await getPosition(dataUser, 'getLeave', '');
    const getAccess = await new Promise((r, j) => { ajax.get(mode, dataUser.id_application, r) });
    const callBack = await getAccess;
    const getLeaveType = await new Promise((r, j) => { ajax.get('getLeaveType', '', r) });
    callBack.forEach(element => {
        timeLeave.push({
            chk_app: element.chk_app,
            chk_cancel: element.chk_cancel,
            chk_qc: element.chk_qc,
            comment_app: element.comment_app,
            comment_cancel: element.comment_cancel,
            id: element.id,
            lbl_name_app: element.lbl_name_app,
            lbl_name_cancel: element.lbl_name_cancel,
            leave_date_from1: element.leave_date_from1,
            leave_date_from2: element.leave_date_from2,
            leave_remark: element.leave_remark,
            leave_requestor: element.leave_requestor,
            leave_requestorTxt: element.leave_requestorTxt,
            leave_type: element.leave_type,
            leave_typeTxt: element.leave_typeTxt,
            total_leave: JSON.parse(element.total_leave) / 60
        });
    });
    return timeLeave;
}
export async function getLeaveType(mode) {
    const getLeaveType = await new Promise((r, j) => { ajax.get(mode, '', r) });
    return await getLeaveType;
   
};