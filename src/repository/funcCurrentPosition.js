// =======================================================================
// Init/State Componant
// =======================================================================
import React, { useState, useEffect } from 'react';
import * as Location from 'expo-location';

export async function getCurrentPosition(){
    let { status } = await Location.requestForegroundPermissionsAsync();
    if (status !== 'granted') {
        console.warn('Fail :: Permission to access location was denied');
        return;
    }
    let location = await Location.getCurrentPositionAsync({});
    const currentPosition = {
        latitude: location.coords.latitude,
        longitude: location.coords.longitude,
    }
    return currentPosition;
}  
  
  
