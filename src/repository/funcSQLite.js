// =======================================================================
// Init/State Componant
// =======================================================================
import * as SQLite from 'expo-sqlite';
let db = SQLite.openDatabase('Survey.db');

// =======================================================================
// Inserts a Data Into The Database On Device
// =======================================================================
export const addSession = (dataUser, dataLogin) => {
  db.transaction(tx => {
    tx.executeSql(
      'INSERT INTO SurveyLogStatus (username ,password, name, id_application, statusLogin, statusTutorial) VALUES (?,?,?,?,?,?)',
        [dataLogin.username,dataLogin.password, dataUser.name, dataUser.id_application, dataUser.status_login, false], 
        insert()
    );
  });
};
const insert = () => {
  db.transaction(tx => {
    tx.executeSql(
      'SELECT * from SurveyLogStatus', [],
      (tx, resultSet) => {
        // setNames(() => {
          let retRA = [];
          resultSet.rows._array.forEach(element => {
            retRA.unshift({ 
              id: element.id, 
              username: element.username,
              password: element.password,
              name: element.name,
              id_application: element.id_application,
              statusLogin: element.statusLogin,
              statusTutorial: element.statusTutorial
            });
          });
          return retRA;
        // });
      },
      (tx, error) => {
        console.log('ERROR :: ' + error);
      }
    );
  });
};

export const addSessionChekIn = (dataChkIn) => {
  console.log(dataChkIn);
  db.transaction(tx => {
    tx.executeSql(
      'INSERT INTO DataChkIn (name, id_application, statusChekIn, date) VALUES (?,?,?,?)',
        [dataChkIn.name, dataChkIn.id_application, dataChkIn.statusCheck, dataChkIn.date], 
        insertChekIn()
    );
  });
};
const insertChekIn = () => {
  db.transaction(tx => {
    tx.executeSql(
      'SELECT * from DataChkIn', [],
      (tx, resultSet) => {
        // setNames(() => {
          let retRA = [];
          console.log(resultSet);
          resultSet.rows._array.forEach(element => {
            retRA.unshift({ 
              id: element.id, 
              name: element.name,
              id_application: element.id_application,
              statusChekIn: element.statusCheck,
              date:element.date
            });
          });
          return retRA;
        // });
      },
      (tx, error) => {
        console.log('ERROR :: ' + error);
      }
    );
  });
};

export const addSessionDataOnDevice = (dataDevice) => {
  db.transaction(tx => {
    tx.executeSql(
      'INSERT INTO dataDevice (version_app) VALUES (?)',
        [dataDevice.version], 
        insertDataOnDevice()
    );
  });
};
const insertDataOnDevice = () => {
  db.transaction(tx => {
    tx.executeSql(
      'SELECT * from dataDevice', [],
      (tx, resultSet) => {
        // setNames(() => {
          let retRA = [];
          console.log(resultSet);
          resultSet.rows._array.forEach(element => {
            retRA.unshift({ 
              id: element.id, 
              version: element.version,
            });
          });
          return retRA;
        // });
      },
      (tx, error) => {
        console.log('ERROR :: ' + error);
      }
    );
  });
};

// =======================================================================
// Update a Data Into The Database On Device
// =======================================================================

export const updateSession = (id,data,dataLogin) => {
  if(data.statusTutorial == undefined){
    data.statusTutorial = 0;
  }
  db.transaction(tx => {
    tx.executeSql(
      `UPDATE SurveyLogStatus SET 
      username = "`+ dataLogin.username + `",
      password = "`+ dataLogin.password + `",
      name = "`+ data.name + `" ,
      id_application = "`+ data.id_application + `" ,
      statusLogin = 1 ,
      statusTutorial = "`+ data.statusTutorial + `"
      WHERE id = ?`,
      [id], 
      update(id));

  })
}
const update = (oldData) => {
  let arrDataRow =[];
  arrDataRow.push(oldData);
  db.transaction(tx => {
    tx.executeSql(
      `SELECT * from SurveyLogStatus`, [],
      (tx, { rows: { _array } }) => {
        let newList = arrDataRow.map(DataRow => {
          if (DataRow.id === _array[0].id)
            return { ...DataRow}
          else
            return _array[0]
        })
        // setNames(() => {
          let retRA = [];
          newList.forEach(elem => {
            retRA.shift(elem);
          });
          return retRA;
        // });
      }
    );
  });
};

export const updateSessionChekIn = (dataCheckIn) => {
  
  if(dataCheckIn.statusChekIn == undefined){
    dataCheckIn.statusChekIn = false;
  }
  db.transaction(tx => {
    tx.executeSql(
      `UPDATE DataChkIn SET 
      name = "`+ dataCheckIn.name + `" ,
      id_application = "`+ dataCheckIn.id_application + `" ,
      statusChekIn = "`+ dataCheckIn.statusChekIn + `",
      date ="` + dataCheckIn.date + `"
      WHERE id = ?`,
      [dataCheckIn.id], 
      updateChekIn(dataCheckIn.id));
  })
}
const updateChekIn = (oldData) => {
  let arrDataRow =[];
  arrDataRow.push(oldData);
  db.transaction(tx => {
    tx.executeSql(
      `SELECT * from DataChkIn`, [],
      (tx, { rows: { _array } }) => {
        let newList = arrDataRow.map(DataRow => {
          if (DataRow.id === _array[0].id)
            return { ...DataRow}
          else
            return _array[0]
        })
        // setNames(() => {
          let retRA = [];
          newList.forEach(elem => {
            retRA.shift(elem);
          });
          return retRA;
        // });
      }
    );
  });
};

export const updateSessionDataOnDevice = (dataDevice) => {
 console.log(dataDevice);
  db.transaction(tx => {
    tx.executeSql(
      `UPDATE dataDevice SET 
      version_app = "`+ dataDevice.version + `"
      WHERE id = ?`,
      [dataDevice.id], 
      updateDataOnDevice(dataDevice.id));
  })
}

const updateDataOnDevice = (oldData) => {
  let arrDataRow =[];
  arrDataRow.push(oldData);
  db.transaction(tx => {
    tx.executeSql(
      `SELECT * from dataDevice`, [],
      (tx, { rows: { _array } }) => {
        let newList = arrDataRow.map(DataRow => {
          if (DataRow.id === _array[0].id){
            return { ...DataRow}
          }
          else
            return _array[0]
        })
        // setNames(() => {
          let retRA = [];
          newList.forEach(elem => {
            retRA.shift(elem);
          });
          return retRA;
        // });
      }
    );
  });
};

// =======================================================================
// Update StatusLogin a Data Into The Database On Device
// =======================================================================

export const LogoutSession = (id) => {
  db.transaction(tx => {
    tx.executeSql('UPDATE SurveyLogStatus SET statusLogin = 0 WHERE id_application = ?', [id], updateLogout(id));
  })
}
const updateLogout = (oldData) => {
  let arrDataRow =[];
  arrDataRow.push(oldData);
  db.transaction(tx => {
    tx.executeSql(
      `SELECT * from SurveyLogStatus`, [],
      (tx, { rows: { _array } }) => {
        let newList = arrDataRow.map(DataRow => {
          if (DataRow.id === _array[0].id)
            return { ...DataRow}
          else
            return _array[0]
        })
        // setNames(() => {
          let retRA = [];
          newList.forEach(elem => {
            retRA.shift(elem);
          });
          return retRA;
        // });
      }
    );
  });
}