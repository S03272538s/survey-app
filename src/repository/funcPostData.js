// =======================================================================
// Init/State Componant
// =======================================================================

import moment from 'moment';
import { getData } from './funcGetData.js';
import Ajax from "../api-call/ajax.js";
const ajax = new Ajax();
ajax.url = "http://1.2.175.220/Survey/";

// =======================================================================
// Post Form Login
// =======================================================================

export async function userLogin(dataLogin) {
    const postDataLogin = await new Promise((r, j) => { ajax.postLogin(dataLogin, 'login', '', r) });
    const statusLogin = await postDataLogin;
    return statusLogin[0];
};
// =======================================================================
// Post Form ResetPassword
// =======================================================================

export async function rePassword(data) {
    const postData = await new Promise((r, j) => { ajax.set(data, 'rePassword', '', r) });
    const statusRePassword = await postData;
    // const postDataLogin = await new Promise((r, j) => { ajax.postLogin(dataLogin, 'login', '', r) });
    // const statusLogin = await postDataLogin;
    // return statusLogin[0];
};

// =======================================================================
// Post Form Upload Image Files
// =======================================================================

export async function files(file, mode, id) {
    const imgCallback = await new Promise((r, j) => {
        ajax.fileUpload(file, mode, id, r)
    })
    return imgCallback;
};

// =======================================================================
// Post InsertData To Base Server
// =======================================================================

export function postDatas(value, data_machine, data_material, image, dataUserId) {
    let arrImgName = [], machineIds = '', materialIds = '';
    new Promise(async (r, j) => {
        ajax.set(data_machine, 'setDataMachine', '', r);
        ajax.set(data_material, 'setDataMaterial', '', r);
    }).then(async () => {
        const machineId = await getData('getMachine', '');
        const materialId = await getData('getMaterial', '');

        image.forEach((item, i) => { arrImgName.push(item.name + '||' + i); });
        machineId.forEach(item => { machineIds = item.id; });
        materialId.forEach(item => { materialIds = item.id; });
        const data = {
            action: "INSERT",
            id: '', // SEND ID WHEN UPDATE DATA 
            sql: {
                user_id: {
                    val: (dataUserId.dataUser.id_application == '' ? 0 : dataUserId.dataUser.id_application),
                    type: "INTEGER"
                },
                type_id: {
                    val: (value.type_id == '' ? 0 : value.type_id),
                    type: "INTEGER"
                },
                type_name: {
                    val: value.type_name,
                    type: "TEXT"
                },
                data_name: {
                    val: value.data_name,
                    type: "TEXT"
                },
                data_shop: {
                    val: value.data_shop,
                    type: "TEXT"
                },
                data_address: {
                    val: value.data_address,
                    type: "TEXT"
                },
                province_id: {
                    val: value.province_id.value,
                    type: "TEXT"
                },
                district_id: {
                    val: value.district_id,
                    type: "TEXT"
                },
                data_phone: {
                    val: value.data_phone,
                    type: "TEXT"
                },
                data_line: {
                    val: value.data_line,
                    type: "TEXT"
                },
                data_facebook: {
                    val: value.data_facebook,
                    type: "TEXT"
                },
                data_instagram: {
                    val: value.data_instagram,
                    type: "TEXT"
                },
                machine_id: {
                    val: (machineIds == '' ? 0 : machineIds),
                    type: "INTEGER"
                },
                material_id: {
                    val: (materialIds == '' ? 0 : materialIds),
                    type: "INTEGER"
                },
                img_name: {
                    val: (arrImgName[0] == undefined ? JSON.stringify('')
                        :
                        JSON.stringify(arrImgName)
                    ),
                    type: "TEXT"
                },
                data_etc: {
                    val: value.data_etc,
                    type: "TEXT"
                },
                latitude: {
                    val: value.latitude,
                    type: "TEXT"
                },
                longitude: {
                    val: value.longitude,
                    type: "TEXT"
                },
                active: {
                    val: true,
                    type: "BOOLEAN"
                }
            }
        }
        new Promise((r, j) => {
            ajax.set(data, 'setDataInput', '', r)
        }).then(async () => {
            if (image[0] !== undefined) {
                files(image, 'fileUploadImg', 'PIC');
            } else {
                // console.log('IMG Null');
            }
        })
    })
};

// =======================================================================
// Post UpdateData In Base Server
// =======================================================================

export function updateDatas(dataRow, data_machine, data_material, image) {
    //     let arrImgName = [];
    //     console.log(dataRow);
    //     new Promise(async (r, j) => {
    //         ajax.set(data_machine, 'setDataMachine', '', r);
    //         ajax.set(data_material, 'setDataMaterial', '', r);
    //     }).then(async () => {
    //         if (dataRow.img_name) {
    //             let baseImg = JSON.parse(dataRow.img_name)
    //             baseImg.forEach(item => {
    //                 arrImgName.push(item);
    //             });
    //             if (photos !== undefined) {
    //                 photos.forEach(item => { arrImgName.push(item.name); });
    //             }
    //         }
    //         const data = {
    //             action: "UPDATE",
    //             id: dataRow.id, // SEND ID WHEN UPDATE DATA 
    //             sql: {
    //                 user_id: {
    //                     val: (dataRow.user_id == '' ? 0 : dataRow.user_id),
    //                     type: "INTEGER"
    //                 },
    //                 type_id: {
    //                     val: (dataRow.type_id == '' ? 0 : dataRow.type_id),
    //                     type: "INTEGER"
    //                 },
    //                 type_name: {
    //                     val: dataRow.type_name,
    //                     type: "TEXT"
    //                 },
    //                 data_name: {
    //                     val: dataRow.data_name,
    //                     type: "TEXT"
    //                 },
    //                 data_shop: {
    //                     val: dataRow.data_shop,
    //                     type: "TEXT"
    //                 },
    //                 data_address: {
    //                     val: dataRow.data_address,
    //                     type: "TEXT"
    //                 },
    //                 province_id: {
    //                     val: dataRow.province_id.value,
    //                     type: "TEXT"
    //                 },
    //                 district_id: {
    //                     val: dataRow.district_id,
    //                     type: "TEXT"
    //                 },
    //                 data_phone: {
    //                     val: dataRow.data_phone,
    //                     type: "TEXT"
    //                 },
    //                 data_line: {
    //                     val: dataRow.data_line,
    //                     type: "TEXT"
    //                 },
    //                 data_facebook: {
    //                     val: dataRow.data_facebook,
    //                     type: "TEXT"
    //                 },
    //                 data_instagram: {
    //                     val: dataRow.data_instagram,
    //                     type: "TEXT"
    //                 },
    //                 img_name: {
    //                     val: (arrImgName[0] == undefined ? '' : JSON.stringify(arrImgName)),
    //                     type: "TEXT"
    //                 },
    //                 data_etc: {
    //                     val: dataRow.data_etc,
    //                     type: "TEXT"
    //                 },
    //                 active: {
    //                     val: true,
    //                     type: "BOOLEAN"
    //                 }
    //             }
    //         }
    //         await new Promise((r, j) => {
    //             ajax.set(data, 'setDataInput', '', r)
    //         }).then(() => {
    //             // if (imageUpload.img[0] !== undefined) {
    //             //     upload();
    //             // } else {
    //             //     // console.log('IMG Null');
    //             // }
    //             if (image[0] !== undefined) {
    //                 files(image, 'fileUploadImg', 'PIC');
    //             }else {
    //                 // console.log('IMG Null');
    //             }
    //         })
    //     })
};

// =======================================================================
// Post AttendData In Base Server
// =======================================================================

export async function postAttendData(dataChkIn, statusCheck) {
    const imgName = await files(dataChkIn.imgChkIn, 'fileUploadImgChkIn', 'PIC');
    var myObject = JSON.parse(imgName);
    const dataChk = {
        action: "INSERT",
        id: '',//SEND ID WHEN UPDATE DATA 
        sql: {
            id_application: {
                val: (dataChkIn.id_application == '' ? 0 : dataChkIn.id_application),
                type: "INTEGER"
            },
            cmp_id: {
                val: dataChkIn.cmpId,
                type: "INTEGER"
            },
            cmp_name: {
                val: dataChkIn.cmpName,
                type: "TEXT"
            },
            img_chkIn: {
                val: myObject[0],
                type: "TEXT"
            },
            status: {
                val: (statusCheck === false ? 0
                    :
                    statusCheck
                ),
                type: "BOOLEAN"
            },
            date_time: {
                val: new Date,
                type: "TEXT"
            },
            latitude: {
                val: dataChkIn.currentPosition.latitude,
                type: "TEXT"
            },
            longitude: {
                val: dataChkIn.currentPosition.longitude,
                type: "TEXT"
            },
            active: {
                val: true,
                type: "BOOLEAN"
            }
        }
    }
    await new Promise(async (r, j) => {
        ajax.set(dataChk, 'setAttendData', '', r);
    }).then(async (callback) => {
        const attendData = await getData('getAttendData', '');
        const dataCheckout = await getData('getDataCheckout', dataChkIn.id_application);
        const attendUserData = await getData('getAttendUserData', dataChkIn.id_application);
        const dateNow = moment(new Date).format('YYYY-MM-DD');
        console.log(dataCheckout[0]);
        const dataUserChk = {
            action: "INSERT",
            id: '',//SEND ID WHEN UPDATE DATA 
            sql: {
                id_application: {
                    val: (dataChkIn.id_application == '' ? 0 : dataChkIn.id_application),
                    type: "INTEGER"
                },
                name: {
                    val: dataChkIn.name,
                    type: "TEXT"
                },
                chk_in_frist: {
                    val: new Date,
                    type: "TEXT"
                },
                chk_out_last: {
                    val: 0,
                    type: "TEXT"
                },
                active: {
                    val: true,
                    type: "BOOLEAN"
                }
            }
        }

        if (attendData.length == 1) {
            new Promise(async (r, j) => {
                ajax.set(dataUserChk, 'setAttendUserData', '', r);
            })
        } else {
            if (attendUserData === false) {
                new Promise(async (r, j) => {
                    ajax.set(dataUserChk, 'setAttendUserData', '', r);
                })
            } else if (attendUserData[0].create_date !== dateNow) {
                new Promise(async (r, j) => {
                    ajax.set(dataUserChk, 'setAttendUserData', '', r);
                })
            }
        }
        if (attendUserData[0].chkOutLast === '0' || attendUserData[0].chkOutLast < dataCheckout[0].date_time
        ) {
            const dataUserChkUpdate = {
                action: "Update",
                id: attendUserData[0].id,//SEND ID WHEN UPDATE DATA 
                sql: {
                    chk_out_last: {
                        val: (dataCheckout == false ? 0 : dataCheckout[0].date_time),
                        type: "TEXT"
                    },
                }
            }
            new Promise(async (r, j) => {
                ajax.set(dataUserChkUpdate, 'setAttendUserData', '', r);
            })
        }
    })
};

// =======================================================================
// Post LinkAPK In Base Server
// =======================================================================

export function postLinkAPK(value) {
    const data = {
        action: "INSERT",
        id: '', // SEND ID WHEN UPDATE DATA 
        sql: {
            link: {
                val: value.linkAPK,
                type: "TEXT"
            },
            version: {
                val: value.versionAPK,
                type: "TEXT"
            },
            active: {
                val: true,
                type: "BOOLEAN"
            }
        }
    }
    new Promise(async (r, j) => {
        ajax.set(data, 'setLinkAPK', '', r);
    }).then(async () => {

    })
};

// =======================================================================
// Post AttendData In Base Server
// =======================================================================

export async function postRequestLeave(data) {
    const dataLeaveRequest = {
        action: "INSERT",
        id: '',//SEND ID WHEN UPDATE DATA 
        sql: {
            id_application: {
                val: data.id_application,
                type: "INTEGER"
            },
            id_position: {
                val: data.id_position,
                type: "INTEGER"
            },
            id_div: {
                val: data.id_div,
                type: "INTEGER"
            },
            id_dept: {
                val: data.id_dept,
                type: "INTEGER"
            },
            id_company: {
                val: data.id_company,
                type: "INTEGER"
            },
            id_em_detail: {
                val: data.id_em_detail,
                type: "INTEGER"
            },
            rw_doc_no: {
                val: data.rw_doc_no,
                type: "TEXT"
            },
            leave_requestor: {
                val: data.leave_requestor,
                type: "INTEGER"
            },
            leave_requestorTxt: {
                val: data.leave_requestorTxt,
                type: "TEXT"
            },
            leave_type: {
                val: data.leave_type,
                type: "INTEGER"
            },
            leave_typeTxt: {
                val: data.leave_typeTxt,
                type: "TEXT"
            },
            leave_date_from1: {
                val: data.leave_date_from1,
                type: "DATE"
            },
            leave_date_from2: {
                val: data.leave_date_from2,
                type: "DATE"
            },
            leave_days1: {
                val: (data.leave_days1 === '' ? 0 : data.leave_days1),
                type: "INTEGER"
            },
            leave_days1Txt: {
                val: data.leave_days1Txt,
                type: "TEXT"
            },
            leave_time_from1: {
                val: (data.leave_time_from1 === '' ? 8 : data.leave_time_from1),
                type: "FLOAT"
            },
            leave_time_from1Txt: {
                val: (data.leave_time_from1Txt === '' ? '08:00' : data.leave_time_from1Txt),
                type: "TEXT"
            },
            leave_time_to1: {
                val: (data.leave_time_from1 === '' ? 17 : data.leave_time_from1),
                type: "FLOAT"
            },
            leave_time_to1Txt: {
                val: (data.leave_time_from1Txt === '' ? '17:00' : data.leave_time_from1),
                type: "TEXT"
            },
            check_hour: {
                val: (data.check_hour === '' ? '0' : data.check_hour),
                type: "TEXT"
            },
            day_leave: {
                val: (data.day_leave === '' ? '1' : data.day_leave),
                type: "INTEGER"
            },
            hour_leave: {
                val: (data.hour_leave === '' ? '0' : data.hour_leave),
                type: "INTEGER"
            },
            total_leave: {
                val: (data.total_leave === '' ? 480 : data.total_leave),
                type: "INTEGER"
            },
            leave_remark: {
                val: data.leave_remark,
                type: "TEXT"
            },
            leave_name_file: {
                val: (data.fileUpload === '' ? '' : data.fileUpload),
                type: "TEXT"
            },
            chk_qc: {
                val: data.chk_qc,
                type: "INTEGER"
            },
            chk_app: {
                val: data.chk_app,
                type: "INTEGER"
            },
            chk_cancel: {
                val: data.chk_cancel,
                type: "INTEGER"
            },
            emreq_active: {
                val: 1,
                type: "INTEGER"
            },
        }
    }

    await new Promise((r, j) => {
        ajax.set(dataLeaveRequest, 'setRequestLeave', '', r)
    }).then(async () => {
        const dataRequestLeave = await getData('getRequestLeave', '');
        
        const dataDocLeaveRequest = {
            action: "INSERT",
            id: '',//SEND ID WHEN UPDATE DATA 
            sql: {
                id_leave_request: {
                    val: dataRequestLeave[0].id,
                    type: "INTEGER"
                },
                id_application: {
                    val: data.id_application,
                    type: "INTEGER"
                },
                id_em_detail: {
                    val: data.id_em_detail,
                    type: "INTEGER"
                },
                rw_doc_no: {
                    val: data.rw_doc_no,
                    type: "TEXT"
                },
                leave_name_file: {
                    val: (data.fileUpload === '' ? '' : data.fileUpload),
                    type: "TEXT"
                },
            }
        }
        await new Promise((r, j) => {
            ajax.set(dataDocLeaveRequest, 'setDocRequestLeave', '', r)
        }).then(async () => {
            console.log('DONE');
        })
    }).catch((error) => {
        console.log(error);
    })
    // console.log(dataLeaveRequest);
    // await new Promise(async (r, j) => {
    //         ajax.set(dataChk, 'setAttendData', '', r);
    //     }).then(async (callback) => {
    //         const attendData = await getData('getAttendData', '');
    //         const dataCheckout = await getData('getDataCheckout', dataChkIn.id_application);
    //         const attendUserData = await getData('getAttendUserData', dataChkIn.id_application);
    //         const dateNow = moment(new Date).format('YYYY-MM-DD');
    //         console.log(dataCheckout[0]);
    //         const dataUserChk = {
    //             action:"INSERT",
    //             id: '',//SEND ID WHEN UPDATE DATA 
    //             sql:{
    //                 id_application: {
    //                     val: (dataChkIn.id_application == '' ? 0 : dataChkIn.id_application),
    //                     type: "INTEGER"
    //                 },
    //                 name:{
    //                     val: dataChkIn.name,
    //                     type: "TEXT"
    //                 },
    //                 chk_in_frist:{
    //                     val: new Date,
    //                     type: "TEXT"
    //                 },
    //                 chk_out_last:{
    //                     val: 0,
    //                     type: "TEXT"
    //                 },
    //                 active: {
    //                     val: true,
    //                     type: "BOOLEAN"
    //                 }
    //             }
    //         }

    //         if(attendData.length == 1){
    //             new Promise(async (r, j) => {
    //                 ajax.set(dataUserChk, 'setAttendUserData', '', r);
    //             })
    //         }else{
    //             if(attendUserData === false){
    //                 new Promise(async (r, j) => {
    //                     ajax.set(dataUserChk, 'setAttendUserData', '', r);
    //                 })
    //             }else if(attendUserData[0].create_date !== dateNow){
    //                 new Promise(async (r, j) => {
    //                     ajax.set(dataUserChk, 'setAttendUserData', '', r);
    //                 })
    //             }
    //         }
    //         if(attendUserData[0].chkOutLast === '0'|| attendUserData[0].chkOutLast < dataCheckout[0].date_time
    //         ){
    //             const dataUserChkUpdate= {
    //                 action:"Update",
    //                 id: attendUserData[0].id,//SEND ID WHEN UPDATE DATA 
    //                 sql:{
    //                     chk_out_last:{
    //                         val: (dataCheckout == false ? 0 : dataCheckout[0].date_time),
    //                         type: "TEXT"
    //                     },
    //                 }
    //             }
    //             new Promise(async (r, j) => {
    //                 ajax.set(dataUserChkUpdate, 'setAttendUserData', '', r);
    //             })
    //         }
    //     })
};



// =======================================================================
// Post DeleteData In Base Server
// =======================================================================

export async function deleteData(valueDel) {
    const delData = await new Promise((r, j) => { ajax.delete(valueDel, 'delete', '', r) });
    const callBackDel = await delData;
    return callBackDel;
};
