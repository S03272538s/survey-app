// =======================================================================
// Init/State Componant
// =======================================================================

import React, { useState, useEffect } from 'react';
import { View, ActivityIndicator} from 'react-native';
import { StyleContain } from '../style/funcStyle';
const styles = StyleContain();

// =======================================================================
// Repository Data
// =======================================================================

import { getData } from '../../repository/funcGetData';

// =======================================================================
// MapView
// =======================================================================

import * as Location from 'expo-location';

import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import getDirections from "react-native-google-maps-directions";

import MapViewDirections from 'react-native-maps-directions';
import ModalMarkers from '../modal/funcModalMarker';
const initialState = {
    latitude: null,
    longitude: null,
    latitudeDelta: 0.00001,
    longitudeDelta: 0.00001,
};

// =======================================================================
//  Export ShowViewMap
// =======================================================================

export default function Map({ dataUser, handleClickLogout }) {
    const [mapStyle, setMapStyle] = useState(styles.mapViewStyle);
    
    //State Position/Marker On Map
    const [markPosition, setMarkPosition] = useState();
    const [currentPosition, setCurrentPosition] = useState(initialState);
   
    //State To ModalMarker 
    const [modalVisible, setModalVisible] = useState(false);
    const [markerDetail, setMarkerDetail] = useState();

    const getDataAll = async () => {
        const dataApi = await getData('locations', '');
        setMarkPosition(dataApi);
    };
    const handleGetDirections =()=>{
        const data = {
            source:{
                // latitude: 13.9873373, //13.9873373,100.6073718
                // longitude: 100.6073718
                latitude: currentPosition.latitude, //13.9873373,100.6073718
                longitude: currentPosition.longitude
            },
            destination:{
                latitude: 13.9873399, //13.9873399,101.6073718
                longitude: 101.6073718
            },
            params:[
                {
                    key:"travelmode",
                    value: "driving"  // may be "walking", "bicycling" or "transit" as well
                },
                // {
                //     key:"dir_action",
                //     value: "navigate" // this instantly initializes navigation using the given travel mode
                // },

            ]
        }
        // getDirections(data)
    }
    useEffect(() => {
        getDataAll();
        (async () => {
            let { status } = await Location.requestForegroundPermissionsAsync();
            if (status !== 'granted') {
                console.warn('Fail :: Permission to access location was denied');
                return;
            }
            let location = await Location.getCurrentPositionAsync({});
            setCurrentPosition(
                {
                    ...currentPosition,
                    latitude: location.coords.latitude,
                    longitude: location.coords.longitude,
                }
            );
            await handleGetDirections();
        })();
        // getDirections('13.9873373,100.6073718' ,'13.9873399,-101.6073718');
    }, []);
    
    const handleOnClickMarker = (marker) => {
        setModalVisible(true);
        setMarkerDetail(marker);
    }
    const handleOnClickMapCallback = () => {
        setModalVisible(false)
    }
    const mapOnReady = () => {
        setMapStyle({ paddingTop: 1, ...mapStyle })
    }
    return currentPosition.latitude ? (
        <View style={mapStyle}>
            {/* <Header dataUser={dataUser} handleClickLogout={handleClickLogout} /> */}
            <MapView
                onPress={() => handleOnClickMapCallback('none')}
                style={styles.mapViewStyle}
                zoomEnabled={true}
                provider={PROVIDER_GOOGLE}
                showsUserLocation={true}
                onMapReady={mapOnReady}
                showsMyLocationButton={true}
                initialRegion={currentPosition}
                // showsTraffic={true}
                // showsIndoorLevelPicker={true}
                toolbarEnabled={true}
                loadingEnabled={true}
            >
                {markPosition !== undefined &&
                    markPosition.map((marker, index) => (
                        <Marker
                            onPress={() => handleOnClickMarker(marker)}
                            key={index}
                            coordinate={{ latitude: marker.latitude, longitude: marker.longitude }}
                        >
                        </Marker>
                    ))
                }
            </MapView>
            {modalVisible === true && markerDetail!== undefined && currentPosition !== undefined &&
                <ModalMarkers
                currentPosition={currentPosition}
                dataMarker={markerDetail}
                statusVisible={modalVisible}
                toggle={handleOnClickMapCallback}
            />
            } 
        </View>
    ) : <ActivityIndicator style={{ flex: 1 }} animating size="large" />
};