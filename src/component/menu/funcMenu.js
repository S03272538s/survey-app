// =======================================================================
// Init/State Componant
// =======================================================================

import React, { useState, useEffect } from 'react';
import { View, ScrollView, Text } from 'react-native';
import ListMenu from './funcListMenu';
import Logout from '../../login-out/funcLogout';
import HeaderMenu from '../header/funcHeaderMenu';
import { FontAwesome5, MaterialCommunityIcons, MaterialIcons } from '@expo/vector-icons';

import { StyleContain } from '../style/funcStyle';
const style = StyleContain();
// =======================================================================
// Repository Data
// =======================================================================

import { getAccessRightDev, getAccessRight } from '../../repository/funcGetData';

// =======================================================================
// NavigatorMenu Componant
// =======================================================================

export default function NavigatorMenu({ navigation, param }) {
  const dataUser = param.dataUser;
  const iconStyle = { size: 24, color: '#292929' };
  const [access, setAccess] = useState(true)
  const [accessDev, setAccessDev] = useState(true)
  useEffect(() => {
    (async () => {
      setAccess(await getAccessRight(dataUser, 'getAccessRights', ''));
      setAccessDev(await getAccessRightDev(dataUser, 'getAccessRights', ''));
    })();
  }, [])
  return (
    <View style={{ flex: 1 }}>
      <HeaderMenu navigation={navigation} namePage={'ตัวเลือก'} />
      <ScrollView style={{ paddingTop: 20, flex: 1 }}>

        <ListMenu
          navigation={navigation}
          nameNavigations={'Home'}
          dataUser={undefined}
          txtTitleList={'หน้าแรก'}
          iconImg={<MaterialCommunityIcons name="home" size={iconStyle.size} color={iconStyle.color} style={{ paddingRight: 20 }} />}
        />

        <View style={{ flex: 1, alignItems: 'flex-start' }}>
          <View style={style.txtBoxShowData}>
            <Text style={style.txtTitleContent} >การใช้งาน</Text>
          </View>
          <View style={{ flex: 0.5, padding: 10, width: '100%' }}>

            <ListMenu
              navigation={navigation}
              nameNavigations={'CheckIn'}
              dataUser={undefined}
              txtTitleList={'เข้า/ออก งาน'}
              iconImg={<MaterialCommunityIcons name="map-marker-check" size={iconStyle.size} color={iconStyle.color} style={{ paddingRight: 20 }} />}
            />
            {access === false &&
              <ListMenu
                navigation={navigation}
                nameNavigations={'Survey'}
                dataUser={undefined}
                txtTitleList={'สำรวจร้าน'}
                iconImg={<MaterialCommunityIcons name="walk" size={iconStyle.size} color={iconStyle.color} style={{ paddingRight: 20 }} />}
              />
            }
          </View>
        </View>

        <View style={{ flex: 1, alignItems: 'flex-start' }}>
          <View style={style.txtBoxShowData}>
            <Text style={style.txtTitleContent} >เกี่ยวกับผู้ใช้</Text>
          </View>
          <View style={{ flex: 0.5, padding: 10, width: '100%' }}>
            <ListMenu
              navigation={navigation}
              nameNavigations={'MenuProfile'}
              dataUser={dataUser}
              txtTitleList={'โปรไฟล์'}
              iconImg={<MaterialCommunityIcons name="account" size={iconStyle.size} color={iconStyle.color} style={{ paddingRight: 20 }} />}
            />
            <ListMenu
              navigation={navigation}
              nameNavigations={'MenuResetPass'}
              dataUser={dataUser}
              txtTitleList={'เปลี่ยนรหัสผ่าน'}
              iconImg={<MaterialCommunityIcons name="lock-reset" size={iconStyle.size} color={iconStyle.color} style={{ paddingRight: 20 }} />}
            />
          </View>
        </View>

        <View style={{ flex: 1, alignItems: 'flex-start' }}>
          <View style={style.txtBoxShowData}>
            <Text style={style.txtTitleContent} >ระบบ</Text>
          </View>
          <View style={{ flex: 0.5, padding: 10, width: '100%' }}>
            {accessDev === false &&
              <ListMenu
                navigation={navigation}
                nameNavigations={'AddLinkAPK'}
                dataUser={undefined}
                txtTitleList={'เปลี่ยน ลิ้งดาวโหลด APK'}
                iconImg={<MaterialIcons name="add-link" size={iconStyle.size} color={iconStyle.color} style={{ paddingRight: 20 }} />}
              />
            }
            <ListMenu
              navigation={navigation}
              nameNavigations={'MenuCheckUpdate'}
              dataUser={{ chkUpdate: true }}
              txtTitleList={'ตรวจสอบอัพเดต'}
              iconImg={<MaterialIcons name="update" size={iconStyle.size} color={iconStyle.color} style={{ paddingRight: 20 }} />}

            />
            <ListMenu
              navigation={navigation}
              nameNavigations={'MenuDetailsApp'}
              dataUser={undefined}
              txtTitleList={'เกี่ยวกับ'}
              iconImg={<MaterialCommunityIcons name="file-document" size={iconStyle.size} color={iconStyle.color} style={{ paddingRight: 20 }} />}
            />
            <Logout navigation={navigation} dataUser={dataUser} />
          </View>
        </View>
      </ScrollView>
    </View>
  );
}