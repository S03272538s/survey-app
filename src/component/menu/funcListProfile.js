// =======================================================================
// Init/State Componant
// =======================================================================

import React from 'react';
import { Text, View, TouchableHighlight } from 'react-native';
import { StyleContain } from '../style/funcStyle';
const style = StyleContain();

// =======================================================================
// NavigatorMenu Componant
// =======================================================================

export default function ListProfile({txtTitle, txtContent, txtContentSub}) {
    return (
        <View style={{ flex: 1, alignItems: 'flex-start' }}>
            <View style={style.txtBoxShowData}>
                <Text style={style.txtTitleContent} >{txtTitle}</Text>
            </View>
            <View style={{ flex: 0.5, padding: 10, width: '100%' }}>
                <Text>{txtContent}</Text>
                {txtContentSub!==undefined&&
                <Text>{txtContentSub}</Text>
                }
            </View>
        </View>
    );
}