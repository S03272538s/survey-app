// =======================================================================
// Init/State Componant
// =======================================================================

import React, { useState, useEffect } from 'react';
import { Text, View, ScrollView, TouchableHighlight } from 'react-native';
import { Button, TextInput, } from 'react-native-paper';
import HeaderMenu from '../header/funcHeaderMenu';
import ModalAlertLogout from '../../login-out/funcModalLogout';
import { MaterialIcons } from '@expo/vector-icons'; 
import { StyleContain } from '../style/funcStyle';
const style = StyleContain();

// =======================================================================
// Repository Data
// =======================================================================

import { postLinkAPK } from '../../repository/funcPostData';
import { getAccessRight, getPosition } from '../../repository/funcGetData';

// =======================================================================
// NavigatorMenu Componant
// =======================================================================

export default function MenuAddLinkAPK({navigation}) {
    const [link, setLink] = useState({linkAPK:'', versionAPK:''})
    const [statusVisible, setStatusVisible] = useState(false)

    const handleOnChageTxt=(txt)=>{
        link.linkAPK = txt;
        setLink({...link});
    }
    const handleOnChageVerTxt=(txt)=>{
        link.versionAPK = txt;
        setLink({...link});
    }
    const chkStatusSave=()=>{link.linkAPK!=='' && setStatusVisible(true, { ...statusVisible })}

    const handleOnClickSaveCallback = (status) => {
        setStatusVisible(false);
        if (status === true) {
            postLinkAPK(link);
        }
    }
    return (
        <View style={{ flex: 1 }}>
            <HeaderMenu navigation={navigation} namePage={'เปลี่ยน Link APK'} />
            <ScrollView style={{ flex: 1, paddingLeft: 20, paddingRight: 20 }}>
                <TextInput
                    theme={style.styleInput}
                    style={style.inputStyle, { flex: 1 }}
                    value={link.versionAPK}
                    label="เวอร์ชั่น"
                    onChangeText={(txt) => { handleOnChageVerTxt(txt) }}
                />
                <TextInput
                    theme={style.styleInput}
                    style={style.inputStyle, { flex: 1 }}
                    value={link.linkAPK}
                    label="ลิ้งดาวโหลดไฟล์ APK"
                    onChangeText={(txt) => { handleOnChageTxt(txt) }}
                />
                <Button
                        color="#4c669f"
                        mode="contained"
                        onPress={chkStatusSave}
                        style={style.btnStyle}
                    >
                        <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: 16 }}>เพิ่มลิ้งดาวโหลด APK</Text>
                    </Button>
                <ModalAlertLogout logTxt='ต้องการเปลี่ยนรหัสผ่าน' statusVisible={statusVisible} toggle={handleOnClickSaveCallback} />
            </ScrollView>
        </View>
    );
}