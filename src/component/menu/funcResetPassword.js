// =======================================================================
// Init/State Componant
// =======================================================================

import React, { useState, useEffect } from 'react';
import { Text, View, ScrollView, TouchableHighlight } from 'react-native';
import { Button, TextInput, } from 'react-native-paper';
import HeaderMenu from '../header/funcHeaderMenu';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import ModalAlertLogout from '../../login-out/funcModalLogout';
import { StyleContain } from '../style/funcStyle';
const style = StyleContain();

// =======================================================================
// Repository Data
// =======================================================================

import { getAccessRight, getPosition } from '../../repository/funcGetData';
import { rePassword } from '../../repository/funcPostData';

// =======================================================================
// NavigatorMenu Componant
// =======================================================================

export default function MenuResetPassword({ navigation, param }) {
    const [statusVisible, setStatusVisible] = useState(false)

    // TextAlert Bar // Show/hide Alert Bar 
    const [txtErrorFill, setTxtErrorFill] = useState('')
    const [txtStatusComplete, setTxtStatusComplete] = useState('')
    const [statusComplete, setStatusComplete] = useState(false)
    const [statusErrorFill, setStatusErrorFill] = useState(false)
    
    // Check Security Password Input Box 
    const [showSecurePass, setShowSecurePass] = useState(false)
    const [showSecureOldPass, setShowSecureOldPass] = useState(false)
    const [showSecureVerrifyPass, setShowSecureVerrifyPass] = useState(false)

    // ShowIcon Set Security Password Input Box 
    const [showIconPass, setShowIconPass] = useState(false)
    const [showIconOldPass, setShowIconOldPass] = useState(false)
    const [showIconVerrifyPass, setShowIconVerrifyPass] = useState(false)

    // Init Param
    const [newPassword, setNewPassword] = useState(
        {
            id_application: param.id_application,
            username: param.username,
            oldPassword:'',
            password: '',
            verifyPassword: ''
        }
    )

    // Set Value Input Box OnChege
    const handleOnChageOldPass = (newPass) => {
        newPassword.oldPassword = newPass;
        setNewPassword({ ...newPassword })
        newPass !== '' ? setShowIconOldPass(true) : setShowIconOldPass(false)
    }
    const handleOnChagePass = (newPass) => {
        newPassword.password = newPass;
        setNewPassword({ ...newPassword })
        newPass !== '' ? setShowIconPass(true) : setShowIconPass(false)
    }
    const handleOnChageVerify = (verifyPass) => {
        newPassword.verifyPassword = verifyPass;
        setNewPassword({ ...newPassword })
        verifyPass !== '' ? setShowIconVerrifyPass(true) : setShowIconVerrifyPass(false)
    }
    
    // Check Switch Show/Hide Icon Secure Password
    const handleShowSecureOld = (status) => { status === false ? setShowSecureOldPass(true) : setShowSecureOldPass(false) }
    const handleShowSecure = (status) => { status === false ? setShowSecurePass(true) : setShowSecurePass(false) }
    const handleShowSecureVerrify = (status) => { status === false ? setShowSecureVerrifyPass(true) : setShowSecureVerrifyPass(false) }

    // Check Value Before Update Password
    const chkStatusSave = () => { 
        if (newPassword.oldPassword === '') {
            setTxtErrorFill('กรุณากรอกรหัสผ่านเดิม')
            setStatusErrorFill(true)
            setTimeout(() => {setStatusErrorFill(false)}, 3000)
        }
        else if (newPassword.oldPassword !== param.password) {
            setTxtErrorFill('รหัสผ่านเดิมไม่ถูกต้อง')
            setStatusErrorFill(true)
            setTimeout(() => {setStatusErrorFill(false)}, 3000)
        }
        else if (newPassword.password === '') {
            setTxtErrorFill('กรุณากรอกรหัสผ่านใหม่')
            setStatusErrorFill(true)
            setTimeout(() => {setStatusErrorFill(false)}, 3000)
        }
        else if (newPassword.verifyPassword === '') {
            setTxtErrorFill('กรุณากรอกยืนยันรหัสผ่านใหม่')
            setStatusErrorFill(true)
            setTimeout(() => {setStatusErrorFill(false)}, 3000)
        } else {
            if (newPassword.password === newPassword.verifyPassword) {
                setStatusVisible(true, { ...statusVisible }) 
            } else {
                setTxtErrorFill('รหัสผ่านไม่เหมือนกัน')
                newPassword.verifyPassword = '';
                setNewPassword({ ...newPassword })
                setStatusErrorFill(true)
                setTimeout(() => {setStatusErrorFill(false)}, 3000)
            }
        }
    
    };

    // Post Update Password/ Set Any To Default
    const handleOnClickSaveCallback = (status) => {
        setStatusVisible(false);
        if (status === true) {
            rePassword(newPassword);
            setTxtStatusComplete('เปลี่ยนรหัสผ่านเรียบร้อย')
            newPassword.oldPassword = '';
            newPassword.password = '';
            newPassword.verifyPassword = '';
            setNewPassword({ ...newPassword })
            setStatusComplete(true)
            setShowIconPass(false)
            setShowIconOldPass(false)
            setShowIconVerrifyPass(false)
            setTimeout(() => {setStatusComplete(false)}, 3000)
        }
    }

    return (
        <View style={{ flex: 1 }}>
            <HeaderMenu navigation={navigation} namePage={'เปลี่ยนรหัสผ่าน'} />
            <ScrollView style={{ flex: 1, paddingLeft: 20, paddingRight: 20 }}>
                <View>
                    <View style={{
                        flex: 0.1,
                        alignItems: 'center',
                        paddingTop: 20,
                        paddingBottom: 20,
                        borderBottomWidth: 1,
                        borderBottomColor: '#FFF9DB',
                    }}
                    >
                        {/* <Text style={style.txtTitleContent}>กรอกข้อมูล เพื่อเปลี่ยนรหัสผ่าน</Text> */}
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <TextInput
                            theme={style.styleInput}
                            style={style.inputStyle, { flex: 1 }}
                            value={newPassword.oldPassword}
                            label="รหัสผ่านเดิม"
                            onChangeText={(text) => { handleOnChageOldPass(text) }}
                            secureTextEntry={showSecureOldPass}
                        />
                        {showIconOldPass === true &&
                            <TouchableHighlight
                                activeOpacity={0.6}
                                underlayColor="#DDDDDD"
                                style={{ borderRadius: 20 }}
                                onPress={() => { handleShowSecureOld(showSecureOldPass) }}
                            >
                                <MaterialCommunityIcons name="eye" size={24} color='#292929' />
                            </TouchableHighlight>
                        }
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <TextInput
                            theme={style.styleInput}
                            style={style.inputStyle, { flex: 1 }}
                            value={newPassword.password}
                            label="รหัสผ่านใหม่"
                            onChangeText={(text) => { handleOnChagePass(text) }}
                            secureTextEntry={showSecurePass}
                        />
                        {showIconPass === true &&
                            <TouchableHighlight
                                activeOpacity={0.6}
                                underlayColor="#DDDDDD"
                                style={{ borderRadius: 20 }}
                                onPress={() => { handleShowSecure(showSecurePass) }}
                            >
                                <MaterialCommunityIcons name="eye" size={24} color='#292929' />
                            </TouchableHighlight>
                        }
                    </View>

                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <TextInput
                            theme={style.styleInput}
                            style={style.inputStyle, { flex: 1 }}
                            value={newPassword.verifyPassword}
                            label="ยืนยันรหัสผ่าน"
                            onChangeText={(text) => { handleOnChageVerify(text) }}

                            secureTextEntry={true}
                            secureTextEntry={showSecureVerrifyPass}

                        />
                        {showIconVerrifyPass === true &&
                            <TouchableHighlight
                                activeOpacity={0.6}
                                underlayColor="#DDDDDD"
                                style={{ borderRadius: 20 }}
                                onPress={() => { handleShowSecureVerrify(showSecureVerrifyPass) }}
                            >
                                <MaterialCommunityIcons
                                    name="eye"
                                    size={24}
                                    color='#292929'
                                />
                            </TouchableHighlight>
                        }
                    </View>

                    {statusErrorFill === true &&
                        <View style={{ 
                            flex: 1, 
                            backgroundColor: '#ff4a4a', 
                            alignItems: 'center' , 
                            paddingTop:20 , 
                            paddingBottom:20,
                            borderWidth: 1, 
                            borderColor: '#FFF9DB',
                            borderRadius: 10
                            }}>
                            <Text style={{color:'#FEFEFE' ,fontWeight:'bold'}}>{txtErrorFill}</Text>
                        </View>
                    }
                    {statusComplete === true &&
                        <View style={{ 
                            flex: 1, 
                            backgroundColor: '#3ae062', 
                            alignItems: 'center' , 
                            paddingTop:20 , 
                            paddingBottom:20,
                            borderWidth: 1, 
                            borderColor: '#FFF9DB',
                            borderRadius: 10
                            }}>
                            <Text style={{color:'#FEFEFE' ,fontWeight:'bold'}}>{txtStatusComplete}</Text>
                        </View>
                    }

                    <Button
                        color="#4c669f"
                        mode="contained"
                        onPress={chkStatusSave}
                        style={style.btnStyle}
                    >
                        <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: 16 }}>เปลี่ยนรหัสผ่าน</Text>
                    </Button>
                </View>

                <ModalAlertLogout logTxt='ต้องการเปลี่ยนรหัสผ่าน' statusVisible={statusVisible} toggle={handleOnClickSaveCallback} />

            </ScrollView>
        </View>
    );
}