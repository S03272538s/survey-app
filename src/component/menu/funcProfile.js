// =======================================================================
// Init/State Componant
// =======================================================================

import React, { useState, useEffect } from 'react';
import { Text, View, ScrollView, TouchableHighlight } from 'react-native';
import HeaderMenu from '../header/funcHeaderMenu';
import { StyleContain } from '../style/funcStyle';
import ListProfile from './funcListProfile';
import moment from 'moment';
const style = StyleContain();

// =======================================================================
// Repository Data
// =======================================================================

import { getData, getAccessRight, getPosition } from '../../repository/funcGetData';

// =======================================================================
// NavigatorMenu Componant
// =======================================================================

export default function MenuProfile({ navigation, param }) {
  const [access, setAccess] = useState(true)
  const [detailEmp, setDetailEmp] = useState();

  useEffect(() => {
    (async () => {
      setAccess(await getAccessRight(param, 'getAccessRights', ''));
      setDetailEmp(await getPosition(param, 'getDetailEmp', ''));
    })();
  }, [])
  return (
    <View style={{ flex: 1 }}>
      <HeaderMenu navigation={navigation} namePage={'โปรไฟล์'} />
      <ScrollView style={{ flex: 1 }}>
        {detailEmp &&
          <View>
            <View style={{
              flex: 0.1,
              alignItems: 'center',
              paddingTop: 20,
              paddingBottom: 20,
              borderBottomWidth: 1,
              borderBottomColor: '#FFF9DB',
            }}
            >
              <Text style={style.txtTitleContent}>ข้อมูลพนักงาน</Text>
            </View>
            
            <ListProfile
              txtTitle={'ชื่อผู้ใช้'}
              txtContent={param.username}
            />
            <ListProfile
              txtTitle={'ชื่อ - นามสกุล'}
              txtContent={detailEmp.cont_lbl_name}
            />
            <ListProfile
              txtTitle={'ตำแหน่ง'}
              txtContent={detailEmp.cont_lbl_pos}
            />
            <ListProfile
              txtTitle={'หน้าที่ / ความรับผิดชอบ'}
              txtContent={detailEmp.pos_description}
              txtContentSub={detailEmp.pos_specification}
            />
            {detailEmp&&
            <View style={{ flex: 1, alignItems: 'flex-start' }}>
              <View style={style.txtBoxShowData}>
                <Text style={style.txtTitleContent} >ข้อมูลสัญญาจ้าง</Text>
              </View>
              <View style={{ flex: 0.5, padding: 10, width: '100%' }}>
              <Text>รูปแบบการจ้าง : {detailEmp.cont_wage_type}</Text>
                <Text> 
                  รูปแบบสัญญาจ้าง : {detailEmp.cont_typeTxt} เป็นระยะเวลา {detailEmp.cont_day!== '0'&& detailEmp.cont_day + ' วัน'} 
                  {detailEmp.cont_month!== '0'&& detailEmp.cont_month + ' เดือน'} 
                  {detailEmp.cont_year!== '0'&& detailEmp.cont_year + ' ปี'} 
                </Text>
               <Text>ตั้งแต่วันที่</Text> 
                <Text>{moment(detailEmp.cont_start_date_contract).format('DD MMMM YYYY')} จนถึง {moment(detailEmp.cont_end_date_contract).format('DD MMMM YYYY')}</Text>
                <Text>{moment(detailEmp.cont_end_date_contract).endOf('day').fromNow()} หมดสัญญาจ้าง</Text>
                
                

              </View>
            </View>
            }
            {/* <ListProfile
              txtTitle={'เงินเดือน'}
              txtContent={detailEmp.pos_salary}
            /> */}
          </View>
        }
      </ScrollView>
    </View>
  );
}