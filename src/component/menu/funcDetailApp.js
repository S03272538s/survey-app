// =======================================================================
// Init/State Componant
// =======================================================================

import React, { useState, useEffect } from 'react';
import { Text, View, ScrollView, Image, Linking } from 'react-native';
import HeaderMenu from '../header/funcHeaderMenu';
import Constants from 'expo-constants';
import { StyleContain } from '../style/funcStyle';
import Logout from '../../login-out/funcLogout';

const style = StyleContain();

// =======================================================================
// Repository Data
// =======================================================================

import * as SQLite from 'expo-sqlite';
const db = SQLite.openDatabase("Survey.db");

// =======================================================================
// NavigatorMenu Componant
// =======================================================================

export default function MenuDetailsApp({ navigation, param }) {
    const [versionApp, setVersionApp] = useState()

    const fetchData = () => {
        db.transaction(tx => {
            tx.executeSql('SELECT * FROM dataDevice', null,
                (tx, { rows: { _array } }) => {
                    setVersionApp(_array[0].version_app)
                }, (tx, error) => console.warn('Error :: ', error)
            )
        })
    }

    useEffect(() => {
        fetchData();
    }, [])

    return (
        <View style={{ flex: 1 }}>
            <HeaderMenu navigation={navigation} namePage={'เกี่ยวกับ'} />
            <ScrollView style={{ flex: 1 }}>
                {versionApp &&
                    <View>
                        <View style={{
                            flex: 0.1,
                            alignItems: 'center',
                            paddingTop: 20,
                            paddingBottom: 20,
                            borderBottomWidth: 1,
                            borderBottomColor: '#FFF9DB',
                        }}
                        >
                            <Text style={style.txtTitleContent}>ข้อมูลแอปพลิเคชัน</Text>
                        </View>
                        <View style={{ flex: 1, alignItems: 'center', }}>
                            <Image
                                style={style.logoLogin}
                                source={{ uri: 'http://1.2.175.220/public/img_default_mobile/logo-login.png' }}
                            />
                            <Text style={style.txtTitleContent}>Version. {Constants.manifest.version}</Text>
                        </View>
                    </View>
                }
            </ScrollView>
        </View>
    );
}