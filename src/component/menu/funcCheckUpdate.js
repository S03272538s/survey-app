// =======================================================================
// Init/State Componant
// =======================================================================

import React, { useState, useEffect } from 'react';
import { Text, View, ScrollView, TouchableHighlight, Linking } from 'react-native';
import { Button, TextInput, } from 'react-native-paper';
import HeaderMenu from '../header/funcHeaderMenu';
import ModalAlertLogout from '../../login-out/funcModalLogout';
import { MaterialIcons } from '@expo/vector-icons';
import { StyleContain } from '../style/funcStyle';
import Constants from 'expo-constants';
const style = StyleContain();

// =======================================================================
// Repository Data
// =======================================================================

import * as SQLite from 'expo-sqlite';
const db = SQLite.openDatabase("Survey.db");

import { postLinkAPK } from '../../repository/funcPostData';
import { getData } from '../../repository/funcGetData';

// =======================================================================
// NavigatorMenu Componant
// =======================================================================

export default function MenuCheckUpdate({ navigation }) {

    const [statusVisible, setStatusVisible] = useState(false)
    const [statusUpdate, setStatusUpdate] = useState(false)
    const [dataLink, setDataLink] = useState()
    useEffect(() => {
        (async () => {
            const getAPKBase = await getData('getLinkAPK', '')
            setDataLink(getAPKBase);
            if (Constants.manifest.version != getAPKBase[0].version) {
                setStatusVisible(true,{ ...statusVisible })
            } else {
                setStatusUpdate(true,{ ...statusUpdate })
            }
            // db.transaction(tx => {
            //     tx.executeSql('SELECT * FROM dataDevice', null,
            //         (tx, { rows: { _array } }) => {
            //             console.log(_array[0].version_app);

            //             if (_array[0].version_app != getAPKBase[0].version) {
            //                 setStatusVisible(true,{ ...statusVisible })
            //             } else {
            //                 setStatusUpdate(true,{ ...statusUpdate })
            //             }
            //         }, (tx, error) => console.warn('Error :: ', error)
            //     )
            // })
        })();
    }, [])

    const handleOnClickSaveCallback = (status) => {
        setStatusVisible(false);
        if (status === true) {
            Linking.openURL(dataLink[0].link)
        }
    }
    return (
        <View style={{ flex: 1 }}>
            <HeaderMenu navigation={navigation} namePage={'ตรวจสอบอัพเดต'} />
            {statusUpdate === false ?
                <View style={{ flex: 1, backgroundColor: '#FEFEFE',alignItems:'center',justifyContent:'center' }}>
                    <MaterialIcons name="system-update-alt" size={60} color="#4c669f" />
                    <Text>แอปพลิเคชัน มีอัพเดต</Text>
                    <Text>กรุณา ดาวโหลดไฟล์ APK</Text>
                    <Button
                        color="#4c669f"
                        mode="contained"
                        onPress={()=>{handleOnClickSaveCallback(true)}}
                        style={style.btnStyle}
                    >
                        <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: 16 }}>ดาวโหลดไฟล์ APK ล่าสุด</Text>
                    </Button>
                </View>
                :
                <View style={{ flex: 1, backgroundColor: '#FEFEFE',alignItems:'center',justifyContent:'center' }}>
                    <MaterialIcons name="file-download-done" size={60} color="#3ae062" />
                    <Text>แอปพลิเคชัน ตอนนี้</Text>
                    <Text>เป็นเวอร์ชันล่าสุดแล้ว</Text>
                    <Button
                        color="#4c669f"
                        mode="contained"
                        onPress={()=>{handleOnClickSaveCallback(true)}}
                        style={style.btnStyle}
                    >
                        <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: 16 }}>ดาวโหลดไฟล์ APK ล่าสุด</Text>
                    </Button>
                </View>
            }
            <ModalAlertLogout logTxt='แอปไม่ใช่รุ่นล่าสุด ต้องการอัพเดต' statusVisible={statusVisible} toggle={handleOnClickSaveCallback} />
        </View>
    );
}