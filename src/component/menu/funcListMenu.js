// =======================================================================
// Init/State Componant
// =======================================================================

import React from 'react';
import { Text, View, TouchableHighlight } from 'react-native';
import { StyleContain } from '../style/funcStyle';
const style = StyleContain();

// =======================================================================
// NavigatorMenu Componant
// =======================================================================

export default function ListMenu({ navigation, nameNavigations, dataUser, txtTitleList, iconImg }) {
  return (

    <TouchableHighlight
    activeOpacity={0.6}
      underlayColor={'#8a8a8a'}
      onPress={() => { dataUser !== undefined ? navigation.navigate(nameNavigations, { dataUser }) : navigation.navigate(nameNavigations) }}
      style={style.touchMenu}
    >
      <View style={style.boxListMenu}>
        {iconImg}
        <Text style={style.txtTitleContent}>
          {txtTitleList}
        </Text>
      </View>
    </TouchableHighlight>
  );
}