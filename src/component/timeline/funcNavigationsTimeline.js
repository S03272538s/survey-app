// =======================================================================
// Init/State Componant
// =======================================================================

import React, { useState, useEffect } from 'react';
import { View, Text, ScrollView, Animated, Dimensions, ActivityIndicator } from 'react-native';
import moment from 'moment';
import { FontAwesome, MaterialCommunityIcons, MaterialIcons } from '@expo/vector-icons';
import { StyleContain } from '../style/funcStyle';
const style = StyleContain();
const { width } = Dimensions.get('window');

import HeaderMenu from '../header/funcHeaderMenu';
import FuncTimeline from './funcTimeline';
// =======================================================================
// Repository Data
// =======================================================================

import { getData, getAccessRight, getPosition, getTimeline } from '../../repository/funcGetData';

// =======================================================================
// Navigations
// =======================================================================
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
const topTab = createMaterialTopTabNavigator();

// =======================================================================
// Render Home Page
// =======================================================================
const timelineWeekly = [
    { title: 'จันทร์' },
    { title: 'อังคาร.' },
    { title: 'พุธ' },
    { title: 'พฤหัส' },
    { title: 'ศุกร์' },
    { title: 'เสาร์' },
    { title: 'อาทิตย์' },
];

export default function NavigationTimeline({ navigation, dataUser }) {
    const [jobTimeline, setJobTimeline] = useState()
    useEffect(() => {
        (async () => {
            setJobTimeline(await getTimeline(dataUser, 'getJobTimeline', ''));
        })();
    },[])
    return (
        <View style={style.viewFlex}>
            <View style={style.viewFlex}>
                <HeaderMenu navigation={navigation} namePage={'ตารางงาน'} />
                <topTab.Navigator
                    initialRouteName={moment(new Date).format('dddd')}
                    tabBarOptions={{
                        labelStyle: { fontSize: 15, fontWeight: 'bold' },
                        tabStyle: { width: width / 3 },
                        activeTintColor: '#ffd506',
                        inactiveTintColor: '#FFF9DB',
                        indicatorStyle: { backgroundColor: '#ffe774' },
                        style: { backgroundColor: '#3865b7', borderTopWidth: 1, borderTopColor: '#ffe774' },
                        scrollEnabled: true,
                        pressColor: '#ffe774'
                    }}
                >
                    {timelineWeekly.map((ele, idx) => (
                        <topTab.Screen
                            key={idx}
                            name={ele.title}
                            options={{
                                tabBarLabel: ele.title,
                            }} >
                            {(props) => jobTimeline ?
                                <FuncTimeline navigation={props.navigation} timeline={jobTimeline[idx]} />
                                :
                                <View style={{ flex: 1, backgroundColor: '#FEFEFE', alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ color: '#292929', fontWeight: 'bold', fontSize: 16, paddingBottom: 20 }}> กำลังดึงข้อมูล... </Text>
                                    <ActivityIndicator size={70} color={'#0580FF'} />
                                </View>
                            }
                        </topTab.Screen>
                    ))}
                </topTab.Navigator>
            </View>
        </View>
    )
};

