// =======================================================================
// Init/State Componant
// =======================================================================

import React, { useState, useEffect } from 'react';
import { View, Text, ScrollView, Animated, Dimensions, } from 'react-native';
import { Button } from 'react-native-paper';
import Timeline from 'react-native-timeline-flatlist'
import { FontAwesome, MaterialCommunityIcons, MaterialIcons } from '@expo/vector-icons';
import { StyleContain } from '../style/funcStyle';
const style = StyleContain();

import HeaderMenu from '../header/funcHeaderMenu';
// =======================================================================
// Repository Data
// =======================================================================

import { getData, getAccessRight, getPosition } from '../../repository/funcGetData';

// =======================================================================
// Render Home Page
// =======================================================================

export default function FuncTimeline({ navigation, timeline, }) {
    const [timeLine, setTimeLine] = useState(timeline);
    
    for (let index = 0; index < 10; index++) {
        if(timeLine[index].description === ''){
            timeLine[index].description = 'ไม่มีข้อมูล';
        }
    }
    return (
        <ScrollView style={{backgroundColor:'#FEFEFE'}}>  
            <Timeline
                // circleColor='rgb(45,156,219)'
                circleSize={20}
                circleColor='#3865b7'
                dotColor='#ffd506'
                lineColor='#3865b7'
                lineWidth={3}
                data={timeLine}
                innerCircle={'dot'}
                timeContainerStyle={{minWidth:72}}
                titleStyle={{display:'flex'}}
                descriptionStyle={{color:'#292929' , borderColor: '#ffe774', borderWidth:2, padding:10, backgroundColor:'#FFF9DB' ,borderRadius:10 , fontWeight: 'bold' , fontSize: 15}}
                timeStyle={{textAlign: 'center', backgroundColor:'#3865b7', color:'white', padding:5, borderRadius:13}}
                options={{
                    style:{paddingTop:50, paddingHorizontal: 20}
                }}
                // columnFormat='two-column'
                // renderFullLine={true}
            />
        </ScrollView>
    )
};