import React, { useState, useEffect } from 'react';
import { Text, View, Image, Platform} from 'react-native';
import { Button } from 'react-native-paper';
import { StyleContain } from '../style/funcStyle';
import PagerView from 'react-native-pager-view';

import Dots from 'react-native-dots-pagination';

import { updateSession } from '../../repository/funcSQLite';

export default function Tutorial({ dataUser, handleClickLogout, dataLogin, handleClickFinishTuto }) {
    const [count, setCount] = useState({ position: 0 });
    const positionPage = (nativeEvent) => {
        count.position = nativeEvent.position;
        setCount({ ...count });
    }
    const handlerFinishTutorial=()=>{
        dataUser.statusTutorial = 1;
        updateSession(1,dataUser,dataLogin);
        handleClickFinishTuto();
    }

    const style = StyleContain();
    return (
        <View style={{ flex: 1 }}>
            <PagerView
                style={style.tutoView}
                initialPage={0}
                onPageSelected={(event) => positionPage(event.nativeEvent)}
            >
                <View key="1" style={style.tutoContainView} >
                    <Text style={style.tutoTxtHeader} >การใช้งานเบื้องต้น Survey</Text>
                    <View style={{ height: '85%', width: '99%', }}>
                        <View style={style.tutoBoxMidImg}>
                            <Text style={style.tutoTxtInBoxImg} >แถบเมนูด้านบน</Text>
                            <Image
                                style={style.tutoimg}
                                source={{ uri: 'http://1.2.175.220/public/img_default_mobile/imgTutorial/TutoStateHeader.png' }}
                            />
                        </View>
                        <View style={style.tutoBoxMidImg}>
                            <Text style={style.tutoTxtInBoxImg} >แถบเมนูด้านล่าง</Text>
                            <Image
                                style={style.tutoimg}
                                source={{ uri: 'http://1.2.175.220/public/img_default_mobile/imgTutorial/TutoStateMenuBar.png' }}
                            />
                        </View>

                    </View>
                </View>
                <View key="2" style={style.tutoContainView}>
                    <Text style={style.tutoTxtHeader} > </Text>
                    <View style={{ height: '85%', width: '99%', }}>
                        <View style={style.tutoBoxMidImg}>
                            <Text style={style.tutoTxtInBoxImg} >หมุดตำแหน่งร้านค้า</Text>
                            <Image
                                style={style.tutoimg}
                                source={{ uri: 'http://1.2.175.220/public/img_default_mobile/imgTutorial/TutoStateMarkerModal.png' }}
                            />
                        </View>
                        <View style={style.tutoBoxMidImg}>
                            <Text style={style.tutoTxtInBoxImg} >ปุ่มเพิ่มช่องกรอกข้อมูล</Text>
                            <Image
                                style={style.tutoimg}
                                source={{ uri: 'http://1.2.175.220/public/img_default_mobile/imgTutorial/TutoStateBtnAppend.png' }}
                            />
                        </View>
                    </View>
                </View>
                <View key="3" style={style.tutoContainView}>
                    <Text style={style.tutoTxtHeader} > </Text>
                    <View style={{ height: '85%', width: '99%', }}>
                        <View style={style.tutoBoxMidImg}>
                            <Text style={style.tutoTxtInBoxImg}>ปุ่มเลือกวันประกันสินค้า</Text>
                            <Image
                                style={style.tutoimg}
                                source={{ uri: 'http://1.2.175.220/public/img_default_mobile/imgTutorial/TutoStateBtnInsurance.png' }}
                            />
                        </View>
                        <View style={style.tutoBoxMidImg}>
                            <Text style={style.tutoTxtInBoxImg} >ลบช่องกรอกข้อมูลที่เพิ่ม/ไม่ต้องการ</Text>
                            <Image
                                style={style.tutoimg}
                                source={{ uri: 'http://1.2.175.220/public/img_default_mobile/imgTutorial/TutoStateBtnDelAppend.png' }}
                            />
                        </View>
                    </View>
                </View>
                <View key="4" style={style.tutoContainView}>
                    <Text style={style.tutoTxtHeader} > </Text>
                    <View style={{ height: '85%', width: '99%', }}>
                        <View style={style.tutoBoxMidImg}>
                            <Text style={style.tutoTxtInBoxImg}>แก้ไขข้อมูล</Text>
                            <Image
                                style={style.tutoimg}
                                source={{ uri: 'http://1.2.175.220/public/img_default_mobile/imgTutorial/TutoStateEditDataTable.png' }}
                            />
                        </View>
                        <View style={style.tutoBoxMidImg}>
                            <Text style={style.tutoTxtInBoxImg} >ลบข้อมูลที่แสดง</Text>
                            <Image
                                style={style.tutoimg}
                                source={{ uri: 'http://1.2.175.220/public/img_default_mobile/imgTutorial/TutoStateDelDataTable.png' }}
                            />
                        </View>
                    </View>
                </View>
                <View key="5" style={style.tutoContainView}>
                    <Button
                        color="#0580FF"
                        // icon="content-save"
                        mode="contained"
                        onPress={()=>handlerFinishTutorial()}
                        style={style.tutoBtnFinish}
                    >
                        <Text style={style.tutoTxtBtnFinish} >เริ่มต้นการใช้งาน</Text>
                    </Button>
                      
                </View>
            </PagerView>
            {Platform.OS ==='android'&&
                <View style={style.tutoViewDot}>
                    <View style={style.tutoDotPosition}>
                        <Dots length={5} active={count.position} />
                    </View>
                </View>
            }
        </View>
    )
}