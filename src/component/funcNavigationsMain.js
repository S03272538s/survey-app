// =======================================================================
// Init/State Componant
// =======================================================================

import React, { useState, useEffect } from 'react';
import { View, Text } from 'react-native';
import { FontAwesome5, MaterialCommunityIcons, MaterialIcons } from '@expo/vector-icons';
import { StyleContain } from './style/funcStyle';
const style = StyleContain();


// =======================================================================
// Import Menu Componant
// =======================================================================

import Home from './home/funcHome';
import Header from './header/funcHeader'
import NavigationSurvey from './survey/funcNavigationSurvey';
import NavigationCheckIn from './checkIn/funcNavigationsCheckIn'; 
import NavigationTimeline from './timeline/funcNavigationsTimeline';
import NavigationLeave from './limitLeave/funcNavigationsLeave';
import NavigationRequestLeave from './requestLeave/funcNavigationsRequestLeave';
import NavigatorMenu from './menu/funcMenu';
import MenuProfile from './menu/funcProfile';
import MenuDetailsApp from './menu/funcDetailApp';
import MenuResetPassword from './menu/funcResetPassword';
import MenuAddLinkAPK from './menu/funcLinkDownloadAPK';
import MenuCheckUpdate from './menu/funcCheckUpdate';
import MenuAbout from './about/funcAboutCompany';
import MenuRequestLeave from './requestLeave/funcRequestLeave';
import Logout from '../login-out/funcLogout';
// import MenuTimeline from './timeline/viewTimeline';
// =======================================================================
// Repository
// =======================================================================
import * as SQLite from 'expo-sqlite';
const db = SQLite.openDatabase("Survey.db");

import { getAccessRight, getAccessRightDev, getAccessJobWeek } from '../repository/funcGetData';

// =======================================================================
// Navigations
// =======================================================================

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
const Tab = createBottomTabNavigator();

import { createDrawerNavigator } from '@react-navigation/drawer';
const Drawer = createDrawerNavigator();
// =======================================================================
// Store Local
// =======================================================================

import { createStore } from 'redux';
const reduxStore = (state, action) => {
    switch (action.type) {
        case 'Add': state = action.playload;
            break;
        default: state = state;
            break;
    }return state;
};

const localStore = createStore(reduxStore, 0);

// =======================================================================
// Home Page Navigation
// =======================================================================

function HomeScreen({ navigation, currentPosition, dataUser, handleClickLogout, showModal}) {
    return (
        <>
            <Header navigation={navigation} dataUser={dataUser} handleClickLogout={handleClickLogout} />
            <View style={style.navigationsHomeView}>
                <Home navigation={navigation} currentPosition={currentPosition} dataUser={dataUser} handleClickLogout={handleClickLogout} />
            </View>
        </>
    );
}
// =======================================================================
// Survey Functions Navigation
// =======================================================================

function NavigationsSurvey({ navigation, dataUser, handleClickLogout }) {
    return (
        <>
            <Header navigation={navigation} dataUser={dataUser} handleClickLogout={handleClickLogout} />
            <NavigationSurvey dataUser={dataUser} handleClickLogout={handleClickLogout} />
        </>
    );
}
// =======================================================================
// CheckIn Functions Navigation
// =======================================================================

function NavigationsCheckIn({ navigation, callBackData, dataUser, currentPosition, handleClickLogout }) {
    return (
        <>
            <Header navigation={navigation} dataUser={dataUser} handleClickLogout={handleClickLogout} />
            <NavigationCheckIn navigation={navigation} callBackData={callBackData} dataUser={dataUser} currentPosition={currentPosition} handleClickLogout={handleClickLogout} />
        </>
    );
}

// =======================================================================
// Navigations Main Componant
// =======================================================================

export default function NavigationMain({ navigationApp, dataUser, currentPosition, handleClickLogout, version }) {
    const [access, setAccess] = useState(true)
    const [accessWeekJob, setAccessWeekJob] = useState(true)
    const [accessDev, setAccessDev] = useState(true)
    
    useEffect(() => {
        (async () => {
            setAccess(await getAccessRight(dataUser, 'getAccessRights', ''));
            setAccessDev(await getAccessRightDev(dataUser, 'getAccessRights', ''));
            setAccessWeekJob(await getAccessJobWeek(dataUser, 'getJobTimeline', ''));

        })();
    }, [])
    return (
        <View style={style.viewFlex}>
            <View style={style.viewFlex}>
                <Drawer.Navigator>
                    <Drawer.Screen
                        name="Home"
                        options={{
                            drawerLabel: 'หน้าหลัก',
                            drawerIcon: ({ color, size }) => (
                                <MaterialCommunityIcons name="home" color={color} size={size} />
                            )
                        }}
                    >
                        {(props) => <HomeScreen navigation={props.navigation} dataUser={dataUser} currentPosition={currentPosition} handleClickLogout={handleClickLogout} />}
                    </Drawer.Screen>
                    <Drawer.Screen
                        name="MenuRequestLeave"
                        options={{
                            drawerLabel: 'ลางาน',
                            drawerIcon: ({ color, size }) => (
                                <MaterialCommunityIcons name="email-send" color={color} size={size} />
                            )
                        }} >
                            
                        {(props) => <NavigationRequestLeave navigation={props.navigation} dataUser={dataUser}  callBackData={props.route}/>}
                    </Drawer.Screen>
                    <Drawer.Screen
                        name="NavigationLeave"
                        options={{
                            drawerLabel: 'ตัวชี้วัด',
                            drawerIcon: ({ color, size }) => (
                                <MaterialCommunityIcons name="speedometer-medium" color={color} size={size} />
                            )
                        }} >
                        {(props) => <NavigationLeave navigation={props.navigation} dataUser={dataUser}/>}
                    </Drawer.Screen>
                    {accessWeekJob === true &&
                    <Drawer.Screen
                        name="MenuTimeline"
                        options={{
                            drawerLabel: 'ตารางงาน',
                            drawerIcon: ({ color, size }) => (
                                <MaterialCommunityIcons name="timetable" color={color} size={size} />
                            )
                        }} >
                        {(props) => <NavigationTimeline navigation={props.navigation} dataUser={dataUser}/>}
                    </Drawer.Screen>
                    }
                    {access === false &&
                        <Drawer.Screen
                            name="Survey"
                            options={{
                                drawerLabel: 'สำรวจ',
                                drawerIcon: ({ color, size }) => (
                                    <MaterialCommunityIcons name="walk" color={color} size={size} />
                                )
                            }}
                        >
                            {(props) => <NavigationsSurvey navigation={props.navigation} dataUser={dataUser} handleClickLogout={handleClickLogout} />}
                        </Drawer.Screen>
                    }
                    <Drawer.Screen
                        name="CheckIn"
                        options={{
                            drawerLabel: 'เข้างาน/ออกงาน',
                            drawerIcon: ({ color, size }) => (
                                <MaterialCommunityIcons name="map-marker-check" color={color} size={size} />
                            )
                        }} >
                        {(props) => <NavigationsCheckIn navigation={props.navigation} callBackData={props.route} navigationApp={navigationApp} dataUser={dataUser} currentPosition={currentPosition} handleClickLogout={handleClickLogout} />}
                    </Drawer.Screen>
                    <Drawer.Screen
                        name="MenuProfile"
                        options={{
                            drawerLabel: 'ข้อมูลพนักงาน',
                            drawerIcon: ({ color, size }) => (
                                <MaterialCommunityIcons name="account" color={color} size={size} />
                            )
                        }} >
                        {(props) => <MenuProfile navigation={props.navigation} param={dataUser} />}
                    </Drawer.Screen>
                    <Drawer.Screen
                        name="MenuResetPass"
                        options={{
                            drawerLabel: 'เปลี่ยนรหัสผ่าน',
                            drawerIcon: ({ color, size }) => (
                                <MaterialCommunityIcons name="lock-reset" color={color} size={size} />
                            )
                        }} >
                        {(props) => <MenuResetPassword navigation={props.navigation} param={dataUser} />}
                    </Drawer.Screen>
                    {accessDev === false &&
                        <Drawer.Screen
                            name="AddLinkAPK"
                            options={{
                                drawerLabel: 'เปลี่ยน ลิ้งดาวโหลด APK',
                                drawerIcon: ({ color, size }) => (
                                    <MaterialIcons name="add-link" color={color} size={size} />
                                )
                            }} >
                            {(props) => <MenuAddLinkAPK navigation={props.navigation} />}
                        </Drawer.Screen>
                    }
                    <Drawer.Screen
                        name="MenuCheckUpdate"
                        options={{
                            drawerLabel: 'ตรวจสอบเวอร์ชัน',
                            drawerIcon: ({ color, size }) => (
                                <MaterialIcons name="update" color={color} size={size} />
                            )
                        }} >
                        {(props) => <MenuCheckUpdate navigation={props.navigation} />}
                    </Drawer.Screen>
                    <Drawer.Screen
                        name="MenuDetailsCompany"
                        options={{
                            drawerLabel: 'เกี่ยวกับบริษัท',
                            drawerIcon: ({ color, size }) => (
                                <MaterialCommunityIcons name="information" color={color} size={size} />
                            )
                        }} >
                        {(props) => <MenuAbout navigation={props.navigation} />}
                    </Drawer.Screen>
                    <Drawer.Screen
                        name="MenuDetailsApp"
                        options={{
                            drawerLabel: 'เกี่ยวกับ',
                            drawerIcon: ({ color, size }) => (
                                <MaterialCommunityIcons name="file-document" color={color} size={size} />
                            )
                        }}>
                        {(props) => <MenuDetailsApp navigation={props.navigation} />}
                    </Drawer.Screen>    
                    <Drawer.Screen
                        name="logout"
                        options={{
                            drawerLabel: 'ออกจากระบบ',
                            drawerIcon: ({ color, size }) => (
                                <MaterialCommunityIcons name="file-document" color={color} size={size} />
                            )   
                        }}
                         >
                        {(props) => 
                             <Logout navigation={props.navigation} dataUser={dataUser} currentPosition={currentPosition} handleClickLogout={handleClickLogout} showModal={true}/>
                        }
                    </Drawer.Screen> 
                </Drawer.Navigator>
            </View>
        </View>
    );
}