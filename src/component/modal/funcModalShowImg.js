// =======================================================================
// Init/State Componant
// =======================================================================

import React from 'react';
import { 
    View, 
    Text, 
    Image,
    TouchableWithoutFeedback, 
} from 'react-native';
import Modal from 'react-native-modal';
import { Button } from 'react-native-paper';
import { StyleContain } from '../style/funcStyle';
const styles = StyleContain();

// =======================================================================
// Render Modal Show Image
// =======================================================================

export default function ModalShowImg({ dataImg, statusVisible, toggle }) {
    return (
        <Modal
            style={styles.modalImgPosition}
            isVisible={statusVisible}
            animationType="slide"
            animationInTiming={500}
            animationOutTiming={500}
            backdropOpacity={0.5}
            propagateSwipe={true}
            customBackdrop={
                <TouchableWithoutFeedback onPress={() => toggle(false)}>
                    <View style={styles.modalImgTouch} />
                </TouchableWithoutFeedback>
            }
        >
            <View style={styles.modalView}>
                <View style={styles.modalContentView}>
                    <Image
                        style={styles.modalImgSize}
                        source={{ uri:dataImg.uri}}
                    />
                </View>
                <View style={styles.modalImgBtnfooter}>
                    <Button
                        mode="outlined"
                        style={styles.modalBtnStyle}
                        onPress={() => toggle(false)}
                        color="#ff4a4a"
                    >
                        <Text style={styles.modalTxtBtnStyle}>ยกเลิก</Text>
                    </Button>
                    <Button
                        mode="outlined"
                        style={styles.modalBtnStyle}
                        onPress={() => toggle(true,dataImg.index)}
                        color="#3ae062"
                    >
                        <Text style={styles.modalTxtBtnStyle}>ลบรูปภาพ</Text>
                    </Button>
                </View>
            </View>
        </Modal>
    )
}