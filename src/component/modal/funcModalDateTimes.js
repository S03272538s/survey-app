// =======================================================================
// Init/State Componant
// =======================================================================

import React from 'react'
import DateTimePickerModal from 'react-native-datetimepicker-modal';
let tempID = '';

// =======================================================================
// Render Modal DatePicker
// =======================================================================

export function DateTimes({id,show,toggle,nowDate,getDatePicker}){
    id.subscribe(()=>{
        tempID= id.getState()
    })
    return(
        <DateTimePickerModal        
            value={nowDate}
            onChange={(event, date) => getDatePicker(date,tempID)}
            show={show}
            toggle={toggle}
        />
    )
}