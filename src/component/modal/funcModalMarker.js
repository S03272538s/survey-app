// =======================================================================
// Init/State Componant
// =======================================================================

import React, { useState, useRef } from 'react';
import {
    View, 
    Text, 
    Image, 
    ScrollView, 
    SafeAreaView,
    TouchableHighlight, 
    TouchableWithoutFeedback, 
} from 'react-native';
import Modal from 'react-native-modal';
import { Button } from 'react-native-paper';
import { FontAwesome5, Entypo, Foundation, MaterialCommunityIcons } from '@expo/vector-icons';
import { StyleContain } from '../style/funcStyle';
import ModalImg from '../modal/funcModalShowImgMarker';
const style = StyleContain();
const pathImg = 'http://1.2.175.220/public/img_survey/';


// =======================================================================
// Call Map Directions Markers
// =======================================================================

import getDirections from "react-native-google-maps-directions";

// =======================================================================
// Render Modal Markers
// =======================================================================

export default function ModalMarker({ currentPosition, dataMarker, statusVisible, toggle }) {
    const refScrollView = useRef(null);
    const [imgName, setImgName] = useState({ imgName: '' });
    const [scrollViewWidth, setScrollViewWidth] = useState(0);
    const [currentXOffset, setCurrentXOffset] = useState({ XOffset: 0 });
    const IconStyle = { size:24 , color: '#2653a5'};

    const handleGetDirections =()=>{
        const data = {
            source:{
                latitude: currentPosition.latitude, //13.9873373,100.6073718
                longitude: currentPosition.longitude
            },
            destination:{
                latitude: dataMarker.latitude, //13.9873399,101.6073718
                longitude: dataMarker.longitude
            },
            params:[
                {
                    key:"travelmode",
                    value: "driving"  // may be "walking", "bicycling" or "transit" as well
                },
                // {
                //     key:"dir_action",
                //     value: "navigate" // this instantly initializes navigation using the given travel mode
                // },
            ]
        }
        getDirections(data)
    }


    let imgNameDB = JSON.parse(dataMarker.img);
        if (imgNameDB !== '') {
            let arrImgName = [];
            imgNameDB.forEach(element => {
                arrImgName.push( pathImg + '"' + element + '"' + '.jpg');
            });
            imgName.imgName = arrImgName;
        }

    const _handleScroll = (event) => {
        var newXOffset = event.nativeEvent.contentOffset.x;
        setCurrentXOffset({ currentXOffset: newXOffset, ...currentXOffset })
    }

    const leftArrow = () => {
        var eachItemOffset = scrollViewWidth;
        var _currentXOffset = currentXOffset - eachItemOffset;
        refScrollView.current.scrollTo({ x: _currentXOffset, y: 0, animated: true });
    }

    const rightArrow = () => {
        var eachItemOffset = scrollViewWidth;
        var _currentXOffset = currentXOffset.XOffset + eachItemOffset.scrollViewWidth;
        refScrollView.current.scrollTo({ x: _currentXOffset, y: 0, animated: true });
    }

    const [statusImgVisible, setStatusImgVisible] = useState(false);
    const [dataImg, setDataImg] = useState([]);

    const handlerClickImg = (item, i) => {
        setStatusImgVisible(true, { ...statusImgVisible })
        setDataImg({uri:item, index:i});
    }
    const handleOnClickImgCallback = (status,index) => {
        setStatusImgVisible(false)
    }

    const renderImage = (item, i) => {
        return (
            <TouchableHighlight
                key={i}
                onPress={() => handlerClickImg(item, i)}
                activeOpacity={0.6}
                underlayColor="#ecf0f1"
            >
                <Image
                    style={style.imgSlideModal}
                    source={{ uri: item }}
                    key={i}
                />
            </TouchableHighlight>
        )
    }

    const handleOnCloseModal=()=>{
        toggle();
    }

    return (
        <Modal
            isVisible={statusVisible}
            style={style.modalMarkers}
            animationType="slide"
            animationInTiming={1000}
            animationOutTiming={1000}
            backdropOpacity={0}
            scrollHorizontal={true}
            propagateSwipe={true}
            customBackdrop={
                <TouchableWithoutFeedback onPress={toggle}>
                    <View style={{ flex: 1 }} />
                </TouchableWithoutFeedback>
            }
        >
            <ModalImg dataImg={dataImg} statusVisible={statusImgVisible} toggle={handleOnClickImgCallback}/>

            <View style={style.positionViewModalMarker}>
                    <View>
                            <View style={{flexDirection: 'row' , margin:20}}>
                                <View style={{flex:1}}/>
                                <TouchableHighlight 
                                style={{alignItems:'flex-end', backgroundColor:'#2653a5', padding: 10, borderRadius:30}}
                                activeOpacity={0.6}
                                underlayColor="#0580FF"
                                onPress={() => {handleGetDirections()}}

                                >
                                <MaterialCommunityIcons name="directions" size={40} color="#FEFEFE" />
                                </TouchableHighlight>
                            </View>
                        {dataMarker.shop ?
                            <View style={style.modalHeaderStyle}>
                                <Entypo name="shop" size={26} color="#fefefe" style={{ paddingLeft: 20 }} />
                                <Text style={style.modalTxtHeaderStyle}>{dataMarker.shop}</Text>
                                <Button style={style.btnCloseModalMarker} onPress={toggle} color="#fefefe">X</Button>
                            </View>
                            :
                            <View style={style.modalHeaderStyle}>
                                <Entypo name="shop" size={26} color="#fefefe" style={{ paddingLeft: 20 }} />
                                <Text style={style.modalTxtHeaderStyle}>ไม่มีชื่อร้าน.. กรุณาตรวจสอบ</Text>
                                <Button style={style.btnCloseModalMarker} onPress={toggle} color="#fefefe">X</Button>
                            </View>
                        }
                        <SafeAreaView style={style.modalMarkerContentView}>
                            <ScrollView>

                            {/* handleGetDirections(); */}
                                {imgName.imgName ?
                                    <View style={style.modalImgSlideView}>
                                        <ScrollView
                                            horizontal={true}
                                            onScroll={_handleScroll}
                                            scrollEventThrottle={16}
                                            ref={refScrollView}
                                            onContentSizeChange={(w, h) => setScrollViewWidth({ scrollViewWidth: w })}
                                        >

                                            {imgName.imgName.map((item, i) => renderImage(item, i))}
                                        </ScrollView>
                                    </View>
                                    : <></>
                                }
                                {dataMarker.name || dataMarker.address || dataMarker.phone || dataMarker.fb || dataMarker.ig || dataMarker.line ?
                                    <>
                                        {dataMarker.name ?
                                            <View style={style.modalTxtBox}>
                                                <View style={style.modalInSizeTxtBox}>
                                                    <FontAwesome5 name="user-alt" size={IconStyle.size} color={IconStyle.color} />
                                                    <Text style={style.modalTxtInSize}>{dataMarker.name}</Text>
                                                </View>
                                            </View>
                                            :
                                            <></>
                                        }
                                        {dataMarker.address ?
                                            <View style={style.modalTxtBox}>
                                                <View style={style.modalInSizeTxtBox}>
                                                    <FontAwesome5 name="map-marked-alt" size={IconStyle.size} color={IconStyle.color} />
                                                    <Text style={style.modalTxtInSize}>{dataMarker.address}</Text>
                                                </View>
                                            </View>
                                            :
                                            <></>
                                        }
                                        {dataMarker.phone ?
                                            <View style={style.modalTxtBox}>
                                                <View style={style.modalInSizeTxtBox}>
                                                    <FontAwesome5 name="phone" size={IconStyle.size} color={IconStyle.color} />
                                                    <Text style={style.modalTxtInSize}>{dataMarker.phone}</Text>
                                                </View>
                                            </View>
                                            :
                                            <></>
                                        }
                                        {dataMarker.fb || dataMarker.ig || dataMarker.line ?
                                            <View style={style.modalTxtBox}>
                                                <View style={style.modalInSizeTxtBox}>
                                                    <ScrollView
                                                        horizontal={true}
                                                    >
                                                        <FontAwesome5 name="share-alt" size={IconStyle.size} color={IconStyle.color} />
                                                        {dataMarker.fb ?
                                                            <View style={style.modalSocialBox}>
                                                                <View style={style.modalSocialIcon}>
                                                                    <FontAwesome5 name="facebook"  size={IconStyle.size} color="darkblue" />
                                                                    <Text style={style.modalSocialTxt}> {dataMarker.fb}</Text>
                                                                </View>
                                                            </View>
                                                            :
                                                            <></>
                                                        }
                                                        {dataMarker.ig ?
                                                            <View style={style.modalSocialBox}>
                                                                <View style={style.modalSocialIcon}>
                                                                    <FontAwesome5 name="instagram"  size={IconStyle.size} color="#405DE6" />
                                                                    <Text style={style.modalSocialTxt}> {dataMarker.ig}</Text>
                                                                </View>
                                                            </View>
                                                            :
                                                            <></>
                                                        }
                                                        {dataMarker.line ?
                                                            <View style={style.modalSocialBox}>
                                                                <View style={style.modalSocialIcon}>
                                                                    <FontAwesome5 name="line"  size={IconStyle.size} color="green" />
                                                                    <Text style={style.modalSocialTxt}> {dataMarker.line}</Text>
                                                                </View>
                                                            </View>
                                                            :
                                                            <></>
                                                        }
                                                    </ScrollView>
                                                </View>
                                            </View>
                                            :
                                            <></>
                                        }
                                    </>
                                    :
                                    <View style={{alignItems:'center'}}>
                                        <Foundation name="alert" size={50} color="#ecc400" />
                                        <Text style={style.modalNullValue}>ไม่มีข้อมูล</Text>
                                        <Text style={style.modalNullValue}>กรุณาตรวจสอบข้อมูลของร้านค้า</Text>
                                    </View>
                                }
                            </ScrollView>
                        </SafeAreaView>
                    </View>
            </View>
        </Modal>
    )
};