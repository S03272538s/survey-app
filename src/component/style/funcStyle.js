export function StyleContain() {
    return {
        //Logo Login
        logoLogin: {
            width: "70%",
            height: 150,
            justifyContent: "center",
            paddingTop: 15,
            position: 'relative'
        },
        // Login Style
        viewLoginContainer: {
            flex: 1,
            backgroundColor: '#ecf0f1',
        },
        viewLoginHeader: {
            paddingTop: 150,
            paddingBottom: 70,
            alignItems: 'center',
        },
        viewContainer: {
            alignItems: 'center',
            justifyContent: 'center',
        },
        viewCardLogin: {
            width: "80%",
            borderRadius: 20,
            justifyContent: 'center',
            opacity: 1,
            backgroundColor: '#ecf0f1',
            //Shadow IOS
            shadowColor: '#000',
            shadowOffset: { width: 1, height: 3 },
            shadowOpacity: 0.2,
            //Shadow Android
            elevation: 10,
            marginBottom: 30,
            paddingBottom: 20
        },
        btnLogin: {
            height: 60,
            width: '80%',
            justifyContent: 'center',
            borderRadius: 20,
        },
        viewBtnLoginStyle: {
            flex: 1,
            width: '100%',
            borderRadius: 20,
        },
        btnLoginStyle: {
            height: '100%',
            width: '100%',
            borderRadius: 20,
            justifyContent: 'center',
        },
        btnTxtLoginStyle: {
            backgroundColor: 'transparent',
            fontSize: 15,
            color: '#fff',
        },
        viewTxtFooterLogin: {
            width: '100%',
            alignItems: 'center',
            justifyContent: 'center',
            height: 20,
            bottom: 0,

        },
        txtFooterLogin: {
            color: '#292929', fontSize: 8
        },
        bgLoginLinearStyle: {
            position: 'absolute',
            left: 0,
            right: 0,
            top: 0,
            height: 300,
        },
        bgLoginLinearColor: ['#2653a5', 'transparent'],
        bgBtnLoginLinearColor: ['#4c669f', '#3b5998', '#192f6a'],

        //Logout Style
        bgLogoutStyle: {
            // backgroundColor:'#ffd506',
        },
        txtLogoutStyle: {
            fontSize: 12
        },
        btnLogoutStyle: {
            height: '100%',
            justifyContent: 'center',
        },

        // Header Style
        viewIconStyle: {
            flex:1,
            // margin: 20,
            borderWidth: 3,
            borderColor: '#ffe774',
            backgroundColor: '#ebf5fe',
            borderRadius: 30,
            alignItems:'flex-end',
        },
        viewHeaderStyle: {
            paddingTop: 30,
            shadowColor: '#000000',
            shadowOffset: { width: 0, height: 2 },
            shadowOpacity: 0.2,
            elavation: 10,
            height: 100,
        },
        txtHeaderStyle: {
            color: '#fafafa',
            fontSize: 17,
            fontWeight: "bold",
            marginRight: 20,
            textShadowColor: '#1d3f7d',
            textShadowOffset: { width: 5, height: 2 },
            textShadowRadius: 10,
        },
        txtTitleContent: {
            color: '#292929',
            fontSize: 16,
            fontWeight: 'bold',
            // textShadowColor: '#1d3f7d', 
            // textShadowOffset: { width: 2, height: 1 }, 
            // textShadowRadius: 2,
        },
        headerViewRight: {
            flex: 1,
            // alignItems: 'flex-start',
            paddingLeft:20
        },
        headerStyle: {
            paddingTop: 500,
            backgroundColor: '#2653a5',
        },
        headerTitleStyle: {
            fontSize: 18,
            fontWeight: 'bold',
        },
        headerTintColor: '#fefefe',
        standaloneHeaderRowFront: {
            flexDirection: 'row',
            alignItems: 'center',
            paddingHorizontal: 20,
            // justifyContent: 'center',
            height: '100%',
            // borderWidth:1,

        },
        standaloneHeaderRowBack: {
            alignItems: 'center',
            backgroundColor: '#CCC',
            flexDirection: 'row',
            justifyContent: 'flex-end',
        },

        // LinearGradient
        LinearStyle: {
            position: 'absolute',
            left: 0,
            right: 0,
            top: 0,
            height: 70,
        },
        LinearColor: ['#2653a5', '#356bcf'],

        // Navigations
        navigationsView: {
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
        },
        navigationsHomeView: {
            flex: 1,
            // borderWidth:2,
            // justifyContent: 'center',
        },
        navigationsCheckInView: {
            flex: 1,
            paddingLeft: 10,
            paddingRight: 10,
            // justifyContent: 'center',
            // borderWidth:2,
            backgroundColor: '#FFFFFF'
        },

        // PAGE MapView
        mapViewStyle: {
            height: '100%',
            width: '100%',
        },

        //PAGE Form Input
        viewInputStyle: {
            backgroundColor: '#FFF',
            height: '100%',
            width: '100%',
            Top: 0,
            shadowColor: '#000000',
            shadowOffset: { width: 0, height: 2 },
            shadowOpacity: 0.2,
            elavation: 2,
            position: 'relative'
        },
        styleInput: {
            colors: {
                text: '#292929',
                primary: '#3b5998',
                underlineColor: 'transparent',
                background: 'transparent',
            }
        },
        inputStyle: {
            marginTop: 10,
            marginBottom: 10,
            borderRadius: 20,
            padding: 10,
        },
        countBadge: {
            paddingHorizontal: 8.6,
            paddingVertical: 5,
            borderRadius: 50,
            position: 'absolute',
            right: 3,
            bottom: 3,
            justifyContent: 'center',
            backgroundColor: '#0580FF'
        },
        countBadgeText: {
            fontWeight: 'bold',
            alignSelf: 'center',
            padding: 'auto',
            color: '#ffffff'
        },

        // DropDown
        itemStyle: {
            justifyContent: 'flex-start'
        },
        containerStyle: {
            height: 60,
            marginTop: 10,
        },
        boxDropDownStyle: {
            borderWidth: 1,
            borderColor: '#4c669f'
        },
        boxDropDownRowStyle: {
            flex: 1,
            borderWidth: 1,
            borderColor: '#4c669f',
        },
        dropDownStyle: {
            backgroundColor: '#fff'
        },

        // USE Multi Page
        viewFlex: {
            flex: 1
        },
        btnStyle: {
            justifyContent: 'center',
            marginTop: 20,
            marginBottom: 20,
            height: 60
        },
        btnTxt: {
            fontSize: 16,
            color: '#fff',
            fontWeight: 'bold',
        },
        btnDelete: {
            alignItems: 'flex-end',
            paddingRight: 20,
            justifyContent: 'center',
            height: '100%',
            borderColor: '#ff4a4a',
            borderTopRightRadius: 20,
            borderBottomRightRadius: 20,
            width: 60
        },
        btnViewLine: {
            flex: 1,
            borderColor: '#4c669f',
            borderBottomWidth: 1,
            alignItems: 'center',
            justifyContent: 'center',
            marginBottom: 10,
            width: "100%"
        },

        // Scroll View
        ScrollViewStyle: {
            backgroundColor: '#FFF',
            Top: 0,
            paddingLeft: 20,
            paddingRight: 20,
            shadowColor: '#000000',
            shadowOffset: { width: 0, height: 2 },
            shadowOpacity: 0.2,
            elavation: 2,
            position: 'relative'
        },
        ScrollViewDataTableStyle: {
            backgroundColor: '#FFF',
            paddingLeft: 20,
            paddingRight: 20,
            shadowColor: '#000000',
            shadowOffset: { width: 0, height: 2 },
            shadowOpacity: 0.2,
            elavation: 2,
            position: 'relative'
        },

        // FlatList
        flatListStyle: {
            backgroundColor: '#FFF',
            Top: 0,
            paddingLeft: 20,
            paddingRight: 20,
            shadowColor: '#000000',
            shadowOffset: { width: 0, height: 2 },
            shadowOpacity: 0.2,
            elavation: 2,
            position: 'relative'
        },

        // SwipeRow
        standaloneRowBack: {
            flex: 1,
            alignItems: 'center',
            flexDirection: 'row',
            justifyContent: 'flex-end',
            borderRadius: 20,
            height: '100%'
        },
        standaloneRowFront: {
            justifyContent: 'center',
            backgroundColor: '#fff'
        },

        // Icon Style
        iconStyle: {
            height: 50,
            width: 50
        },

        // IMG Style
        imgSlideModal: {
            height: 150,
            width: 150,
            margin: 2,
        },

        // ModalMarkers
        modalMarkers: {
            marginBottom: 0,
            marginRight: 0,
            marginLeft: 0,
            justifyContent: "flex-end",
        },
        modalHeaderStyle: {
            flexDirection: "row",
            backgroundColor: "#2653a5",
            // height: 50,
            borderTopLeftRadius: 20,
            borderTopRightRadius: 20,
            alignItems: 'center',
            justifyContent: 'center',
            elevation: 3,
        },
        modalTxtHeaderStyle: {
            paddingLeft: 20,
            color: '#fefefe'
        },
        btnCloseModalMarker: {
            flex: 1,
            backgroundColor: '#2653a5',
            borderRadius: 20,
            alignItems: 'flex-end',
        },
        modalNullValue: {
            color: '#d4d4d4',
            fontWeight: 'bold',
            fontSize: 16,
        },
        txtHeaderModalMarker: {
            color: '#fefefe',
            fontWeight: "bold",
            fontSize: 16,
            paddingLeft: 20
        },
        positionViewModalMarker: {
            justifyContent: "flex-end",
        },
        modalMarkerContentView: {
            paddingTop: 5,
            backgroundColor: "white",
            height: 300,
            shadowColor: "#000",
            shadowOffset: {
                width: 0,
                height: 2
            },
            shadowOpacity: 1,
            shadowRadius: 4,
            elevation: 2,
        },
        modalImgSlideView: {
            flexDirection: "row",
            paddingBottom: 40,
        },
        modalTxtBox: {
            flex: 1,
            paddingLeft: 10,
            marginBottom: 20
        },
        modalInSizeTxtBox: {
            flexDirection: 'row',
            paddingLeft: 20,
        },
        modalTxtInSize: {
            flex: 1,
            fontSize: 14,
            paddingTop: 10,
            marginLeft: 20,
            marginRight: 20,
            borderBottomWidth: 1,
            borderBottomColor: '#c6d5f1',
        },
        modalSocialBox: {
            flex: 1,
            flexDirection: 'row',
            marginLeft: 20,
        },
        modalSocialIcon: {
            borderWidth: 1,
            borderColor: '#c6d5f1',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
            borderRadius: 20,
            backgroundColor: '#f5f7fc'
        },
        modalSocialTxt: {
            fontSize: 16,
            marginLeft: 10,
            marginRight: 10,
        },

        // Modal Image
        modalImgPosition: {
            alignItems: 'center',
        },
        modalImgTouch: {
            flex: 1,
            backgroundColor: '#000',
        },
        modalView: {
            flex: 1,
            justifyContent: "center",
            paddingBottom: 50,
            width: '90%',
        },
        modalContentView: {
            backgroundColor: "white",
            borderTopLeftRadius: 10,
            borderTopRightRadius: 10,
            justifyContent: 'center',
            alignItems: 'center',
            height: 450,
            shadowColor: "#000",
            shadowOffset: {
                width: 0,
                height: 2
            },
            shadowOpacity: 0.25,
            shadowRadius: 4,
            elevation: 5
        },
        modalImgSize: {
            height: 400,
            width: '100%',
        },
        button: {
            borderRadius: 20,
            elevation: 2
        },
        textStyleModal: {
            color: "white",
            fontWeight: "bold",
            textAlign: "center",
        },
        modalText: {
            textAlign: "center"
        },
        modalBtnStyle: {
            justifyContent: 'center',
            height: '100%',
            width: '50%',
        },
        modalTxtBtnStyle: {
            fontWeight: "bold",
            textAlign: "center"
        },
        modalImgBtnfooter: {
            flexDirection: 'row',
            backgroundColor: '#FFF',
            height: 50,
            borderBottomLeftRadius: 10,
            borderBottomRightRadius: 10,
        },
        background: {
            position: 'absolute',
            left: 0,
            right: 0,
            top: 0,
            height: 2,
        },
        btn_alert: {
            height: 500,
            borderRadius: 50
        },

        //Modal Alret
        modalAlretView: {
            flex: 1,
            justifyContent: "center",
            paddingBottom: 50,
            width: '70%',
        },
        modalAlretContant: {
            backgroundColor: "white",
            borderTopLeftRadius: 10,
            borderTopRightRadius: 10,
            padding: 20,
            height: 50,
            shadowColor: "#000",
            shadowOffset: {
                width: 0,
                height: 2
            },
            shadowOpacity: 0.25,
            shadowRadius: 4,
            elevation: 5
        },
        modalAlretTxt: {
            marginBottom: 15,
            textAlign: "center"
        },
        viewBtnAlret: {
            backgroundColor: '#FFF',
            height: 50,
            borderBottomLeftRadius: 10,
            borderBottomRightRadius: 10,
        },
        viewTwinBtnAlret: {
            flexDirection: 'row',
            backgroundColor: '#FFF',
            height: 50,
            borderBottomLeftRadius: 10,
            borderBottomRightRadius: 10,
        },
        btnAlretStyle: {
            justifyContent: 'center',
            height: '100%',
            width: '100%',
        },
        btnTwinAlretStyle: {
            justifyContent: 'center',
            height: '100%',
            width: '50%',
        },
        btnAlretTxt: {
            fontWeight: "bold",
            textAlign: "center"
        },

        // Camera Style
        cameraView: {
            flex: 1
        },
        cameraBtnContainer: {
            flex: 1,
            backgroundColor: 'transparent',
            flexDirection: 'row',
            margin: 20,
        },
        cameraBtnSwitchCamera: {
            flex: 0.25,
            alignSelf: 'flex-end',
            marginTop: 10,
            marginRight: 10,
            justifyContent: 'center'

        },
        cameraBtnTakePhoto: {
            flex: 1,
            // borderWidth:1,
            marginBottom: 10,
            padding: 10,
            borderRadius: 50,
            // borderColor: '#FFFFFF',
            alignSelf: 'center',
            justifyContent: 'center'
        },
        cameraBtnText: {
            fontSize: 18,
            color: 'white',
        },
        // CheckIn Style

        CheckInHeader: {
            marginTop: 20,
            marginBottom: 20,
            // borderWidth:2,
            flexDirection: 'row',
            alignItems: 'center',
        },
        checkInBoxImg: {
            flex: 1,
            borderWidth: 1,
            borderRadius: 10,
            borderColor: '#d4d4d4',
            alignItems: 'center',
            justifyContent: 'center',
            width: '80%'
        },
        checkInHeaderTxt: {
            fontSize: 16,
            fontWeight: 'bold',
            color: '#292929',
        },
        txtHeadInBoxCamera: {
            color: '#d5d5d5',
            fontWeight: 'bold',
            fontSize: 16
        },
        txtContentInBoxCamera: {
            color: '#d4d4d4',
            fontWeight: 'bold',
            fontSize: 14
        },
        checkInImgSize: {
            height: 400,
            width: '100%',
            borderRadius: 10,
        },
        // CheckList Style
        checkListContent: {
            flexDirection: "row",
            marginBottom: 10,
            alignItems: "center",
        },
        checkListTxtAlpha: {
            marginLeft: 10,
            color: '#777777',
        },

        // ChekIn/Out Data Table Style
        txtAlphaDataTable: {
            marginLeft: 10,
            color: '#777777',
        },

        // Page Profiles
        txtBoxShowData: {
            flex: 0.5,
            borderBottomWidth: 1,
            borderBottomColor: '#FFF9DB',
            padding: 10,
            width: '100%'
        },

        // ListMenu 
        touchMenu: {
            marginLeft: 20,
            marginRight: 20,
            // borderBottomWidth: 1, 
            marginBottom: 20
        },
        boxListMenu: {
            flex: 1,
            alignItems: 'flex-end',
            height: 40,
            flexDirection: 'row'
        },
        
        // Style Reset location
        touchReset: {
            flex: 1,
            borderWidth: 1,
            borderRadius: 10,
            borderColor: '#ffd506',
            backgroundColor: '#FFF9DB',
            alignItems: 'center',
            justifyContent: 'center',
            width: '80%',
            marginBottom: 20,
        },

        // Page Feed News
        viewFeedContent: {
            flex: 1,
            // flexDirection: "row",
            backgroundColor: '#ffff',
            // padding: 5
        },
        touchFeed: {
            borderWidth: 2,
            borderRadius: 10,
            borderColor:'#4c669f',
            marginBottom: 10,
        },
        viewFeedImgContent: {
            flex: 1,
            // borderWidth: 1,
            alignItems: 'center',
        },
        viewFeedTitleContent: {
            paddingLeft: 10,
            paddingTop: 10,
        },
        viewFeedIntrolContent: {
            padding: 5,
        },
        viewFeedMore: {
             alignItems:'flex-end', 
             marginBottom:10, 
             marginRight:10,
        },
        txtFeedTitle: {
            fontSize: 16,
            color: '#292929',
        },
        txtFeedIntrol: {
            fontSize: 14,
            color: '#828282'
        },
        txtFeedContent: {
            fontSize: 14,
            color: '#d4d4d4'
        },
        // LeaveRequest
        txtLeaveIntrol: {
            fontSize: 14,
            color: '#292929'
        },
        txtLeaveCancel: {
            fontSize: 14,
            color: '#ff4a4a'
        },
        txtLeaveAppprove: {
            fontSize: 14,
            color: '#3ae062'
        },
        viewLeaveStatus: {
            alignItems:'center', 
            marginBottom:10, 
            marginRight:10,
       },
        // Page Emty Feed
        viewFeedEmtyContent: {
            flex: 1,
            padding: 5,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: '#fefefe',
        },
        txtFeedEmty: {
            fontSize: 14,
            color: '#d4d4d4'
        },
        // PAGE Tutorial
        tutoView: {
            flex: 1,
            backgroundColor: '#fefefe',
            // alignItems: 'center',
            // justifyContent: 'center'
        },
        tutoContainView: {
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
        },
        tutoTxtHeader: {
            fontSize: 18,
            fontWeight: 'bold',
            color: '#292929',

        },
        tutoTxtInBoxImg: {
            textAlign: 'center',
            fontSize: 16,
            fontWeight: 'bold',
            color: '#292929',

        },
        tutoTxtBtnFinish: {
            textAlign: 'center',
            fontSize: 16,
            fontWeight: 'bold',
            color: '#fefefe',
        },
        tutoBtnFinish: {
            justifyContent: 'center',
            marginTop: 20,
            marginBottom: 20,
            height: 60,
            borderRadius: 20,
        },
        tutoimg: {
            width: '100%',
            height: '100%',
            resizeMode: 'contain',
        },
        tutoBoxSmallImg: {
            padding: 5,
            margin: 2,
            height: '25%',
            borderWidth: 2,
            borderRadius: 20,
            borderColor: '#4c669f',
        },
        tutoBoxMidImg: {
            padding: 5,
            margin: 2,
            height: '50%',
            borderWidth: 2,
            borderRadius: 20,
            borderColor: '#4c669f',
        },
        tutoBoxBigImg: {
            padding: 5,
            margin: 2,
            height: '100%',
            borderWidth: 2,
            borderRadius: 20,
            borderColor: '#4c669f',
        },
        tutoDotPosition: {
            flex: 1,
            justifyContent: 'flex-end',
        },
        tutoViewDot: {
            height: '100%',
            width: '100%',
            position: 'absolute',
        },
    }
}