// =======================================================================
// Init/State Componant
// =======================================================================

import React, { useState, useEffect } from 'react';
import { Text, View, ScrollView, Image, Linking } from 'react-native';
import HeaderMenu from '../header/funcHeaderMenu';
import Constants from 'expo-constants';
import { StyleContain } from '../style/funcStyle';
import Logout from '../../login-out/funcLogout';

const style = StyleContain();

// =======================================================================
// Repository Data
// =======================================================================

import * as SQLite from 'expo-sqlite';
const db = SQLite.openDatabase("Survey.db");

// =======================================================================
// NavigatorMenu Componant
// =======================================================================

export default function MenuAbout({ navigation, param }) {
    return (
        <View style={{ flex: 1 }}>
            <HeaderMenu navigation={navigation} namePage={'เกี่ยวกับ'} />
            <ScrollView style={{ flex: 1 }}>
                    <View>
                        <View style={{
                            flex: 0.1,
                            alignItems: 'center',
                            paddingTop: 20,
                            paddingBottom: 20,
                            borderBottomWidth: 1,
                            borderBottomColor: '#FFF9DB',
                        }}
                        >
                            <Text style={style.txtTitleContent}>วิสัยทัศน์ พันธกิจ </Text>
                            <Text style={style.txtTitleContent}>สารจากประธานกรรมการ </Text>
                            <Text style={style.txtTitleContent}>โครงสร้างองค์กร  </Text>
                            <Text style={style.txtTitleContent}>โครงสร้างบริษัท  </Text>
                        </View>
                        <View style={{ flex: 1, alignItems: 'center', }}>
                        </View>
                    </View>
            </ScrollView>
        </View>
    );
}