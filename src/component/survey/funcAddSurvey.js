// =======================================================================
// Init/State Componant
// =======================================================================

import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    Image,
    FlatList,
    Platform,
    ScrollView,
    RefreshControl,
    TouchableOpacity,
    ActivityIndicator,
    TouchableHighlight
} from 'react-native';
import Header from '../header/funcHeader';
import ModalAlert from '../../login-out/funcModalLogout';
import { SwipeRow } from 'react-native-swipe-list-view';
import { Button, TextInput, } from 'react-native-paper';
import { FontAwesome5, FontAwesome, MaterialIcons } from '@expo/vector-icons';
import { StyleContain } from '../style/funcStyle';
const style = StyleContain();

// =======================================================================
// Navigation
// =======================================================================

import * as Location from 'expo-location';
import { CommonActions } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
const Stack = createStackNavigator();

// =======================================================================
// DatePicker
// =======================================================================

import moment from 'moment';
import { createStore } from 'redux';
import { DateTimes } from '../modal/funcModalDateTimes'
import DropDownPicker from 'react-native-dropdown-picker';

const reduxStore = (state, action) => {
    switch (action.type) {
        case 'Add': state = action.playload;
            break;
        default: state = state;
            break;
    }
    return state;
};
const localStore = createStore(reduxStore, 0);
const dataUserStore = createStore(reduxStore, 0);

const nowDate = new Date();

// =======================================================================
// ImagePicker
// =======================================================================

import * as ImagePicker from 'expo-image-picker';
import { ImageBrowser } from 'expo-image-picker-multiple';
import * as ImageManipulator from 'expo-image-manipulator';

// =======================================================================
// Repository Data
// =======================================================================

import { getData } from '../../repository/funcGetData';
import { postDatas } from '../../repository/funcPostData';

// =======================================================================
//  ShowDataScreen
// =======================================================================

function DataScreen({ navigation, route }) {
    // State Value Form Input Data
    const [value, setValue] = useState({
        user_id: '',
        type_id: '',
        type_name: '',
        data_name: '',
        data_shop: '',
        data_address: '',
        province_id: '',
        district_id: '',
        data_phone: '',
        data_line: '',
        data_facebook: '',
        data_instagram: '',
        machine_id: '',
        material_id: '',
        img_name: '',
        data_etc: '',
        latitude: '',
        longitude: '',
    });

    // State/Value ON ShowData
    const [showType, setType] = useState();
    const [showTypeMachine, setTypeMachine] = useState();
    const [showTypeMaterial, setTypeMaterial] = useState();
    const [showDistricts, setDistricts] = useState();
    const [showProvinces, setProvinces] = useState();

    // State Value Store
    const [dataUserId, setDataUserId] = useState(dataUserStore.getState());

    /// State/Value ON DateTime
    const [currentDate, setCurrentDate] = useState();
    const [pickDate, setPickDate] = useState([{ id: 0, date: '' }]);
    const [show, showModal] = useState(false);
    const toggle = () => showModal(!show);

    //State IMG upload
    const [image, setImage] = useState({ img: [] });

    // State Alert Complete
    const [statusVisible, setStatusVisible] = useState(false);

    // State Refresh Page
    const [refreshing, setRefreshing] = useState(false);

    // Input Hide
    const [showInputType, setShowInputType] = useState('none');
    const [showInputMachine, setShowInputMachine] = useState('none');
    const [showInputMaterial, setShowInputMaterial] = useState('none');
    // Render Once Time On Page
    useEffect(() => {
        getDataAll();
        getCurrentDate();
        (async () => {
            let { status } = await Location.requestForegroundPermissionsAsync();
            if (status !== 'granted') {
                setErrorMsg('Permission to access location was denied');
                return;
            }
            let location = await Location.getCurrentPositionAsync({});
            value.latitude = location.coords.latitude;
            value.longitude = location.coords.longitude;
        })();
        (async () => {
            if (Platform.OS !== 'web') {
                const { statusImg } = await ImagePicker.requestMediaLibraryPermissionsAsync();
                if (statusImg !== 'granted') {
                    // alert('กรุณา เปิดอนุญาติการเข้าถึงรูปภาพ');
                }
            }
        })();
    }, []);

    // Get All Data By Call Api
    const getDataAll = async () => {
        let arrProvinces = [];
        //  GET Data Selection Box
        const getType = await getData('getType', '');
        const getTypeMachine = await getData('getTypeMachine', '');
        const getTypeMaterial = await getData('getTypeMaterial', '');
        const getProvinces = await getData('getProvinces', '');

        getProvinces.forEach((element) => {
            arrProvinces.push({
                label: element.provinces_name,
                value: element.provinces_id
            });
        });

        setType(setValueSelection(getType));
        setTypeMachine(setValueSelection(getTypeMachine));
        setTypeMaterial(setValueSelection(getTypeMaterial));
        setProvinces(arrProvinces);

    };
    const getDistricts = async (provinces) => {
        let arrDistricts = [];
        //  GET Data Selection Box

        const getDistricts = await getData('getDistricts', '');
        getDistricts.forEach((element) => {
            if (element.provinces_id == provinces.value) {
                arrDistricts.push({
                    label: element.districts_name,
                    value: element.districts_id
                });
            }
        });
        setDistricts(arrDistricts);
    };
    const setValueSelection = (arr) => {
        let arrData = [];
        arr.forEach((element) => {
            arrData.push({
                label: element.name,
                value: element.id
            });
        });
        arrData.push({ value: 'etc', label: 'อื่นๆ' });
        return arrData;
    };

    // Reload Page
    const wait = (timeout) => { return new Promise(resolve => setTimeout(resolve, timeout)) };
    const onRefresh = React.useCallback(() => { wait(2000).then(() => setRefreshing(false)) }, []);

    // Get DateTime/Date Picker
    const getCurrentDate = () => {
        var date = new Date().getDate();
        var month = new Date().getMonth() + 1;
        var year = new Date().getFullYear();
        setCurrentDate({
            format: date + '-' + month + '-' + year,//format: dd-mm-yyyy;
            day: date,
            month: month,
            year: year
        });
    };

    const getDate = (index) => {
        localStore.dispatch({ type: 'Add', playload: index })
        toggle();
        return;
    };

    const getDatePicker = (date, id) => {
        let arrPickDate = [];
        pickDate.forEach((element, i) => {
            if (pickDate[i].id == id) {
                pickDate[i].date = date;
            }
            arrPickDate.push(element);
        });
        setPickDate(arrPickDate);
    }

    /// APPEND MACHINE START.
    const [addMachine, setAddMachine] = useState({
        viewItem: [{
            itemMachine: 0,
            sur_id: dataUserId.dataUser.id_application,
            machine_type_id: '',
            machine_type: '',
            machine_name: '',
            machine_insurance: '',
        }]
    });
    const addViewMachine = () => {
        addMachine.viewItem.push({ itemMachine: addMachine.viewItem.length, machine_insurance: '' }); // push Array to State Value
        pickDate.push({ id: addMachine.viewItem.length - 1, date: '' });
        setAddMachine({ viewItem: addMachine.viewItem }); // SetState value{ item:(Array) }
    };
    const delViewMachine = (index) => () => {
        addMachine.viewItem.splice(index, 1);   // Remove array.splice(index,countRemove);
        setAddMachine({ viewItem: addMachine.viewItem });   // SetState value{ item:(Array) }
    };

    /// APPEND MATERAIL START.
    const [addMaterial, setAddMaterial] = useState({
        viewItem: [{
            itemMaterial: 0,
            sur_id: dataUserId.dataUser.id_application,
            material_type_id: '',
            material_type: '',
            material_name: '',
            material_price: '',
            showInputHide:'none',
        }]
    });
    const addViewMaterial = () => {
        addMaterial.viewItem.push({ itemMaterial: addMaterial.viewItem.length }); // push Array to State Value
        setAddMaterial({ viewItem: addMaterial.viewItem }); // SetState value{ item:(Array) }
    }
    const delViewMaterial = (index) => () => {
        addMaterial.viewItem.splice(index, 1);   // Remove array.splice(index,countRemove);
        setAddMaterial({ viewItem: addMaterial.viewItem });   // SetState value{ item:(Array) }
    }

    // FuncIMG
    if (route.params !== undefined) {
        image.img = route.params.photos;
    }
    const handlerClickImg = (item, i) => {
        // console.log(image);
    }
    const renderImage = (item, i) => {
        return (
            <TouchableHighlight
                key={i}
                onPress={() => handlerClickImg(item, i)}
                activeOpacity={0.6}
                underlayColor="#ecf0f1"
            >
                <View style={{ flex: 1, borderWidth: 0.5, marginRight: 10 }}>
                    <Image
                        style={{ height: 200, width: 200 }}
                        source={{ uri: item.uri }}
                        key={i}
                    />
                </View>
            </TouchableHighlight>
        )
    }

    // Show hide box type etc
    const chageType = (typeItem, index) => {
        value.type_id = typeItem.value;
        if (typeItem.value === 'etc') {
            setShowInputType('flex');
        }else{
            setShowInputType('none');
        }
    }
    const chageMachine = (machineItem, index) => {
        addMaterial.viewItem[index].material_type_id = machineItem.value
        value.type_id = machineItem.value;
        // if (typeItem.value === 'etc') {
        //     setShowInputType('flex');
        // }
    }

    // Set localstore
    let TempId = '';
    localStore.subscribe(() => {
        TempId = localStore.getState()
    })

    // Functions Chack Insert Data
    const chkStatusSave = () => { setStatusVisible(true, { ...statusVisible }) };
    const handleOnClickSaveCallback = (status) => {
        setStatusVisible(false);
        if (status === true) {
            postData();
            navigation.dispatch(
                CommonActions.reset({
                    index: 1,
                    routes: [{
                        name: 'Add Survey',
                    }],
                })
            )
        }
    }
    // POST Data To API For Insert To DB
    const postData = async () => {
        let arrMachineTypeId = [], arrMachineType = [], arrMachineName = [], arrMachineInsurance = [];
        let arrMaterialTypeId = [], arrMaterialType = [], arrMaterialName = [], arrMaterialPrices = [];
        // Loop Data For Insert Data Machine
        addMachine.viewItem.forEach((machine, i) => {
            arrMachineTypeId.push(machine.machine_type_id);
            arrMachineType.push(machine.machine_type);
            arrMachineName.push(machine.machine_name);
            arrMachineInsurance.push(pickDate[i].date);
        });
        // Loop Data For Insert Data Machine
        addMaterial.viewItem.forEach(material => {
            arrMaterialTypeId.push(material.material_type_id);
            arrMaterialType.push(material.material_type);
            arrMaterialName.push(material.material_name);
            arrMaterialPrices.push(material.material_price);
        });
        // Make Data For Insert Data Machine
        const data_machine = {
            action: "INSERT",
            id: '', // SEND ID WHEN UPDATE DATA 
            sql: {
                sur_id: {
                    val: dataUserId.dataUser.id_application,
                    type: "INTEGER"
                },
                machine_type_id: {
                    val: JSON.stringify(arrMachineTypeId),
                    type: "TEXT"
                },
                machine_type: {
                    val: JSON.stringify(arrMachineType),
                    type: "TEXT"
                },
                machine_name: {
                    val: JSON.stringify(arrMachineName),
                    type: "TEXT"
                },
                machine_insurance: {
                    val: JSON.stringify(arrMachineInsurance),
                    type: "TEXT"
                },
                active: {
                    val: true,
                    type: "BOOLEAN"
                }
            }
        };
        const data_material = {
            action: "INSERT",
            id: '', // SEND ID WHEN UPDATE DATA 
            sql: {
                sur_id: {
                    val: dataUserId.dataUser.id_application,
                    type: "INTEGER"
                },
                material_type_id: {
                    val: JSON.stringify(arrMaterialTypeId),
                    type: "TEXT"
                },
                material_type: {
                    val: JSON.stringify(arrMaterialType),
                    type: "TEXT"
                },
                material_name: {
                    val: JSON.stringify(arrMaterialName),
                    type: "TEXT"
                },
                material_price: {
                    val: JSON.stringify(arrMaterialPrices),
                    type: "TEXT"
                },
                active: {
                    val: true,
                    type: "BOOLEAN"
                }
            }
        };
        postDatas(value, data_machine, data_material, image.img, dataUserId);
    }

    return (
        <ScrollView
            style={style.ScrollViewStyle}
            refreshControl={
                <RefreshControl
                    refreshing={refreshing}
                    onRefresh={onRefresh}
                />
            }
        >
            <DropDownPicker
                placeholder="ประเภท"
                items={showType}
                itemStyle={style.itemStyle}
                dropDownStyle={style.dropDownStyle}
                style={style.boxDropDownStyle}
                containerStyle={style.containerStyle}
                onChangeItem={(item) => chageType(item)}
            />
            <TextInput
                theme={style.styleInput}
                style={style.inputStyle, { display: showInputType }}
                // mode="outlined"
                label="ประเภท"
                onChangeText={text => value.type_name = text}
            />
            <TextInput
                theme={style.styleInput}
                style={style.inputStyle}
                // mode="outlined"
                label="ชื่อ-นามสกุล"
                onChangeText={text => value.data_name = text}
            />
            <TextInput
                theme={style.styleInput}
                style={style.inputStyle}
                // mode="outlined"
                label="ชื่อร้าน"
                onChangeText={text => value.data_shop = text}
            />
            <TextInput
                theme={style.styleInput}
                style={style.inputStyle}
                // mode="outlined"
                label="เบอร์"
                onChangeText={text => value.data_phone = text}
            />
            <TextInput
                theme={style.styleInput}
                style={style.inputStyle}
                // mode="outlined"
                label="ที่อยู่"
                onChangeText={text => value.data_address = text}
            />
            <DropDownPicker
                items={showProvinces}
                // onChangeItem={item => (value.province_id = item.value)}
                onChangeItem={(item) => getDistricts(item)}
                placeholder="จังหวัด"
                itemStyle={style.itemStyle}
                containerStyle={style.containerStyle}
                style={style.boxDropDownStyle}
                dropDownStyle={style.dropDownStyle}
            />
            <DropDownPicker
                items={showDistricts}
                onChangeItem={item => (value.district_id = item.value)}
                placeholder="อำเภอ"
                itemStyle={style.itemStyle}
                containerStyle={style.containerStyle}
                style={style.boxDropDownStyle}
                dropDownStyle={style.dropDownStyle}
            />
            <TextInput
                theme={style.styleInput}
                style={style.inputStyle}
                // mode="outlined"
                label="LINE ID"
                onChangeText={text => value.data_line = text}
            />
            <TextInput
                theme={style.styleInput}
                style={style.inputStyle}
                // mode="outlined"
                label="Facebook"
                onChangeText={text => value.data_facebook = text}
            />
            <TextInput
                theme={style.styleInput}
                style={style.inputStyle}
                // mode="outlined"
                label="Instagram"
                onChangeText={text => value.data_instagram = text}
            />
            <TextInput
                theme={style.styleInput}
                style={style.inputStyle}
                // mode="outlined"
                label="หมายเหตุ*"
                onChangeText={text => value.data_etc = text}
            />
            {/* APPEND MACHINE SHOW ON DISPLAY START.*/}
            <FlatList
                contentContainerStyle={style.flatListStyle, { position: 'relative' }}
                data={addMachine.viewItem}
                renderItem={({ item, index }) =>
                    <View style={{ marginBottom: 10 }}>
                        <SwipeRow rightOpenValue={-60} style={{ height: 250 }}>
                            <View style={style.standaloneRowBack}>
                                <TouchableOpacity
                                    key={index}
                                    activeOpacity={0.5}
                                    style={style.btnDelete}
                                    onPress={delViewMachine(index)}
                                >
                                    {/* <Text onPress={delViewMachine(index)}>Done</Text> */}
                                    <MaterialIcons key={index} name="delete" size={26} color="#ff4a4a" />
                                </TouchableOpacity>
                            </View>
                            <View style={style.standaloneRowFront}>
                                <View style={{ flexDirection: "row" }}>
                                    <View style={{ flex: 1, }}>
                                        <DropDownPicker
                                            key={index}
                                            items={showTypeMachine}
                                            onChangeItem={item => addMachine.viewItem[index].machine_type_id = item.value}
                                            placeholder="เครื่องใช้ไฟฟ้า"
                                            itemStyle={style.itemStyle}
                                            containerStyle={style.containerStyle}
                                            style={style.boxDropDownStyle}
                                            dropDownStyle={style.dropDownStyle}
                                        />
                                    </View>
                                </View>
                                <View>
                                    {/* <TextInput
                                        theme={styleInput}
                                        style={inputStyle}
                                        // mode="outlined"
                                        label="เครื่องใช้ไฟฟ้า*(เมื่อเลือกอื่นๆ)"
                                        onChangeText={text => addMachine.viewItem[index].machine_type = text}
                                    /> */}
                                    <TextInput
                                        key={index}
                                        theme={style.styleInput}
                                        style={style.inputStyle}
                                        // mode="outlined"
                                        label="ชื่อ/รุ่น"
                                        onChangeText={text => addMachine.viewItem[index].machine_name = text}
                                    />
                                    <Button
                                        icon="calendar"
                                        mode="outlined"
                                        color="#0580FF"
                                        style={style.btnStyle}
                                        onPress={() => getDate(index)}
                                    >
                                        {pickDate[index].date ? 'ประกันถึง : ' + moment(pickDate[index].date).format('DD MMMM, YYYY') : 'เลือกวันประกัน'}
                                    </Button>
                                    <DateTimes id={localStore} show={show} toggle={toggle} nowDate={nowDate} getDatePicker={getDatePicker} />
                                </View>
                            </View>
                        </SwipeRow>
                    </View>
                }
            />
            <Button
                mode="contained"
                color="#4c669f"
                style={style.btnStyle}
                onPress={() => addViewMachine()}
            >
                <FontAwesome name="plus-circle" size={30} color="#fff" />
            </Button>
            <View style={style.btnViewLine} />

            {/* APPEND MATERAIL SHOW ON DISPLAY START.*/}
            <FlatList
                contentContainerStyle={style.flatListStyle, { position: 'relative' }}
                data={addMaterial.viewItem}
                renderItem={({ item, index }) =>
                    <View style={{ marginBottom: 10 }}>
                        <SwipeRow rightOpenValue={-60} style={{ height: 250 }}>
                            <View style={style.standaloneRowBack}>
                                <TouchableOpacity
                                    activeOpacity={0.5}
                                    style={style.btnDelete}
                                    onPress={delViewMaterial(index)}
                                >
                                    {/* <Text onPress={delViewMachine(index)}>Done</Text> */}
                                    <MaterialIcons name="delete" size={26} color="#ff4a4a" />
                                </TouchableOpacity>
                            </View>
                            <View style={style.standaloneRowFront}>
                                <View style={{ flexDirection: "row" }}>
                                    <View style={{ flex: 1, }}>
                                        <DropDownPicker
                                            items={showType}
                                            onChangeItem={item => addMaterial.viewItem[index].material_type_id = item.value}
                                            placeholder="วัตถุดิบที่ใช้"
                                            itemStyle={style.itemStyle}
                                            containerStyle={style.containerStyle}
                                            style={style.boxDropDownStyle}
                                            dropDownStyle={style.dropDownStyle}
                                        />
                                    </View>
                                </View>
                                <View>
                                    <TextInput
                                        theme={style.styleInput}
                                        style={style.inputStyle, { display: addMaterial.viewItem[index].showInputHide }}
                                        // mode="outlined"
                                        label="วัตถุดิบที่ใช้"
                                        onChangeText={text => addMaterial.viewItem[index].material_type = text}
                                    />
                                    <TextInput
                                        theme={style.styleInput}
                                        style={style.inputStyle}
                                        // mode="outlined"
                                        label="ชื่อ/ชนิด"
                                        onChangeText={text => addMaterial.viewItem[index].material_name = text}
                                    />
                                    <TextInput
                                        theme={style.styleInput}
                                        style={style.inputStyle}
                                        // mode="outlined"
                                        label="ราคา/กก"
                                        onChangeText={text => addMaterial.viewItem[index].material_price = text}
                                    />
                                </View>
                            </View>
                        </SwipeRow>
                    </View>
                }
            />
            <Button
                // icon="plus"
                mode="contained"
                color="#4c669f"
                style={style.btnStyle}
                onPress={() => addViewMaterial()}
            >
                <FontAwesome name="plus-circle" size={30} color="#fff" />
            </Button>
            <View style={style.btnViewLine} />

            {/* UPLOAD IMG SHOW ON DISPLAY */}
            {route.params ?
                <View style={{ flex: 1, height: 320 }}>
                    <ScrollView
                        horizontal={true}
                    >
                        {image.img.map((item, i) => renderImage(item, i))}
                    </ScrollView>
                    <Button
                        color="#4c669f"
                        icon="upload"
                        mode="contained"
                        onPress={() => navigation.navigate('Upload', { cusName: value.data_name, shopName: value.data_shop })}
                        style={style.btnStyle}
                    >
                        <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: 16 }}>เลือกรูปภาพใหม่</Text>
                    </Button>
                </View>
                :
                <View style={{ flex: 1, height: 320, }}>
                    <TouchableHighlight
                        underlayColor={'#8a8a8a'}
                        onPress={() => navigation.navigate('Upload', { cusName: value.data_name, shopName: value.data_shop })}
                        style={{ flex: 1, borderWidth: 1, borderRadius: 10, alignItems: 'center', justifyContent: 'center', height: '100%' }}
                    >
                        <View
                            style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
                        >
                            <FontAwesome5 name="images" size={60} color="#d4d4d4" />
                            <Text style={{ color: '#d4d4d4', fontWeight: 'bold', fontSize: 16 }}>ไม่มีรูปภาพ</Text>
                            <Text style={{ color: '#d4d4d4', fontWeight: 'bold', fontSize: 14 }}>กรุณา กด เพื่ออัพโหลดรูปภาพ</Text>
                        </View>
                    </TouchableHighlight>
                </View>
            }
            <Button
                color="#4c669f"
                icon="content-save"
                mode="contained"
                onPress={chkStatusSave}
                style={style.btnStyle}
            >
                <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: 16 }}>บันทึก</Text>
            </Button>
            <ModalAlert logTxt='ต้องการบันทึกข้อมูล' statusVisible={statusVisible} toggle={handleOnClickSaveCallback} />
        </ScrollView>
    );
}

function UploadScreen({ navigation, route }) {
    const _getHeaderLoader = () => (
        <ActivityIndicator size='small' color={'#0580FF'} />
    );
    const imagesCallback = (callback) => {
        navigation.setOptions({
            headerRight: () => _getHeaderLoader()
        });

        callback.then(async (photos) => {
            const cPhotos = [];
            for (let photo of photos) {

                const pPhoto = await _processImageAsync(photo.uri);
                cPhotos.push({
                    uri: pPhoto.uri,
                    // name: moment(new Date).format('YYYY_MM_DD_')+ photo.filename,
                    name: route.params.shopName + '||' + route.params.cusName + '||' + moment(new Date).format('YYYY_MM_DD||hh:mm:ss:s'),
                    type: 'image/jpg'
                })
            }
            navigation.navigate('Add Survey', { photos: cPhotos });
        }).catch((e) => console.log(e));
    };
    async function _processImageAsync(uri) {
        const file = await ImageManipulator.manipulateAsync(
            uri,
            [{ resize: { width: 1000 } }],
            { compress: 0.8, format: ImageManipulator.SaveFormat.JPEG }
        );
        return file;
    };

    const _renderDoneButton = (count, onSubmit) => {
        if (!count) return null;
        return <TouchableOpacity
            style={{
                marginRight: 20,
                borderWidth: 2,
                borderRadius: 10,
                borderColor: '#fefefe',
            }}
            title={'อัพโหลด'}
            onPress={onSubmit}>
            <Text
                style={{
                    padding: 5,
                    color: '#fefefe',
                    fontSize: 18,
                    fontWeight: 'bold',
                }}
                onPress={onSubmit}>อัพโหลด</Text>
        </TouchableOpacity>
    }

    const updateHandler = (count, onSubmit) => {
        navigation.setOptions({
            title: `เลือก ${count} รูป`,
            headerRight: () => _renderDoneButton(count, onSubmit)
        });
    };
    useEffect(() => {
    }, []);
    const renderSelectedComponent = (number) => (
        <View style={style.countBadge}>
            <Text style={style.countBadgeText}>{number}</Text>
        </View>
    );
    return (
        <ScrollView style={{ borderWidth: 1, paddingTop: 10 }}>
            <ImageBrowser
                // max={3}
                loadCount={10}
                onChange={(num, onSubmit) => { updateHandler(num, onSubmit) }}
                callback={(callback) => { imagesCallback(callback) }}
                renderSelectedComponent={renderSelectedComponent}
            />
        </ScrollView>
    );
}

// Func Main Set View Stack Activity New Page
export default function addSurvey(dataUser, handleClickLogout) {
    if (dataUser !== undefined) {
        dataUserStore.dispatch({ type: 'Add', playload: dataUser })
    }
    return (
        <View style={style.viewInputStyle}>
            <Stack.Navigator>
                <Stack.Screen
                    name="Add Survey"
                    component={DataScreen}
                    options={{header:(props) => (null)}}
                    // options={{
                    //     header: (props) => (
                    //         <Header dataUser={{ name: 'เพิ่มข้อมูลสำรวจ' }} handleClickLogout={handleClickLogout} />
                    //     ),
                    // }}
                />
                <Stack.Screen
                    name="Upload"
                    component={UploadScreen}
                    options={{
                        title: 'อัพโหลดรูปภาพ',
                        headerStyle: style.headerStyle,
                        headerTintColor: style.headerTintColor,
                        headerTitleStyle: style.headerTitleStyle,
                    }}
                />
            </Stack.Navigator>
        </View>
    );
};
