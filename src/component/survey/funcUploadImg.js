// =======================================================================
// Init/State Componant
// =======================================================================

import React, { useEffect } from 'react';
import {
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    ActivityIndicator,
} from 'react-native';
import { StyleContain } from '../style/funcStyle';
const style = StyleContain();

// =======================================================================
// Upload Images Componant
// =======================================================================

export default function UploadImg({ navigation, route }) {
    const _getHeaderLoader = () => (
        <ActivityIndicator size='small' color={'#0580FF'} />
    );

    const imagesCallback = (callback) => {
        navigation.setOptions({
            headerRight: () => _getHeaderLoader()
        });

        callback.then(async (photos) => {
            const cPhotos = [];
            for (let photo of photos) {

                const pPhoto = await _processImageAsync(photo.uri);
                cPhotos.push({
                    uri: pPhoto.uri,
                    // name: moment(new Date).format('YYYY_MM_DD_')+ photo.filename,
                    name: route.params.shopName + '||' + route.params.cusName + '||' + moment(new Date).format('YYYY_MM_DD||hh:mm:ss:s'),
                    type: 'image/jpg'
                })
            }
            navigation.navigate('Add Survey', { photos: cPhotos });
        }).catch((e) => console.log(e));
    };
    async function _processImageAsync(uri) {
        const file = await ImageManipulator.manipulateAsync(
            uri,
            [{ resize: { width: 1000 } }],
            { compress: 0.8, format: ImageManipulator.SaveFormat.JPEG }
        );
        return file;
    };

    const _renderDoneButton = (count, onSubmit) => {
        if (!count) return null;
        return <TouchableOpacity
            style={{
                marginRight: 20,
                borderWidth: 2,
                borderRadius: 10,
                borderColor: '#fefefe',
            }}
            title={'อัพโหลด'}
            onPress={onSubmit}>
            <Text
                style={{
                    padding: 5,
                    color: '#fefefe',
                    fontSize: 18,
                    fontWeight: 'bold',
                }}
                onPress={onSubmit}>อัพโหลด</Text>
        </TouchableOpacity>
    }

    const updateHandler = (count, onSubmit) => {
        navigation.setOptions({
            title: `เลือก ${count} รูป`,
            headerRight: () => _renderDoneButton(count, onSubmit)
        });
    };
    useEffect(() => {
    }, []);
    const renderSelectedComponent = (number) => (
        <View style={style.countBadge}>
            <Text style={style.countBadgeText}>{number}</Text>
        </View>
    );
    return (
        <ScrollView style={{ borderWidth: 1, paddingTop: 10 }}>
            <ImageBrowser
                // max={3}
                loadCount={10}
                onChange={(num, onSubmit) => { updateHandler(num, onSubmit) }}
                callback={(callback) => { imagesCallback(callback) }}
                renderSelectedComponent={renderSelectedComponent}
            />
        </ScrollView>
    );
}