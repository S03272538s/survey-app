// =======================================================================
// Init/State Componant
// =======================================================================
import React, { useState, useEffect, useRef } from 'react';
import {
    Text,
    View,
    Animated,
    Dimensions,
    ScrollView,
    RefreshControl,
    TouchableOpacity,
    TouchableHighlight,
    ActivityIndicator,
} from 'react-native';
import moment from 'moment';
import EditData from './funcEditData';
import SearchData from './filter/funcSearch';
import { DataTable, Button, TextInput } from 'react-native-paper';
import { MaterialIcons, FontAwesome } from '@expo/vector-icons';
import ModalLogout from '../../login-out/funcModalLogout';
import { StyleContain } from '../style/funcStyle';
const style = StyleContain();
// =======================================================================
// Navigations
// =======================================================================
const { height } = Dimensions.get('window');
import { createStackNavigator } from '@react-navigation/stack';
import { CommonActions } from '@react-navigation/native';

// =======================================================================
// ImagePicker
// =======================================================================

import * as ImageManipulator from 'expo-image-manipulator';
import { ImageBrowser } from 'expo-image-picker-multiple';

// =======================================================================
// Repository Data
// =======================================================================

import { getData } from '../../repository/funcGetData';
import { deleteData } from '../../repository/funcPostData';

// =======================================================================
// DataTableView
// =======================================================================
function DataScreen({ navigation, route }) {
    //Get Data Api Set To State Value 
    const [showData, setData] = useState();
    const [initData, setInitData] = useState();
    const [type, setType] = useState();
    const [typeMachine, setTypeMachine] = useState();
    const [typeMaterial, setTypeMaterial] = useState();
    const [provinces, setProvinces] = useState();
    const [districts, setDistricts] = useState();

    // State Refresh Page
    const [refreshing, setRefreshing] = useState(false);

    //Start Alert Data To Delete
    const [statusVisible, setStatusVisible] = useState(false);
    const [valueDel, setValueDel] = useState();


    const [headerShown, setHeaderShown] = useState(false);
    const translation = useRef(new Animated.Value(-100)).current;

    const getDataAll = async () => {
        let arrData = [], arrProvinces = [];
        // Get Data Survey
        const getDataSurvey = await getData('getData', '');
        getDataSurvey.forEach((element) => {
            arrData.push(element);
            getDistricts(element.district_id);
        });
        // Get Data Selections
        const getType = await getData('getType', '');
        const getTypeMachine = await getData('getTypeMachine', '');
        const getTypeMaterial = await getData('getTypeMaterial', '');
        const getProvinces = await getData('getProvinces', '');
        getProvinces.forEach((element) => {
            arrProvinces.push({ label: element.provinces_name, value: element.provinces_id });
        });

        setData(arrData);
        setInitData(arrData);
        setType(setValueSelection(getType));
        setTypeMachine(setValueSelection(getTypeMachine));
        setTypeMaterial(setValueSelection(getTypeMaterial));
        setProvinces(arrProvinces);
    };
    const setValueSelection = (arr) => {
        let arrData = []
        arr.forEach((element) => {
            arrData.push({ label: element.name, value: element.id });
        });
        arrData.push({ value: 'etc', label: 'อื่นๆ' });
        return arrData;
    };
    const getDistricts = async (provinces) => {
        let arrDistricts = [];
        // GET DISTRICTS SURVEY
        const getDistricts = await getData('getDistricts', '');
        getDistricts.forEach((element) => {
            if (element.provinces_id == provinces.value) {
                arrDistricts.push({ label: element.districts_name, value: element.districts_id });
            }
        });
        setDistricts(arrDistricts);
    };

    //Reload Page
    const onRefresh = React.useCallback(() => {
        // setRefreshing(true);
        getDataAll();
        // wait(2000).then(() => setRefreshing(false));
    }, []);

    const wait = (timeout) => {
        return new Promise(resolve => setTimeout(resolve, timeout));
    }
    if (route.params !== undefined) {
        if (route.params.statusUpdate === true) {
            wait(2000).then(() => onRefresh());
            route.params.statusUpdate = false;
        }
        // reRenderByDelete(true);
        route.params = undefined;
    }

    //Delete Row Data
    const chkStatusDel = (data) => {
        setStatusVisible(true, { ...statusVisible })
        setValueDel({
            id: data.id,
            machineId: data.machine_id,
            materialId: data.material_id,
            ...statusVisible
        })
    }

    const reRenderByDelete = (status) => {
        setRefreshing(status);
        getDataAll();
        wait(2000).then(() => setRefreshing(false));
    }

    const statusDel = (status) => {
        setStatusVisible(false, { ...statusVisible });
        if (status === true) {
            handleDeleteData();
        }
    }

    const handleDeleteData = async () => {
        const callBackDel = await deleteData(valueDel);
        if (callBackDel == true) {
            navigation.dispatch(
                CommonActions.reset({
                    index: 1,
                    routes: [{
                        name: 'Data',
                    }],
                })
            )
        }
    }


    const [sortShop, setSortShop] = useState('ascending');
    const [iconSortShop, setIconSortShop] = useState(<FontAwesome name="sort-alpha-asc" size={20} color="black" />);

    const [sortName, setSortName] = useState('ascending');
    const [iconSortName, setIconSortName] = useState(<FontAwesome name="sort-alpha-asc" size={20} color="black" />);

    const [styleTitle, setStyleTitle] = useState([
        {
            borderWidth: 0 , 
            borderColor: '#FEFEFE'
        },
        {
            borderWidth: 0 ,
            borderColor: '#FEFEFE'
        }
    ])

    const handlerOnClickSortShopFilter = (sortOld, showData) => {

        styleTitle[0].borderWidth = 0;
        styleTitle[1].borderWidth = 2;
        styleTitle[0].borderColor = '#FEFEFE';
        styleTitle[1].borderColor = '#ffd506';
        
        if (sortOld === 'ascending') {
            const sorted = [...showData].sort((a, b) => b.data_shop > a.data_shop)
            setData(sorted);
            setSortShop('descending');
            
            setIconSortShop(<FontAwesome name="sort-alpha-desc" size={20} color="black" />)
        }
        if (sortOld === 'descending') {
            const sorted = [...showData].sort((a, b) => a.data_shop > b.data_shop)
            setData(sorted);
            setSortShop('ascending');
            setIconSortShop(<FontAwesome name="sort-alpha-asc" size={20} color="black" />)
        }
    }

    const handlerOnClickSortNameFilter = (sortOld, showData) => {
        styleTitle[0].borderWidth = 2;
        styleTitle[1].borderWidth = 0;
        styleTitle[0].borderColor = '#ffd506';
        styleTitle[1].borderColor = '#FEFEFE';
        if (sortOld === 'ascending') {
            const sorted = [...showData].sort((a, b) => b.data_name > a.data_name)
            setData(sorted);
            setSortName('descending');
            setIconSortName(<FontAwesome name="sort-alpha-desc" size={20} color="black" />)
        }
        if (sortOld === 'descending') {
            const sorted = [...showData].sort((a, b) => a.data_name > b.data_name)
            setData(sorted);
            setSortName('ascending');
            setIconSortName(<FontAwesome name="sort-alpha-asc" size={20} color="black" />)
        }
    }

    const searchFilter = (data) => {
        setData([...data]);
    }

    const scrollPopup = (event) => {
        let scrolling = event.nativeEvent.contentOffset.y;
        if (scrolling > 150) {
            setHeaderShown(true);
        } else {
            setHeaderShown(false);
        }
    }

    useEffect(() => {
        Animated.timing(translation, {
            toValue: headerShown ? -150 : 0,
            duration: 250,
            useNativeDriver: true,
        }).start();
    }, [headerShown]
    );
    useEffect(() => {
        getDataAll();
    }, [])

    

    return showData ? (
        <>
            <Animated.View
                style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    right: 0,
                    height: 220,
                    zIndex: 1,
                    transform: [
                        { translateY: translation },
                    ],
                }}
            >
                <View style={{ flex: 1, paddingTop: 10, backgroundColor: '#FEFEFE' }}>
                    <SearchData
                        dataInit={initData}
                        searchFilter={searchFilter}
                    />
                    <DataTable style={{ flex: 0.3, justifyContent: 'flex-end', borderBottomWidth: 2, borderBottomColor: '#ffd506' }}>
                        <DataTable.Header>
                            <TouchableHighlight
                                activeOpacity={0.6}
                                style={{ flex: 1, alignItems: 'center', borderTopLeftRadius: 20, borderWidth:styleTitle[0].borderWidth , borderColor:styleTitle[0].borderColor ,backgroundColor:styleTitle[0].borderColor }}
                                underlayColor={'#FFF9DB'}
                                onPress={() => { handlerOnClickSortNameFilter(sortName, showData) }}
                            >
                                <DataTable.Title>
                                    <View style={{ paddingRight: 20 }}>
                                        <Text style={{ fontWeight: 'bold', }}>ชื่อลูกค้า</Text>
                                    </View>

                                    {iconSortName}
                                </DataTable.Title>
                            </TouchableHighlight>
                            <TouchableHighlight
                                activeOpacity={0.6}
                                style={{ flex: 1, alignItems: 'center', borderWidth:styleTitle[1].borderWidth ,borderColor:styleTitle[1].borderColor, backgroundColor:styleTitle[1].borderColor}}
                                underlayColor={'#FFF9DB'}
                                onPress={() => { handlerOnClickSortShopFilter(sortShop, showData) }}
                            >
                                <DataTable.Title>
                                    <View style={{ paddingRight: 20 }}>
                                        <Text style={{ fontWeight: 'bold', }}>ชื่อร้าน</Text>
                                    </View>
                                    {iconSortShop}
                                </DataTable.Title>
                            </TouchableHighlight>
                            <View style={{ flex: 1, alignItems: 'center', borderTopRightRadius: 20, }}>
                                <DataTable.Title>
                                    <Text style={{ fontWeight: 'bold', color: '#292929' }}>ติดต่อ</Text>
                                </DataTable.Title>
                            </View>
                        </DataTable.Header>
                    </DataTable>
                </View>
            </Animated.View>
            <ScrollView
                onScroll={(event) => { scrollPopup(event) }}
                scrollEventThrottle={16}
                style={style.ScrollViewDataTableStyle}
            // overScrollMode={'never'}
            // snapToInterval={height - 20}
            // snapToAlignment={"start"}
            >
                <View style={{
                    flex: 1,
                    paddingTop: 220,
                }}>
                    <DataTable style={style.viewFlex}>
                        {showData.map((data, index) => (
                            <DataTable.Row
                                style={{ borderBottomWidth: 1, borderBottomColor: '#4c669f', height: 60 }}
                                key={index}
                                onPress={() => navigation.navigate('Edit', {
                                    data: data,
                                    type: type,
                                    typeMachine: typeMachine,
                                    typeMaterial: typeMaterial,
                                    provinces: provinces,
                                    districts: districts,
                                })}
                                onLongPress={() => chkStatusDel(data)}
                            >
                                <DataTable.Cell   >{data.data_name}</DataTable.Cell>
                                <DataTable.Cell  >{data.data_shop}</DataTable.Cell>
                                <DataTable.Cell numeric>{data.data_phone}</DataTable.Cell>
                            </DataTable.Row>
                        ))}
                    </DataTable>

                </View>
                <ModalLogout logTxt='ต้องการลบข้อมูล หรือไม่' statusVisible={statusVisible} toggle={statusDel} />
            </ScrollView>
        </>
    ) : (
        <View style={{ flex: 1, backgroundColor: '#FEFEFE', alignItems: 'center', justifyContent: 'center' }}>
            <Text style={{ color: '#292929', fontWeight: 'bold', fontSize: 16, paddingBottom:20 }}> กำลังดึงข้อมูล... </Text>
            <ActivityIndicator size={70} color={'#0580FF'} />
        </View>

    )
}

// =======================================================================
// EditRowData Page
// =======================================================================

function EditScreen({ navigation, route, dataUser }) {
    useEffect(() => {
    }, []);
    const handleUploadImg = () => {
        navigation.navigate('UploadImg');
    }
    const handleCallbackUpdate = () => {
        navigation.dispatch(
            CommonActions.reset({
                index: 1,
                routes: [{
                    name: 'Data',
                    params: { statusUpdate: true },
                }],
            })
        )
    }
    const handleDeleteData = async (data) => {
        // console.log(data);
    }
    return (
        <ScrollView style={style.ScrollViewStyle}>
            <EditData
                data={route.params.data}
                dataUser={dataUser}
                type={route.params.type}
                typeMachine={route.params.typeMachine}
                typeMaterial={route.params.typeMaterial}
                provinces={route.params.provinces}
                uploadImg={handleUploadImg}
                photos={route.params.photos}
                statusShowImg={route.params.statusDelImg}
                delData={handleDeleteData}
                CallBackUpdate={handleCallbackUpdate}
            />
        </ScrollView>
    );
}

// =======================================================================
// Upload IMGs Page
// =======================================================================

function UploadImgScreen({ navigation, route }) {

    const _getHeaderLoader = () => (
        <ActivityIndicator size='small' color={'#0580FF'} />
    );

    const imagesCallback = (callback) => {
        navigation.setOptions({
            headerRight: () => _getHeaderLoader()
        });

        callback.then(async (photos) => {
            const cPhotos = [];
            for (let photo of photos) {

                const pPhoto = await _processImageAsync(photo.uri);
                cPhotos.push({
                    uri: pPhoto.uri,
                    name: moment(new Date).format('YYYY_MM_DD_') + photo.filename,
                    type: 'image/jpg'
                })
            }
            navigation.navigate('Edit', { photos: cPhotos, statusDelImg: false });
        })
            .catch((e) => console.log(e));
    };

    async function _processImageAsync(uri) {
        const file = await ImageManipulator.manipulateAsync(
            uri,
            [{ resize: { width: 1000 } }],
            { compress: 0.8, format: ImageManipulator.SaveFormat.JPEG }
        );
        return file;
    };

    const _renderDoneButton = (count, onSubmit) => {
        if (!count) return null;
        return <TouchableOpacity title={'อัพโหลด'} onPress={onSubmit}>
            <Text style={{ paddingRight: 20, color: '#fefefe', fontSize: 18, fontWeight: 'bold' }} onPress={onSubmit}>อัพโหลด</Text>
        </TouchableOpacity>
    }

    const updateHandler = (count, onSubmit) => {
        navigation.setOptions({
            title: `เลือก ${count} รูป`,
            headerRight: () => _renderDoneButton(count, onSubmit)
        });
    };
    useEffect(() => {
    }, []);
    const renderSelectedComponent = (number) => (
        <View style={style.countBadge}>
            <Text style={style.countBadgeText}>{number}</Text>
        </View>
    );
    return (
        <ScrollView style={style.ScrollViewStyle}>
            <ImageBrowser
                // max={3}
                loadCount={20}
                onChange={(num, onSubmit) => { updateHandler(num, onSubmit) }}
                callback={(callback) => { imagesCallback(callback) }}
                renderSelectedComponent={renderSelectedComponent}
            />
        </ScrollView>
    );
}

// =======================================================================
// DataTable Navigations
// =======================================================================

export default function Datatable(dataUser, handleClickLogout) {
    const Stack = createStackNavigator();
    return (
        <View style={style.viewInputStyle}>
            <Stack.Navigator
            >
                <Stack.Screen
                    name="Data"
                    component={DataScreen}
                    options={{ header: (props) => (null) }}
                // options={{
                //     header: (props) => (
                //         <Header dataUser={{ name: 'Data Survey' }} handleClickLogout={handleClickLogout} />
                //     )
                // }} 
                />

                <Stack.Screen
                    name="Edit"
                    // component={EditScreen}
                    options={{
                        title: 'แก้ไขข้อมูล',
                        headerStyle: {
                            backgroundColor: '#2653a5',
                        },
                        headerTitleStyle: {
                            fontSize: 18,
                            fontWeight: 'bold',
                        },
                        headerTintColor: '#fefefe',
                    }}
                >
                    {(props) => <EditScreen navigation={props.navigation} route={props.route} dataUser={dataUser} />}

                </Stack.Screen>
                <Stack.Screen
                    name="UploadImg"
                    component={UploadImgScreen}
                    options={{
                        title: 'อัพโหลดรูปภาพ',
                        headerStyle: {
                            backgroundColor: '#2653a5',
                        },
                        headerTitleStyle: {
                            fontSize: 18,
                            fontWeight: 'bold',
                        },
                        headerTintColor: '#fefefe',
                    }}
                />
            </Stack.Navigator>
        </View>

    );
};
