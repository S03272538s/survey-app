import React, { useState, useEffect } from 'react';
import { Text, View, FlatList, ScrollView, Image, TouchableOpacity, TouchableHighlight } from 'react-native';
import { Button, TextInput } from 'react-native-paper';

import DropDownPicker from 'react-native-dropdown-picker';

import { DateTimes } from '../modal/funcModalDateTimes'
import { createStore } from 'redux';
import moment from 'moment';
import 'moment/locale/th';

import { FontAwesome5, FontAwesome, MaterialIcons } from '@expo/vector-icons';
import { SwipeRow } from 'react-native-swipe-list-view';

// import ModalAlert from '../login-out/funcModalAlertLogin';
import ModalAlert from '../../login-out/funcModalLogout';
import ModalImg from '../modal/funcModalShowImg';


import { getData } from '../../repository/funcGetData';
import { updateDatas } from '../../repository/funcPostData';
import Ajax from "../../api-call/ajax.js";

import { StyleContain } from '../style/funcStyle';
const style = StyleContain();

const ajax = new Ajax();
ajax.url = "http://1.2.175.220/Survey/";

// init component
const EditData = ({ data, dataUser, type, typeMachine, typeMaterial, provinces, uploadImg, photos, statusShowImg, delData, CallBackUpdate }) => {
    const { viewStyle, textStyle, inputStyle, DropDownStyle, editStyle, BoxDropDownStyle, BoxDropDownRowStyle, standaloneRowFront, standaloneRowBack, BtnDelete, BtnViewLine } = headerStyle;
    const [dataRow, setDataRow] = useState(data);
    const [districts, setDistricts] = useState();
    const [showInputType, setShowInputType] = useState('none');
    /// State/Value ON DateTime
    const [currentDate, setCurrentDate] = useState();
    const [birthDate1, setBirthDate1] = useState(new Date());
    const [show, showModal] = useState(false);
    const toggle = () => showModal(!show);

    //State IMG upload
    const [image, setImage] = useState({ img: [] });
    const [imageUpload, setImageUpload] = useState({ img: [] });
    const [statusDelImg, setStatusDelImg] = useState(statusShowImg);

    // State Style Input
    const [styleInput, setStyleInput] = useState(
        {
            colors: {
                text: '#292929', primary: '#3b5998',
                underlineColor: 'transparent', background: 'transparent'
            }
        });
    // State Alert Complete
    const [statusVisible, setStatusVisible] = useState(false);
    const [statusImgVisible, setStatusImgVisible] = useState(false);
    const [dataImg, setDataImg] = useState([]);

    const chageType = (typeItem) => {
        dataRow.type_id = typeItem.value;
        if (typeItem.value === 'etc') {
            setShowInputType('flex');
        } else {
            setShowInputType('none');

        }
    }

    const wait = (timeout) => {
        return new Promise(resolve => setTimeout(resolve, timeout));
    }
    useEffect(() => {
        getDistricts(dataRow.province_id);
        getAppendData();
    }, []);

    if (statusDelImg !== true) {
        let arrImgName = [];
        let imgNameDB = JSON.parse(data.img_name);
        if (imgNameDB !== '') {
            imgNameDB.forEach(element => {
                arrImgName.push('http://1.2.175.220/public/img_survey/' + '"' + element + '"' + '.jpg');
            });
            image.img = arrImgName;
            if (photos !== undefined) {
                photos.forEach((element, index) => {
                    arrImgName.push(element.uri);
                });
                image.img = arrImgName;
                imageUpload.img = photos;
            }
        }
    } else {
        console.log('ELSE');
    }
    const getDistricts = async (provinces) => {
        let arrDistricts = [];
        // GET DISTRICTS SURVEY
        const getDistricts = await getData('getDistricts', '');
        getDistricts.forEach((element) => {
            if (element.provinces_id == provinces.value || element.provinces_id == provinces) {
                arrDistricts.push({ label: element.districts_name, value: element.districts_id });
            }
        });
        setDistricts(arrDistricts);
    };
    // Get Data Machine && Material by id Primery in table sur_data 
    const getAppendData = async () => {
        let arrMachine = [], arrMaterial = [];
        let arrObjAddDate = [];

        // GET Data Machine
        const getMachine = await getData('getMachine', '');
        getMachine.forEach((element, i) => {
            if (data.machine_id === element.id) {
                let id_json = JSON.parse(element.id),
                    // sur_id_json = JSON.parse(element.sur_id),
                    machine_type_id_json = JSON.parse(element.type_id),
                    machine_type_json = JSON.parse(element.machine_type),
                    machine_name_json = JSON.parse(element.machine_name),
                    machine_insurance_json = JSON.parse(element.machine_insurance);
                machine_type_id_json.forEach((element, i) => {
                    arrMachine.push({
                        // sur_id: sur_id_json[i],
                        machine_type_id: element,
                        machine_type: machine_type_json[i],
                        machine_name: machine_name_json[i],
                        machine_insurance: machine_insurance_json[i],
                    }
                    );
                    arrObjAddDate.push({
                        id: i,
                        date: machine_insurance_json[i]
                    });
                });
            }
        });
        // GET Data Material
        const getMaterial = await getData('getMaterial', '');
        getMaterial.forEach(element => {
            if (data.material_id === element.id) {
                let id_json = JSON.parse(element.id),
                    // sur_id_json = JSON.parse(element.sur_id),
                    material_id_json = JSON.parse(element.type_id),
                    material_type_json = JSON.parse(element.material_type),
                    material_name_json = JSON.parse(element.material_name),
                    material_price_json = JSON.parse(element.material_price);

                material_id_json.forEach((element, i) => {
                    arrMaterial.push({
                        // sur_id: sur_id_json[i],
                        material_type_id: element,
                        material_type: material_type_json[i],
                        material_name: material_name_json[i],
                        material_price: material_price_json[i],
                    })
                });
            }
        });

        arrMachine.slice(0, 0);
        arrMaterial.slice(0, 0);
        arrObjAddDate.slice(0, 0);

        setPickDate(arrObjAddDate);
        setAddMachine({ viewItem: arrMachine });
        setAddMaterial({ viewItem: arrMaterial })
    };
    const setMachineName = (txt, index) => {
        addMachine.viewItem[index].machine_name = txt;
        setAddMachine({ ...addMachine })
    };
    const setMachineTypeName = (txt, index) => {
        addMachine.viewItem[index].machine_type = txt;
        setAddMachine({ ...addMachine })
    };
    const setMaterialTypeName = (txt, index) => {
        addMaterial.viewItem[index].material_type = txt;
        setAddMachine({ ...addMaterial })
    };
    const setMaterialName = (txt, index) => {
        addMaterial.viewItem[index].material_name = txt;
        setAddMachine({ ...addMaterial })
    };
    const setMaterialPrice = (txt, index) => {
        addMaterial.viewItem[index].material_price = txt;
        setAddMachine({ ...addMaterial })
    };
    // Get Current DateTime / Get Date Picker
    const getCurrentDate = () => {
        var date = new Date().getDate();
        var month = new Date().getMonth() + 1;
        var year = new Date().getFullYear();
        setCurrentDate({
            format: date + '-' + month + '-' + year,//format: dd-mm-yyyy;
            day: date,
            month: month,
            year: year
        });
    };
    const reduxStore = (state, action) => {
        switch (action.type) {
            case 'Add':
                state = action.playload;
                break;
            default:
                state = state;
                break;
        }
        return state;
    };
    const localStore = createStore(reduxStore, 0);

    const getDate = (index) => {
        localStore.dispatch({ type: 'Add', playload: index })
        toggle();
        return;
    }
    const getDatePicker = (date, id) => {
        let arr = [];
        pickDate.forEach((element, i) => {
            if (pickDate[i].id == id) {
                pickDate[i].date = date;
                // setPickDate({date:date ,...pickDate});
            } else {
                // pickDate.push({id:index, date:birthDate1});
            }
            arr.push(element);
        });
        setPickDate(arr, ...pickDate);
    }
    /// APPEND MACHINE START.
    const [addMachine, setAddMachine] = useState(
        {
            viewItem: [{
                // sur_id: '',
                machine_type_id: '',
                machine_type: '',
                machine_name: '',
                machine_insurance: '',
            }]
        }
    );
    const [pickDate, setPickDate] = useState([{ id: 0, date: '' }]);
    const addViewMachine = () => {
        addMachine.viewItem.push({ itemMachine: addMachine.viewItem.length, machine_insurance: '' }); // push Array to State Value
        pickDate.push({ id: addMachine.viewItem.length - 1, date: '' });
        setAddMachine({ viewItem: addMachine.viewItem }); // SetState value{ item:(Array) }
    }
    const delViewMachine = (index) => () => {
        addMachine.viewItem.splice(index, 1);   // Remove array.splice(index,countRemove);
        setAddMachine({ viewItem: addMachine.viewItem });   // SetState value{ item:(Array) }
    }
    /// APPEND MACHINE END.
    /// APPEND MATERAIL START.
    const [addMaterial, setAddMaterial] = useState({
        viewItem: [{
            // itemMaterial: 0,
            sur_id: '',
            material_type_id: '',
            material_type: '',
            material_name: '',
            material_price: '',
        }]
    });
    const addViewMaterial = () => {
        addMaterial.viewItem.push({ itemMaterial: addMaterial.viewItem.length }); // push Array to State Value
        setAddMaterial({ viewItem: addMaterial.viewItem }); // SetState value{ item:(Array) }
    }
    const delViewMaterial = (index) => () => {
        addMaterial.viewItem.splice(index, 1);   // Remove array.splice(index,countRemove);
        setAddMaterial({ viewItem: addMaterial.viewItem });   // SetState value{ item:(Array) }
    }
    // APPEND MATERAIL END.

    const upload = () => {
        new Promise((r, j) => {
            ajax.fileUpload(imageUpload.img, "fileUploadImg", "PIC", r)
        })
            .then((v) => {
                // console.log(v);
            })
    }

    const handlerClickImg = (uri, i) => {
        setStatusImgVisible(true, { ...statusImgVisible })
        setDataImg({ uri: uri, index: i });
    }
    const handleOnClickImgCallback = (status, index) => {
        setStatusImgVisible(false)
        if (status === true) {
            delImg(index);
        }
    }
    const delImg = (index) => {
        setStatusDelImg(true, { ...statusDelImg });
        image.img.splice(index, 1);   // Remove array.splice(index,countRemove);
        // setAddMaterial({img: image.img});   // SetState value{ item:(Array) }
        setImage({ ...image });   // SetState value{ item:(Array) }
    }
    const clickAddImg = (index) => {
        setStatusDelImg(index, { ...statusDelImg });
    }
    const renderImage = (item, i) => {
        return (
            <TouchableHighlight
                key={i}
                onPress={() => handlerClickImg(item, i)}
                activeOpacity={0.6}
                underlayColor="#ecf0f1"
            >
                <View style={{ flex: 1, borderWidth: 0.5, marginRight: 10 }}>
                    <Image
                        style={{ height: 200, width: 200 }}
                        source={{ uri: item }}
                        key={i}
                    />
                </View>
            </TouchableHighlight>
        )
    }
    //Start Post Update Data 
    const updateData = async () => {
        let arrMachineTypeId = [], arrMachineType = [], arrMachineName = [], arrMachineInsurance = [];
        let arrMaterialTypeId = [], arrMaterialType = [], arrMaterialName = [], arrMaterialPrices = [];
        let arrImgName = [], machineIds = '', materialIds = '';
        let arrImgNameUpload = [];

        // Loop Data For Insert Data Machine
        addMachine.viewItem.forEach((machine, i) => {
            arrMachineTypeId.push(machine.machine_type_id);
            arrMachineType.push(machine.machine_type);
            arrMachineName.push(machine.machine_name);
            arrMachineInsurance.push(pickDate[i].date);
        });
        // Loop Data For Insert Data Machine
        addMaterial.viewItem.forEach(material => {
            arrMaterialTypeId.push(material.material_type_id);
            arrMaterialType.push(material.material_type);
            arrMaterialName.push(material.material_name);
            arrMaterialPrices.push(material.material_price);
        });
        // Make Data For Insert Data Machine
        const data_machine = {
            action: "UPDATE",
            id: dataRow.machine_id, // SEND ID WHEN UPDATE DATA 
            sql: {
                sur_id: {
                    val: dataRow.user_id,
                    type: "INTEGER"
                },
                machine_type_id: {
                    val: JSON.stringify(arrMachineTypeId),
                    type: "TEXT"
                },
                machine_type: {
                    val: JSON.stringify(arrMachineType),
                    type: "TEXT"
                },
                machine_name: {
                    val: JSON.stringify(arrMachineName),
                    type: "TEXT"
                },
                machine_insurance: {
                    val: JSON.stringify(arrMachineInsurance),
                    type: "TEXT"
                },
                active: {
                    val: true,
                    type: "BOOLEAN"
                }
            }
        },
            data_material = {
                action: "UPDATE",
                id: dataRow.material_id, // SEND ID WHEN UPDATE DATA 
                sql: {
                    sur_id: {
                        val: dataRow.user_id,
                        type: "INTEGER"
                    },
                    material_type_id: {
                        val: JSON.stringify(arrMaterialTypeId),
                        type: "TEXT"
                    },
                    material_type: {
                        val: JSON.stringify(arrMaterialType),
                        type: "TEXT"
                    },
                    material_name: {
                        val: JSON.stringify(arrMaterialName),
                        type: "TEXT"
                    },
                    material_price: {
                        val: JSON.stringify(arrMaterialPrices),
                        type: "TEXT"
                    },
                    active: {
                        val: true,
                        type: "BOOLEAN"
                    }
                }
            };
        new Promise(async (r, j) => {
            ajax.set(data_machine, 'setDataMachine', '', r);
            ajax.set(data_material, 'setDataMaterial', '', r);
        }).then(async () => {
            if (dataRow.img_name) {
                let baseImg = JSON.parse(dataRow.img_name)
                if (baseImg !== '') {
                    baseImg.forEach(item => {
                        arrImgName.push(item);
                    });
                    if (photos !== undefined) {
                        photos.forEach(item => { arrImgName.push(item.name); });
                    }
                }

            }
           
            // const idAppEdit = JSON.parse(dataUser.id_application);
            const data = {
                action: "UPDATE",
                id: dataRow.id, // SEND ID WHEN UPDATE DATA 
                sql: {
                    user_id:{
                            val: dataRow.user_id,
                            type: "INTEGER"
                        },
                    user_id_edit_last: {
                        val: dataUser.dataUser.id_application,
                        // val: 123,
                        type: "INTEGER"
                    },
                    type_id: {
                        val: (dataRow.type_id == '' ? 0 : dataRow.type_id),
                        type: "INTEGER"
                    },
                    type_name: {
                        val: dataRow.type_name,
                        type: "TEXT"
                    },
                    data_name: {
                        val: dataRow.data_name,
                        type: "TEXT"
                    },
                    data_shop: {
                        val: dataRow.data_shop,
                        type: "TEXT"
                    },
                    data_address: {
                        val: dataRow.data_address,
                        type: "TEXT"
                    },
                    province_id: {
                        val: dataRow.province_id.value,
                        type: "TEXT"
                    },
                    district_id: {
                        val: dataRow.district_id,
                        type: "TEXT"
                    },
                    data_phone: {
                        val: dataRow.data_phone,
                        type: "TEXT"
                    },
                    data_line: {
                        val: dataRow.data_line,
                        type: "TEXT"
                    },
                    data_facebook: {
                        val: dataRow.data_facebook,
                        type: "TEXT"
                    },
                    data_instagram: {
                        val: dataRow.data_instagram,
                        type: "TEXT"
                    },
                    machine_id: {
                        val: (machineIds == '' ? 0 : dataRow.machine_id),
                        type: "INTEGER"
                    },
                    material_id: {
                        val: (machineIds == '' ? 0 : dataRow.material_id),
                        type: "INTEGER"
                    },
                    img_name: {
                        val: (arrImgName[0] ? '' : JSON.stringify(arrImgName)),
                        type: "TEXT"
                    },
                    data_etc: {
                        val: dataRow.data_etc,
                        type: "TEXT"
                    },
                    active: {
                        val: true,
                        type: "BOOLEAN"
                    }
                }
            }
            await new Promise((r, j) => {
                ajax.set(data, 'setDataInput', '', r)
            }).then(() => {
                if (imageUpload.img[0] !== undefined) {
                    upload();
                } else {
                    // console.log('IMG Null');
                }
                // setStatusVisible(true,{...statusVisible});
                // alert("บันทึก สำเร็จ");
            })
        })
    }
    const chkStatusUpdate = () => {
        setStatusVisible(true, { ...statusVisible })
    }
    const handleOnClickUpdateCallback = (status) => {
        setStatusVisible(false)
        if (status === true) {
            updateData();
            CallBackUpdate();
        }
    }
    //Start Post Data To Delete
    let objData = {
        id: dataRow.id,
        machineId: dataRow.machine_id,
        materialId: dataRow.material_id
    };

    return (
        <>
            <DropDownPicker
                items={type}
                defaultValue={dataRow.type_id}
                onChangeItem={(item) => chageType(item)}
                // onChangeItem={item => (value.type_id = item.value)}
                placeholder="ประเภท"
                itemStyle={{ justifyContent: 'flex-start' }}
                containerStyle={DropDownStyle}
                style={BoxDropDownStyle}
                // style={{ backgroundColor: '#fafafa', borderColor: '#000' }}
                dropDownStyle={{ backgroundColor: '#fff' }}
            />
            <TextInput
                theme={style.styleInput}
                style={style.inputStyle, { display: showInputType }}
                // mode="outlined"
                label="ประเภท"
                onChangeText={text => dataRow.type_name = text}
            />
            <TextInput
                theme={styleInput}
                style={inputStyle}
                // mode="outlined"
                label="ชื่อ-นามสกุล"
                value={dataRow.data_name}
                onChangeText={text => { dataRow.data_name = text, setDataRow({ ...dataRow }) }}
            />
            <TextInput
                theme={styleInput}
                style={inputStyle}
                // mode="outlined"
                label="ชื่อร้าน"
                value={dataRow.data_shop}
                onChangeText={text => { dataRow.data_shop = text, setDataRow({ ...dataRow }) }}
            />
            <TextInput
                theme={styleInput}
                style={inputStyle}
                // mode="outlined"
                label="เบอร์"
                value={dataRow.data_phone}
                onChangeText={text => { dataRow.data_phone = text, setDataRow({ ...dataRow }) }}
            />
            <TextInput
                theme={styleInput}
                style={inputStyle}
                // mode="outlined"
                label="ที่อยู่"
                value={dataRow.data_address}
                onChangeText={text => { dataRow.data_address = text, setDataRow({ ...dataRow }) }}
            />
            <DropDownPicker
                items={provinces}
                defaultValue={dataRow.province_id}
                onChangeItem={(item) => getDistricts(item)}
                placeholder="จังหวัด"
                itemStyle={{ justifyContent: 'flex-start' }}
                containerStyle={DropDownStyle}
                style={BoxDropDownStyle}
                dropDownStyle={{ backgroundColor: '#fff' }}
            />
            <DropDownPicker
                items={districts}
                defaultValue={dataRow.district_id}
                onChangeItem={item => { dataRow.district_id = item.value, setDataRow({ ...dataRow }) }}
                placeholder="อำเภอ"
                itemStyle={{ justifyContent: 'flex-start' }}
                containerStyle={DropDownStyle}
                style={BoxDropDownStyle}
                dropDownStyle={{ backgroundColor: '#fff' }}
            />
            <TextInput
                theme={styleInput}
                style={inputStyle}
                // mode="outlined"
                label="LINE ID"
                value={dataRow.data_line}
                onChangeText={text => { dataRow.data_line = text, setDataRow({ ...dataRow }) }}
            />
            <TextInput
                theme={styleInput}
                style={inputStyle}
                // mode="outlined"
                label="Facebook"
                value={dataRow.data_facebook}
                onChangeText={text => { dataRow.data_facebook = text, setDataRow({ ...dataRow }) }}
            />
            <TextInput
                theme={styleInput}
                style={inputStyle}
                // mode="outlined"
                label="Instagram"
                value={dataRow.data_instagram}
                onChangeText={text => { dataRow.data_instagram = text, setDataRow({ ...dataRow }) }}
            />

            {/* APPEND MACHINE SHOW ON DISPLAY START.*/}
            <FlatList
                contentContainerStyle={editStyle, { position: 'relative' }}
                data={addMachine.viewItem}
                renderItem={({ item, index }) =>
                    <View style={{ marginBottom: 10 }}>
                        <SwipeRow rightOpenValue={-60} style={{ height: 250 }}>
                            <View style={standaloneRowBack}>
                                <TouchableOpacity
                                    activeOpacity={0.5}
                                    style={BtnDelete}
                                    onPress={delViewMachine(index)}
                                >
                                    {/* <Text onPress={delViewMachine(index)}>Done</Text> */}
                                    <MaterialIcons name="delete" size={26} color="#ff4a4a" />
                                </TouchableOpacity>
                            </View>
                            <View style={standaloneRowFront}>
                                <View style={{ flexDirection: "row" }}>
                                    <View style={{ flex: 1, }}>
                                        <DropDownPicker
                                            items={typeMachine}
                                            defaultValue={addMachine.viewItem[index].machine_type_id}
                                            onChangeItem={item => addMachine.viewItem[index].machine_type_id = item.value}
                                            placeholder="เครื่องใช้ไฟฟ้า"
                                            itemStyle={{ justifyContent: 'flex-start' }}
                                            containerStyle={DropDownStyle}
                                            style={BoxDropDownRowStyle}
                                            dropDownStyle={{ backgroundColor: '#fff' }}
                                        />
                                    </View>
                                </View>
                                <View>
                                    {/* <TextInput
                                        mode="outlined"
                                        label="เครื่องใช้ไฟฟ้า*(เมื่อเลือกอื่นๆ)"
                                        value={addMachine.viewItem[index].machine_type}
                                        onChangeText={text => setMachineTypeName(text, index)}
                                    /> */}
                                    <TextInput
                                        theme={styleInput}
                                        style={inputStyle}
                                        // mode="outlined"
                                        label="ชื่อ/รุ่น"
                                        value={addMachine.viewItem[index].machine_name}
                                        onChangeText={text => setMachineName(text, index)}
                                    />
                                    <Button
                                        icon="calendar"
                                        mode="outlined"
                                        color="#4c669f"
                                        style={{ justifyContent: 'center', marginTop: 10, height: 50 }}
                                        onPress={() => getDate(index)}
                                    >
                                        {pickDate[index].date ? 'ประกันถึง : ' + moment(pickDate[index].date).locale('th').format('DD MMMM , YYYY') : 'เลือกวันประกัน'}
                                    </Button>
                                    <DateTimes id={localStore} show={show} toggle={toggle} birthDate1={birthDate1} getDatePicker={getDatePicker} />
                                </View>
                            </View>
                            {/*<Button icon="delete" mode="outlined" style={{ height: 60, alignItems: 'center', justifyContent: 'center', marginTop: 10 }} onPress={delViewMachine(index)}>Delete</Button> */}
                        </SwipeRow>
                    </View>
                }
            />
            <Button
                mode="contained"
                color="#4c669f"
                style={{
                    backgroundColor: '#4c669f',
                    // flex: 1,
                    // alignItems: 'center',
                    justifyContent: 'center',
                    marginBottom: 10,
                }}
                onPress={() => addViewMachine()}
            >
                <FontAwesome name="plus-circle" size={30} color="#fff" />
                {/* <Text>เพิ่มเครื่องใช้ไฟฟ้า</Text> */}
            </Button>
            <View style={BtnViewLine} />

            {/* APPEND MACHINE SHOW ON DISPLAY END.*/}
            {/* APPEND MATERAIL SHOW ON DISPLAY START.*/}
            <FlatList
                contentContainerStyle={editStyle, { position: 'relative' }}
                data={addMaterial.viewItem}
                renderItem={({ item, index }) =>
                    <View style={{ marginBottom: 10 }}>
                        <SwipeRow rightOpenValue={-60} style={{ height: 250 }}>
                            <View style={standaloneRowBack}>
                                <TouchableOpacity
                                    activeOpacity={0.5}
                                    style={BtnDelete}
                                    onPress={delViewMaterial(index)}
                                >
                                    {/* <Text onPress={delViewMachine(index)}>Done</Text> */}
                                    <MaterialIcons name="delete" size={26} color="#ff4a4a" />
                                </TouchableOpacity>
                            </View>
                            <View style={standaloneRowFront}>
                                <View style={{ flexDirection: "row" }}>
                                    <View style={{ flex: 1, }}>
                                        <DropDownPicker
                                            items={typeMaterial}
                                            defaultValue={addMaterial.viewItem[index].material_type_id}
                                            onChangeItem={item => addMaterial.viewItem[index].material_type_id = item.value}
                                            placeholder="วัตถุดิบที่ใช้"
                                            itemStyle={{ justifyContent: 'flex-start' }}
                                            containerStyle={DropDownStyle}
                                            style={BoxDropDownRowStyle}
                                            dropDownStyle={{ backgroundColor: '#fff' }}
                                        />
                                    </View>
                                </View>
                                <View>
                                    {/* <TextInput
                                        theme={styleInput}
                                        style={inputStyle}
                                        // mode="outlined"
                                        label="วัตถุดิบที่ใช้*(เมื่อเลือกอื่นๆ)"
                                        value={addMaterial.viewItem[index].material_type}
                                        onChangeText={text => setMaterialTypeName(text, index)}
                                    /> */}
                                    <TextInput
                                        theme={styleInput}
                                        style={inputStyle}
                                        // mode="outlined"
                                        label="ชื่อ/ชนิด"
                                        value={addMaterial.viewItem[index].material_name}
                                        onChangeText={text => setMaterialName(text, index)}
                                    />
                                    <TextInput
                                        theme={styleInput}
                                        style={inputStyle}
                                        // mode="outlined"
                                        label="ราคา/กก"
                                        value={addMaterial.viewItem[index].material_price}
                                        onChangeText={text => setMaterialPrice(text, index)}
                                    />
                                </View>
                            </View>
                            {/* <Button icon="delete" mode="outlined" style={{ height: 60, alignItems: 'center', justifyContent: 'center', marginTop: 10 }} onPress={delViewMaterial(index)}>Delete</Button> */}
                        </SwipeRow>
                    </View>
                }
            />
            <Button
                mode="contained"
                color="#4c669f"
                style={{
                    backgroundColor: '#4c669f',
                    // flex: 1,
                    // alignItems: 'center',
                    justifyContent: 'center',
                    marginBottom: 10,
                }}
                onPress={() => addViewMaterial()}
            >
                <FontAwesome name="plus-circle" size={30} color="#fff" />
                {/* <Text>เพิ่มวัตถุดิบ</Text> */}
            </Button>
            <View style={BtnViewLine} />
            {/* APPEND MATERAIL SHOW ON DISPLAY END.*/}

            {/* UPLOAD IMG SHOW ON DISPLAY END.*/}

            {data.img_name !== '' ?
                <View style={{ flex: 1, height: 320 }}>
                    <ScrollView
                        horizontal={true}
                    >
                        <View style={{ flexDirection: "row" }}>
                            {image.img.map((item, i) => renderImage(item, i))}
                        </View>
                    </ScrollView>
                    <Button
                        color="#4c669f"
                        icon="upload"
                        mode="contained"
                        // onPress={() => navigation.navigate('Detail', 'data')}
                        onPress={() => { uploadImg(), clickAddImg(false) }}
                        style={{ justifyContent: 'center', marginTop: 20, marginBottom: 20, height: 60 }}
                    >
                        <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: 16 }}>เพิ่มรูปภาพ</Text>
                    </Button>
                </View>
                :
                <View style={{ flex: 1, height: 320, }}>
                    <TouchableHighlight
                        underlayColor={'#8a8a8a'}
                        onPress={() => { uploadImg() }}
                        style={{ flex: 1, borderWidth: 1, borderRadius: 10, alignItems: 'center', justifyContent: 'center', height: '100%' }}
                    >
                        <View
                            style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
                        >
                            <FontAwesome5 name="images" size={60} color="#d4d4d4" />
                            <Text style={{ color: '#d4d4d4', fontWeight: 'bold', fontSize: 16 }}>ไม่มีรูปภาพ</Text>
                            <Text style={{ color: '#d4d4d4', fontWeight: 'bold', fontSize: 14 }}>กรุณา กด เพื่ออัพโหลดรูปภาพ</Text>
                        </View>
                    </TouchableHighlight>

                </View>
            }
            {/* UPLOAD IMG SHOW ON DISPLAY END.*/}
            <Button
                icon="square-edit-outline"
                mode="contained"
                color="#4c669f"
                onPress={chkStatusUpdate}
                style={{ justifyContent: 'center', marginTop: 20, marginBottom: 20, height: 60 }} >
                Save
            </Button>

            <ModalAlert logTxt='ต้องการแก้ไขข้อมูล' statusVisible={statusVisible} toggle={handleOnClickUpdateCallback} />
            <ModalImg dataImg={dataImg} image={image} statusVisible={statusImgVisible} toggle={handleOnClickImgCallback} />
            {/* <Button icon="delete" mode="contained" onPress={() => delData(objData)} style={{ justifyContent: 'center', marginTop: 20, marginBottom: 20, height: 60 }}>
                Delete
            </Button> */}
        </>
    );
};
// export to render
export default EditData;
// style
const headerStyle = {
    viewStyle: {
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        height: 100 % '',
        paddingTop: 15,
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        elavation: 0,
        position: 'relative'
    },
    textStyle: {
        fontSize: 20
    }, inputStyle: {
        marginTop: 10,
        marginBottom: 10,

    }, DropDownStyle: {
        height: 60,
        marginTop: 10,
    }, editStyle: {
        backgroundColor: '#FFF',
        // justifyContent :'center',
        // alignItems:'center',
        Top: 0,
        paddingLeft: 20,
        paddingRight: 20,
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        elavation: 2,
        position: 'relative'
    },
    BoxDropDownStyle: {
        borderWidth: 1,
        borderColor: '#4c669f'
    },
    BoxDropDownRowStyle: {
        flex: 1,
        borderWidth: 1,
        borderColor: '#4c669f',
        // width: 300
    },
    standaloneRowFront: {
        // flexDirection: 'row',
        // alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff'
        // height: '100%',
        // borderWidth:1,
    },
    standaloneRowBack: {
        flex: 1,
        alignItems: 'center',
        // backgroundColor: '#bdd2ff',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        // borderWidth: 1,
        borderRadius: 20,
        height: '100%'
        // justifyContent: 'space-between',
    }, BtnViewLine: {
        flex: 1,
        borderColor: '#4c669f',
        borderBottomWidth: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 10,
        width: "100%"
    },
    BtnDelete: {
        // flex: 1,
        alignItems: 'flex-end',
        paddingRight: 20,
        justifyContent: 'center',
        // backgroundColor: '#ff4a4a',
        height: '100%',
        // borderLeftWidth:1,
        borderColor: '#ff4a4a',


        borderTopRightRadius: 20,
        borderBottomRightRadius: 20,
        width: 60
    }
};
