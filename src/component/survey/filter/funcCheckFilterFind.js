// =======================================================================
// Init/State Componant
// =======================================================================

import React, { useState } from 'react';
import CheckBox from '@react-native-community/checkbox';
import { View, Text, TouchableHighlight } from 'react-native';
import { StyleContain } from '../../style/funcStyle';
const style = StyleContain();

// =======================================================================
// CheckList Screen
// =======================================================================
const dataRow = [
    {
        id:'1',
        typeFilter:'ชื่อลูกค้า',
        color: '#292929',
        fontWeight:'bold',
        checkStatus: true
    },
    {
        id:'2',
        typeFilter:'ชื่อร้าน',
        color: '#DDDDDD',
        fontWeight:'100',
        checkStatus: false
    }
];

export default function CheckListScreen({searchType, resetSearch}) {
    const [toggleCheckBox, setToggleCheckBox] = useState(dataRow);
    const handleCheckList = (newValue, index) => {
        searchType(toggleCheckBox[index]);
        resetSearch();
        dataRow.forEach((ele,idx)=>{
            if (idx !== toggleCheckBox[index].id) {
                toggleCheckBox[idx].checkStatus = false;
                toggleCheckBox[idx].color = '#8d8d8d';    
                toggleCheckBox[idx].fontWeight = '100';    
            }
            toggleCheckBox[index].checkStatus = newValue;
            toggleCheckBox[index].color = '#292929';   
            toggleCheckBox[index].fontWeight = 'bold';    
        });
        setToggleCheckBox({ ...toggleCheckBox });
    }
    return (
        <View style={{ flex: 0.5, flexDirection: 'row', alignItems: 'center', paddingHorizontal: 10 }}>
            <Text>ค้นหาจาก</Text>
            {dataRow.map((data, index) => (
                <TouchableHighlight
                    key={index}
                    activeOpacity={0.6}
                    underlayColor="#FFF9DB"
                    style={{borderRadius:20,padding: 10}}
                    onPress={() => handleCheckList(true, index)}
                >
                    <>
                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                            <CheckBox
                                disabled={false}
                                tintColors={{ true:'#ffd506', false: '#8d8d8d' }}
                                // boxType={'circle'}
                                value={toggleCheckBox[index].checkStatus}
                                onValueChange={(newValue) => handleCheckList(newValue, index)}
                            />
                            <Text style={{color:toggleCheckBox[index].color, fontWeight:toggleCheckBox[index].fontWeight }}>{data.typeFilter}</Text>
                        </View>
                    </>
                </TouchableHighlight>
            ))
            }
        </View>
    );
}