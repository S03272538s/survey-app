// =======================================================================
// Init/State Componant
// =======================================================================

import React,{useState} from 'react';
import CheckListScreen from './funcCheckFilterFind';
import { TextInput } from 'react-native-paper';
import { View } from 'react-native';
import { MaterialIcons, FontAwesome } from '@expo/vector-icons';
import { StyleContain } from '../../style/funcStyle';
const style = StyleContain();


// =======================================================================
// CheckList Screen
// =======================================================================

export default function SearchData({ dataInit, searchFilter }) {
    const [valueSearch, setValueSearch] = useState('')

    const resetSearch=()=>{
        setValueSearch('');
        searchFilter(dataInit);
    }
    return (
        <View style={{ flex: 1}}>
            <View style={{ flex: 1, flexDirection: 'row',}}>
                <View style={{ flex: 1}}>
                    <TextInput
                        theme={style.styleInput}
                        style={{flex: 1}}
                        label="ค้นหา"
                        value={valueSearch}
                        onChangeText={(value) => { searchFilter(searchFilterFunctions(value, dataInit)),setValueSearch(value);}}
                    />
                </View>
                <View style={{ flex: 0.1, justifyContent: 'center' }}>
                    <FontAwesome name="search" size={24} color="black" />
                </View>
            </View>
            <View style={{ flex: 1,paddingVertical:10}}>
                <CheckListScreen 
                    searchType={searchType}
                    resetSearch={resetSearch}
                />
            </View>
        </View>
    );
}
// =======================================================================
// Function Search  
// =======================================================================

let typeSearch = 'ชื่อลูกค้า';

export function searchFilterFunctions(searchText, data) {
    let newData = [];
    console.log(searchText);
    if (searchText) {
        newData = data.filter(function (item) {
        if(typeSearch==='ชื่อลูกค้า'){
                const itemData = item.data_name.toUpperCase();
                const textData = searchText.toUpperCase();
                return itemData.includes(textData);
        }
        if(typeSearch==='ชื่อร้าน'){
                const itemData = item.data_shop.toUpperCase();
                const textData = searchText.toUpperCase();
                return itemData.includes(textData);
        }
        });
        
        return newData;
    }
    return data;
}

export function searchType(type) {
    typeSearch = type.typeFilter;
}