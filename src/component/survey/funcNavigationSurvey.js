// =======================================================================
// Init/State Componant
// =======================================================================

import * as React from 'react';
import { View,Text,Button } from 'react-native';
import AddSurvey from './funcAddSurvey';
import ShowDataTable from './showDataTable';
import MapView from '../mapView/funcMapView';
import Header from '../header/funcHeaderTitle';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { StyleContain } from '../style/funcStyle';
const style = StyleContain();

// =======================================================================
// Navigations
// =======================================================================
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
const Stack = createStackNavigator();
const Tab = createMaterialTopTabNavigator();

// =======================================================================
// Store Local
// =======================================================================

import { createStore } from 'redux';
const reduxStore = (state, action) => {
  switch (action.type) {
    case 'Add': state = action.playload;
      break;
    default: state = state;
      break;
  }return state;
};
const localStore = createStore(reduxStore, 0);

// =======================================================================
// MapView Page Navigation
// =======================================================================

function MapScreen({ dataUser, handleClickLogout }) {
  return (
    <View style={style.navigationsView}>
      <MapView dataUser={dataUser} handleClickLogout={handleClickLogout} />
    </View>
  );
}

// =======================================================================
// DataTable Page Navigation
// =======================================================================

function DataScreen({ dataUser, handleClickLogout }) {
  
  return (
    <View style={style.navigationsView}>
      <ShowDataTable dataUser={dataUser} handleClickLogout={handleClickLogout} />
    </View>
  );
}

// =======================================================================
// AddData Page Navigation
// =======================================================================

function SurveyScreen({ dataUser, handleClickLogout }) {
  return (
    <View style={style.navigationsView}>
      <AddSurvey dataUser={dataUser} handleClickLogout={handleClickLogout} />
    </View>

  );
}

function TopNavigation({ dataUser, handleClickLogout }) {
  return (
    <View style={style.viewFlex}>
        <View style={style.viewFlex}>
      <Tab.Navigator>
            <Tab.Screen
              name="Map"
              options={{
                tabBarLabel: 'แผนที่',
                tabBarIcon: ({ color, size }) => (
                  <MaterialCommunityIcons name="map" color={color} size={size} />
                )
              }} >
              {(props) => <MapScreen dataUser={dataUser} handleClickLogout={handleClickLogout} />}
            </Tab.Screen>
            <Tab.Screen
              name="Survey"
              options={{
                tabBarLabel: 'เพิ่มข้อมูลสำรวจ',
                tabBarIcon: ({ color, size }) => (<MaterialCommunityIcons name="walk" color={color} size={size} />)
              }}
            >
              {(props) => <SurveyScreen dataUser={dataUser} handleClickLogout={handleClickLogout} />}
            </Tab.Screen>
            <Tab.Screen
              name="Data"
              options={{
                tabBarLabel: 'ข้อมูลการสำรวจ',
                tabBarIcon: ({ color, size }) => (
                  <MaterialCommunityIcons name="database" color={color} size={size} />
                )
              }} >
              {(props) => <DataScreen dataUser={dataUser} handleClickLogout={handleClickLogout} />}
            </Tab.Screen>
          </Tab.Navigator>
    </View>
    </View>

  );
}

// =======================================================================
// Navigation Componant
// =======================================================================

export default function NavigationSurvey({ dataUser, handleClickLogout}) {
  if (dataUser !== undefined) {
    localStore.dispatch({ type: 'Add', playload: dataUser })
  }
  return (
    <View style={style.viewFlex}>
        <View style={style.viewFlex}>
        <Stack.Navigator>
                <Stack.Screen
                    name="Survey1"
                    // component={DataScreen}
                    options={{header:(props) => (null)}}
                >
                  {(props) => <TopNavigation dataUser={dataUser} handleClickLogout={handleClickLogout} />}
                </Stack.Screen>
                <Stack.Screen
                    name="Upload"
                    component={DataScreen}
                    options={{
                        title: 'อัพโหลดรูปภาพ',
                        headerStyle: style.headerStyle,
                        headerTintColor: style.headerTintColor,
                        headerTitleStyle: style.headerTitleStyle,
                    }}
                />
            </Stack.Navigator>
        </View>
    </View>
  );
}