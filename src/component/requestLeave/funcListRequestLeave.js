// =======================================================================
// Init/State Componant
// =======================================================================

import React, { useState, useEffect } from 'react';
import { Text, View, ScrollView, Image, Linking, TouchableHighlight, ActivityIndicator } from 'react-native';
import moment from 'moment';

import { StyleContain } from '../style/funcStyle';
const style = StyleContain();

// =======================================================================
// Repository Data
// =======================================================================

import { getData, getAccessRight, getPosition, getLeave, getLeaveType } from '../../repository/funcGetData';

// =======================================================================
// NavigatorMenu Componant
// =======================================================================

export default function MenuListRequestLeave({ navigation, dataUser }) {
    const [data, setData] = useState();
    useEffect(() => {
        (async () => {
            setData(await getData('getLeave', dataUser.id_application));
        })();
    }, [])
    console.log(moment(new Date).format('YYYY-MM-DD'));
    console.log(data);
    return data ? (
        <View style={{ flex: 1 }}>
            <ScrollView style={{ flex: 1, backgroundColor: '#FEFEFE' }}>
                <View style={{ flex: 1, paddingTop: 10, }}>
                    {data.map((leave, idx) => (
                        <TouchableHighlight
                            key={idx}
                            activeOpacity={0.6}
                            style={style.touchFeed}
                            underlayColor={'#8a8a8a'}
                            onPress={() => { navigation.navigate('FeedDetail', { dataFeed: feed }) }}
                        >
                            <View>
                                <View style={style.viewFeedTitleContent}>
                                    <Text style={style.txtFeedTitle}>ยื่นเรื่อง {leave.leave_typeTxt} เมื่อ {moment(leave.create_date).format('วันที่ DD เดือน MMMM ปี YYYY')}</Text>
                                </View>
                                <View style={style.viewFeedIntrolContent}>
                                    <Text style={style.txtLeaveIntrol}>ผู้ขอลา : {leave.leave_requestorTxt}</Text>
                                    <Text style={style.txtLeaveIntrol}>ประเภทการลา : {leave.leave_typeTxt}</Text>
                                    <Text style={style.txtLeaveIntrol}>สาเหตุการลา : {leave.leave_remark}</Text>
                                    <Text style={style.txtLeaveIntrol}>ระยะเวลาในการลารวม : {leave.total_leave} นาที /  {leave.total_leave/60} ชั่วโมง / {leave.day_leave} วัน</Text>
                                    <Text style={style.txtLeaveIntrol}>เมื่อวันที่ :{moment(leave.leave_date_from1).format(' DD เดือน MMMM ปี YYYY')}</Text>
                                    <Text style={style.txtLeaveIntrol}>ถึงวันที่ :{moment(leave.leave_date_from2).format(' DD เดือน MMMM ปี YYYY')}</Text>
                                </View>

                                <View style={style.viewFeedIntrolContent}>
                                    <Text style={style.txtLeaveIntrol}>ผู้รับเรื่อง : {leave.lbl_name_app} </Text>
                                </View>
                                <View style={style.viewLeaveStatus}>
                                    {leave.chk_cancel==='0'?
                                    <Text style={style.txtLeaveAppprove}>สถานะ {leave.chk_app==="1"||"2"? 'อนุมัติแล้ว' : 'รอการอนุมัติ'}</Text>
                                    :
                                    <Text style={style.txtLeaveCancel}>สถานะ ไม่ได้รับการอนุมัติ</Text>
                                    }
                                </View>
                            </View>
                        </TouchableHighlight>
                    ))
                    }
                </View>
            </ScrollView>
        </View>
    ) : (
        <View style={{ flex: 1, backgroundColor: '#FEFEFE', alignItems: 'center', justifyContent: 'center' }}>
            <Text style={{ color: '#292929', fontWeight: 'bold', fontSize: 16, paddingBottom: 20 }}> กำลังดึงข้อมูล... </Text>
            <ActivityIndicator size={70} color={'#0580FF'} />
        </View>
    )
}