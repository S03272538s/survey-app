// =======================================================================
// Init/State Componant
// =======================================================================

import React, { useState } from 'react';
import CheckBox from '@react-native-community/checkbox';
import { View, Text, TouchableHighlight } from 'react-native';
import { StyleContain } from '../style/funcStyle';
const style = StyleContain();

// =======================================================================
// CheckList Screen
// =======================================================================

export default function CheckFileUpload({ dataUser, callBackFileUpload}) {
    const [dataCheckBox, setDataCheckBox] = useState([
        {id:'0', value: 'ไม่มีไฟล์แนบ', checkStatus:true},
        {id:'1', value: 'อัพโหลดไฟล์', checkStatus:false},
        {id:'2', value: 'ถ่ายรูป', checkStatus:false},
    ])
    const [toggleCheckBox, setToggleCheckBox] = useState(dataCheckBox)
    

    const handleCheckList = (newValue, index) => {
        callBackFileUpload(toggleCheckBox[index]);
        dataCheckBox.forEach((element, idx)=>{
            if (idx !== toggleCheckBox[index].id) {
                console.log( toggleCheckBox[idx].checkStatus);
                toggleCheckBox[idx].checkStatus = false;
            }
            toggleCheckBox[index].checkStatus = newValue;
        });
        setToggleCheckBox({ ...toggleCheckBox });
    }   
    return ( 
        dataCheckBox.map((data, index) => (
            <TouchableHighlight
                key={index}
                activeOpacity={0.6}
                underlayColor="#DDDDDD"
                onPress={() => handleCheckList(true, index)}
               
            >
                <>
                    <View 
                    style={style.checkListContent}>
                        <CheckBox
                            disabled={false}
                            // boxType={'circle'}
                            value={toggleCheckBox[index].checkStatus}
                            onValueChange={(newValue) => handleCheckList(newValue, index)}
                        />
                        <Text>{data.value}</Text>
                    </View>
                </>
            </TouchableHighlight>
        ))
    );
}