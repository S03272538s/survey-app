// =======================================================================
// Init/State Componant
// =======================================================================

import React, {useState, useEffect} from 'react';
import { View, Text, ScrollView, Animated, Dimensions, ActivityIndicator } from 'react-native';
import moment from 'moment';
import { FontAwesome, MaterialCommunityIcons, MaterialIcons } from '@expo/vector-icons';
import MenuRequestLeave from './funcRequestLeave';
import MenuListRequestLeave from './funcListRequestLeave';
import { StyleContain } from '../style/funcStyle';
const style = StyleContain();
const { width } = Dimensions.get('window');

import HeaderMenu from '../header/funcHeaderMenu';
// =======================================================================
// Repository Data
// =======================================================================

import { getData, getAccessRight, getPosition, getLeave, getLeaveType } from '../../repository/funcGetData';

// =======================================================================
// Navigations
// =======================================================================
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
const topTab = createMaterialTopTabNavigator();

// =======================================================================
// Render Home Page
// =======================================================================

export default function NavigationRequestLeave({ navigation, dataUser, callBackData }) {
    const [leave, setLeave] = useState([{totalLeave:'asdasdasdasdasd'}]);
    const [healt, setHealt] = useState(
        [
            {title:'สุขภาพการทำงาน',id:'1'},
            {title:'ตัวชี้วัด',id:'2'},
        ]
        );
    const [leaveType, setLeaveType] = useState();
    const [detailEmp, setDetailEmp] = useState();

    let arrDeatil = [];
    useEffect(() => {
        (async () => {
            setLeave( await getLeave(dataUser, 'getLeave', ''));
            setLeaveType( await getLeaveType('getLeaveType', ''));
            setDetailEmp(await getPosition(dataUser, 'getDetailEmp', ''));

        })();
    },[])
    const [feedNews, setFeedNews] = useState(
        [{ picture_name:'งานที่  1' },{ picture_name:'งานที่  2' },{ picture_name:'งานที่  3' }]
        )
    return (
        <View style={style.viewFlex}>
            <View style={style.viewFlex}>
                <HeaderMenu navigation={navigation} namePage={'ลางาน'} />
                {leaveType && detailEmp ?
                <topTab.Navigator
                    initialRouteName={moment(new Date).format('dddd')}
                    tabBarOptions={{
                        labelStyle: { fontSize: 15, fontWeight: 'bold' },
                        tabStyle: { width: width / 2 },
                        activeTintColor: '#ffd506',
                        inactiveTintColor: '#FFF9DB',
                        indicatorStyle: { backgroundColor: '#ffe774' },
                        style: { backgroundColor: '#3865b7', borderTopWidth: 1, borderTopColor: '#ffe774' },
                        scrollEnabled: true,
                        pressColor: '#ffe774'
                    }}
                >
                        <topTab.Screen
                            name='SentRequestLeave'
                            options={{
                                 tabBarLabel: 'ทำเรื่องขอลา',
                            }} >
                            {(props) => <MenuRequestLeave navigation={props.navigation} dataUser={dataUser} detailEmp={detailEmp} callBackData={callBackData}/>}
                        </topTab.Screen>
                        <topTab.Screen
                            name='ListRequestLeave'
                            options={{
                                 tabBarLabel: 'ข้อมูลการลา',
                            }} >
                            {(props) =><MenuListRequestLeave navigation={props.navigation} dataUser={dataUser}/>}
                        </topTab.Screen>
                </topTab.Navigator>
            : 
            <View style={{ flex: 1, backgroundColor: '#FEFEFE', alignItems: 'center', justifyContent: 'center' }}>
                 <Text style={{ color: '#292929', fontWeight: 'bold', fontSize: 16, paddingBottom: 20 }}> กำลังดึงข้อมูล... </Text>
             <ActivityIndicator size={70} color={'#0580FF'} />
             </View>
            }
            </View>
        </View>
    )
};