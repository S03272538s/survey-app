// =======================================================================
// Init/State Componant
// =======================================================================

import React, { useState } from 'react';
import CheckBox from '@react-native-community/checkbox';
import { View, Text, TouchableHighlight } from 'react-native';
import { StyleContain } from '../style/funcStyle';
const style = StyleContain();

// =======================================================================
// CheckList Screen
// =======================================================================

export default function CheckListLeave({ dataUser, callBackCheckBox}) {
    const [dataCheckBox, setDataCheckBox] = useState([
        {id:'0', value: 'ลาแบบทั้งวัน', checkStatus:true},
        {id:'1', value: 'ลาแบบเลือกเวลา', checkStatus:false},
        {id:'2', value: 'ลาแบบเลือกวันที่', checkStatus:false},
    ])
    // const [toggleCheckBox, setToggleCheckBox] = useState(nearLocations);
    const [toggleCheckBox, setToggleCheckBox] = useState(dataCheckBox)
    

    const handleCheckList = (newValue, index) => {
        callBackCheckBox(toggleCheckBox[index]);
        dataCheckBox.forEach((element, idx)=>{
            if (idx !== toggleCheckBox[index].id) {
                console.log( toggleCheckBox[idx].checkStatus);
                toggleCheckBox[idx].checkStatus = false;
            }
            toggleCheckBox[index].checkStatus = newValue;
        });
        setToggleCheckBox({ ...toggleCheckBox });
    }   
    return ( 
        dataCheckBox.map((data, index) => (
            <TouchableHighlight
                key={index}
                activeOpacity={0.6}
                underlayColor="#DDDDDD"
                onPress={() => handleCheckList(true, index)}
               
            >
                <>
                    {/* {data.distance >= 5 ?
                        <Text style={style.checkListTxtAlpha}> อีก {data.distance} เมตร จะถึง</Text>
                        :
                        <Text style={style.checkListTxtAlpha}> คุณอยู่ที่ </Text>
                    } */}
                       
                    <View 
                    // key={index} 
                    style={style.checkListContent}>
                        <CheckBox
                            key={index}
                            disabled={false}
                            // boxType={'circle'}
                            value={toggleCheckBox[index].checkStatus}
                            onValueChange={(newValue) => handleCheckList(newValue, index)}
                        />
                        <Text>{data.value}</Text>
                        {/* <Text>{data.cmpName}({data.cmpAbbreviation})</Text> */}
                    </View>
                </>
            </TouchableHighlight>
        ))
    );
}