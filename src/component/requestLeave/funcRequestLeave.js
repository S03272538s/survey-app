// =======================================================================
// Init/State Componant
// =======================================================================

import React, { useState, useEffect } from 'react';
import { Text, View, ScrollView, Image, Linking, TouchableHighlight } from 'react-native';
import HeaderMenu from '../header/funcHeaderMenu';
import Constants from 'expo-constants';
import { StyleContain } from '../style/funcStyle';
import Logout from '../../login-out/funcLogout';
import { Button, TextInput, } from 'react-native-paper';
import DropDownPicker from 'react-native-dropdown-picker';
import { DateTimes } from '../modal/funcModalDateTimes';
import moment from 'moment';
import * as DocumentPicker from 'expo-document-picker';
import { FontAwesome, MaterialCommunityIcons, MaterialIcons } from '@expo/vector-icons';
import { CommonActions } from '@react-navigation/native';
import ModalAlertLogin from '../../login-out/funcModalAlertLogin';

import DateTimePickerModal from 'react-native-datetimepicker-modal';
import CheckListLeave from './funcCheckListLeave';
import CheckFileUpload from './funcCheckFileUpload';
const style = StyleContain();

// =======================================================================
// Repository Data
// =======================================================================

import { getData, getAccessRight, getPosition, getLeave, getLeaveType } from '../../repository/funcGetData';
import { postRequestLeave, files} from '../../repository/funcPostData';

// =======================================================================
// NavigatorMenu Componant
// =======================================================================

export default function MenuRequestLeave({ navigation, dataUser, detailEmp, callBackData }) {
    const [leaveType, setLeaveType] = useState();
    const [leaveTypeDetail, setLeaveTypeDetail] = useState();
    const [showTypeDetail, setTypeDetail] = useState();
    const [showStart, showModalStart] = useState(false);
    const [showEnd, showModalEnd] = useState(false);
    const toggleStart = () => showModalStart(!showStart);
    const toggleEnd = () => showModalEnd(!showEnd);

    const [showTimeStart, setshowTimeStart] = useState(false);
    const [showTimeEnd, setshowTimeEnd] = useState(false);
    const toggleTimeStart = () => setshowTimeStart(!showTimeStart);
    const toggleTimeEnd = () => setshowTimeEnd(!showTimeEnd);

    const [statusVisibleDone, setStatusVisibleDone] = useState(false);


    const [valuePost, setValuePost] = useState({
        fileUpload: '',
        id_application: detailEmp.id_application,
        id_position: detailEmp.id_position,
        id_div: detailEmp.id_division,
        id_dept: detailEmp.id_dept,
        id_company: detailEmp.id_company,
        id_em_detail: detailEmp.id_emp,
        rw_doc_no: '',
        leave_requestor: detailEmp.id_emp,
        leave_requestorTxt: detailEmp.cont_lbl_name,
        leave_type: '',
        leave_typeTxt: '',
        leave_date_from1: '',
        leave_date_from2: '',
        leave_days1: '',
        leave_days1Txt: '',
        leave_time_from1: '',
        leave_time_from1Txt: '',
        leave_time_to1: '',
        leave_time_to1Txt: '',
        check_hour: '',
        day_leave: '',
        hour_leave: '',
        total_leave: '',
        leave_remark: '',
        leave_name_file: '',
        chk_qc: 0,
        lbl_name_qc: '',
        comment_qc: '',
        chk_app: 2,
        lbl_name_app: '',
        comment_app: '',
        chk_cancel: 0,
        lbl_name_cancel: '',
        comment_cancel: '',
        rw_requestor: '',
    });


    useEffect(() => {
        (async () => {
            let arrTypeLeave = [], detailTypeLeave = [];
            const callbackLeaveType = await getLeaveType('getLeaveType', '');
            await callbackLeaveType.forEach(element => {
                detailTypeLeave.push({
                    id: element.id,
                    leave_deadline:element.leave_deadline,
                    leave_doc:element.leave_doc
                });
                arrTypeLeave.push({
                    label: element.leave_typeTxt,
                    value: element.id
                });
            });
            setLeaveTypeDetail(await detailTypeLeave);
            setLeaveType(await arrTypeLeave);
        })();
    }, [])

    useEffect(() => {
        (async () => {
        const NumDocPerDay = await getData('getRequestLeave', '');
        NumDocPerDay.forEach((element, idx) => {
            if(element.create_date === moment(new Date).format('YYYY-MM-DD')){
                idx <= 0 && idx++;
                idx++;
                valuePost.rw_doc_no = 'LV'+moment(new Date).format('YYYYMMDD')+'-'+idx;
                // console.log(NumDocPerDay);
            }
            if(NumDocPerDay[0].create_date !== moment(new Date).format('YYYY-MM-DD')){
                valuePost.rw_doc_no = 'LV'+moment(new Date).format('YYYYMMDD')+'-'+ 1
            }
        });
        })();
    }, [])

    const nowDate = new Date();
    const [datePick, setDatePick] = useState();
    const [timeStart, setTimeStart] = useState();
    const [timeEnd, setTimeEnd] = useState();
    const [dateStart, setDateStart] = useState();
    const [dateEnd, setDateEnd] = useState(dateStart);
    const [typeChkLeave, setTypeChkLeave] = useState('0');
    const [chkFileUpload, setChkFileUpload] = useState('0');
    const [timeLeave, setTimeLeave] = useState({ format: '00:00', min: '0' });
    const [dateLeave, setDateLeave] = useState({ countDate: 0 });

    const showDatePicker = (typeDate) => {
        if (typeDate === 'start') {
            toggleStart();
        }
        if (typeDate === 'end') {
            toggleEnd();
        }
    }
    const handlerDatePicker = (date, typeDate) => {

        if (typeDate === 'start') {
            setDateStart(date);
            valuePost.leave_date_from1 = moment(date).format('YYYY-MM-DD');
            if (typeChkLeave === '0' || typeChkLeave === '1') {
                valuePost.leave_date_from2 = moment(date).format('YYYY-MM-DD');
            }
        }
        if (typeDate === 'end') {
            setDateEnd(date);
            valuePost.leave_date_from2 = moment(date).format('YYYY-MM-DD');
        }
        // let total = date.getDate() - dateStart.getDate();
        // let total = date - dateStart;
        if(dateStart){
            let diff = date.getTime( ) - dateStart.getTime( );
            diff = Math.floor(diff / (1000 * 60 * 60 * 24));
            if (diff <= 0) {
                setTimeEnd(undefined);
                setTimeLeave({ countDate:0});
            }
            if (diff > 0) {
                setTimeEnd(undefined);
                setDateLeave({ countDate:diff });
            }
            valuePost.day_leave = diff;
            valuePost.total_leave = 480*diff;
            if(diff > 5){
                valuePost.leave_days1 = 6;
                valuePost.leave_days1Txt = 'กำหนดเอง'
            }else{
                valuePost.leave_days1 = diff;
                valuePost.leave_days1Txt = diff+' วัน'
            }
        }
    }

    const showTimePicker = (typeDate) => {
        if (typeDate === 'start') {
            toggleTimeStart();
        }
        if (typeDate === 'end') {
            toggleTimeEnd();
        }
    }
    const handlerTimePicker = async (date, typeDate) => {
        if (typeDate === 'start') {
            setTimeStart(date);
        }
        if (typeDate === 'end') {
            setTimeEnd(date);
        }
        let total = date.getTime() - timeStart.getTime();
        
        if (total <= 0) {
            setTimeEnd(undefined);
            setTimeLeave({ format: '00:00', min: '0' });
        }
        if (total > 0 && total < 1800000) {
            setTimeEnd(undefined);
            setTimeLeave({ format: '00:00', min: '0' });
        }
        if (total >= 1800000) {
            var minutes = Math.floor(total / 60000);
            var hour = minutes / 60;
            valuePost.total_leave = minutes;
            if (hour >= 1 && moment(total).utc().format('mm') !== '00') {
                setTimeLeave({ format: moment(total).utc().format('HH ชั่วโมง mm นาที'), min: minutes });

            } else if (hour >= 0 && moment(total).utc().format('mm') === '00') {
                setTimeLeave({ format: moment(total).utc().format('HH ชั่วโมง'), min: minutes });
            }
            else {
                setTimeLeave({ format: moment(total).utc().format('mm นาที'), min: minutes });
            }
        }
    }

    const callBackCheckBox = (data) => {
        setTypeChkLeave(data.id);
    }
    const callBackFileUpload = (data) => {
        setChkFileUpload(data.id);
    }
    const chageType = (data) => {
        valuePost.leave_type = data.value;
        valuePost.leave_typeTxt = data.label;
        leaveTypeDetail.forEach(element => {
            if(data.value === element.id){
                setTypeDetail(element);
            }
        });
        
    }
    const _pickDocument = async () => {
        callBackData.params = undefined;
        let response = await DocumentPicker.getDocumentAsync({ type: "*/*" });
        if (response.type == 'success') {
            let { name, size, uri } = response;
            let nameParts = name.split('.');
            let fileType = nameParts[nameParts.length - 1];
            var fileToUpload = {
                name: name,
                size: size,
                uri: uri,
                typename: fileType,
                type: "application/" + fileType
            };
            valuePost.fileUpload = [{ name: valuePost.rw_doc_no, type: fileToUpload.type, uri: fileToUpload.uri }];
        }
    }

    if (callBackData.params) {
        console.log(callBackData);
        valuePost.fileUpload = [{ name: valuePost.rw_doc_no, type: 'image/jpg', uri: callBackData.params.photo.uri }];
    }
    const postRequest = async (data) => {

        if(typeChkLeave === '0'){
            valuePost.check_hour = true 
        }

        if(timeEnd&&timeStart !== ''){
            let timeClockStart = moment(timeStart).format('HH').split('0');
            let timeClockEnd = moment(timeEnd).format('HH').split('0');
            valuePost.leave_time_from1 = timeClockStart[0];
            valuePost.leave_time_from1Txt = moment(timeStart).format('HH:mm');

            valuePost.leave_time_to1 = timeClockEnd[0];
            valuePost.leave_time_to1Txt = moment(timeEnd).format('HH:mm')
        }

        if(valuePost.fileUpload){
            const fileName = await files(valuePost.fileUpload, 'fileUploadRequestLeave', 'PIC');
            valuePost.fileUpload = fileName;
        }
        await postRequestLeave(valuePost);
        setStatusVisibleDone(true)
    }
    const handleOnClickDone = () => {
        setStatusVisibleDone(false)
        navigation.dispatch(
            CommonActions.reset({
                index: 1,
                routes: [{
                    name: 'ListRequestLeave',
                }],
            })
        )
    }
    return (
        <View style={{ flex: 1 }}>
            <ModalAlertLogin logAlert={'ทำเรื่องขอลาสำเร็จ รอการอนุมัติ'} statusVisible={statusVisibleDone} toggle={handleOnClickDone} />
            <ScrollView style={{ flex: 1, backgroundColor: '#FEFEFE' }}>
                <View style={{ flex: 1, padding:20}}>
                        <Text>ชื่อผู้ขอ</Text>
                        <Text style={{ paddingLeft: 20, paddingBottom: 10 }}>{dataUser.name}</Text>
                        <Text>ตำแหน่ง</Text>
                        <Text style={{ paddingLeft: 20, paddingBottom: 10 }}>{detailEmp.cont_lbl_pos}</Text>
                        <DropDownPicker
                            placeholder="ประเภท"
                            items={leaveType}
                            itemStyle={style.itemStyle}
                            dropDownStyle={style.dropDownStyle}
                            style={style.boxDropDownStyle}
                            containerStyle={style.containerStyle}
                            onChangeItem={(item) => chageType(item)}
                        />
                        {showTypeDetail&&
                        <>
                            <Text  style={{ paddingTop:20}}>ข้อกำหนด</Text>
                            <Text style={{ paddingLeft: 20, paddingBottom: 10 }}>{showTypeDetail.leave_deadline}</Text>
                        </>
                        }
                         
                    <Button
                        icon="calendar"
                        mode="outlined"
                        color="#0580FF"
                        style={{ backgroundColor: '#FEFEFE', marginVertical: 10, paddingVertical: 10 }}
                        onPress={() => { showDatePicker('start') }}
                    >
                        {dateStart ? 'วันที่เริ่มลา : ' + moment(dateStart).format('DD MMMM, YYYY') : 'เลือกวันที่ลา'}
                    </Button>
                    {dateStart &&
                        <>
                            <View style={{ flex: 1, paddingLeft: 30, paddingVertical: 20 }}>
                                <CheckListLeave dataUser={dataUser} callBackCheckBox={callBackCheckBox} />
                            </View>


                            {typeChkLeave === '0' ?
                                <View style={{ flex: 1, alignItems: 'center' }}>
                                    <Text>ลาวันที่ {moment(dateStart).format('DD MMMM, YYYY')}</Text>
                                </View>
                                : typeChkLeave === '1' ?
                                    <View style={{ flex: 1, alignItems: 'center' }}>
                                        <Button
                                            icon="clock"
                                            mode="outlined"
                                            color="#0580FF"
                                            // style={style.btnStyle}
                                            style={{ backgroundColor: '#FEFEFE', marginVertical: 10, paddingVertical: 10 }}
                                            onPress={() => { showTimePicker('start') }}
                                        >
                                            {timeStart ? 'เวลาเริ่มต้น : ' + moment(timeStart).format('HH:mm น.') : 'เลือกเวลาเริ่มต้น'}
                                        </Button>
                                        {timeStart &&
                                            <Button
                                                icon="clock"
                                                mode="outlined"
                                                color="#0580FF"
                                                // style={style.btnStyle}
                                                style={{ backgroundColor: '#FEFEFE', marginVertical: 10, paddingVertical: 10 }}
                                                onPress={() => { showTimePicker('end') }}
                                            >
                                                {timeEnd ? 'เวลาสิ้นสุด : ' + moment(timeEnd).format('HH:mm น.') : 'เลือกเวลาสิ้นสุด'}
                                            </Button>

                                        }
                                        {timeLeave.min > 0 &&
                                            <Text>ลา {timeLeave.format} ({timeLeave.min} นาที) ในวันที่ {moment(dateStart).format('DD MMMM, YYYY')} </Text>
                                        }

                                    </View>
                                    : typeChkLeave === '2' &&
                                    <View style={{ flex: 1, alignItems: 'center' }}>
                                    <Button
                                        icon="calendar"
                                        mode="outlined"
                                        color="#0580FF"
                                        // style={style.btnStyle}
                                        style={{ backgroundColor: '#FEFEFE', marginVertical: 10, paddingVertical: 10 }}
                                        onPress={() => { showDatePicker('end') }}
                                    >
                                        {dateEnd ? 'วันที่สิ้นสุด : ' + moment(dateEnd).format('DD MMMM, YYYY') : 'เลือกวันที่สิ้นสุด'}
                                    </Button>
                                    {dateLeave.countDate > 0 &&
                                        <Text>ลา {dateLeave.countDate} วัน ตั้งแต่วันที่ {moment(dateStart).format('DD MMMM, YYYY')} ถึง  {moment(dateEnd).format('DD MMMM, YYYY')}</Text>
                                    }
                                    </View>

                            }
                            <View style={{ flex: 1, flexDirection: 'row', paddingLeft: 30, paddingVertical: 10,}}>
                                <CheckFileUpload dataUser={dataUser} callBackFileUpload={callBackFileUpload} />
                            </View>
                        </>
                    }
                    <DateTimePickerModal
                        value={nowDate}
                        minimumDate={nowDate}
                        // onChange={(event, date) => getDatePicker(date,tempID)}
                        onChange={(event, date) => handlerDatePicker(date, 'start')}
                        show={showStart}
                        toggle={toggleStart}
                    />
                    <DateTimePickerModal
                        value={dateStart}
                        minimumDate={dateStart}
                        // onChange={(event, date) => getDatePicker(date,tempID)}
                        onChange={(event, date) => handlerDatePicker(date, 'end')}
                        show={showEnd}
                        toggle={toggleEnd}
                    />

                    <DateTimePickerModal
                        value={nowDate}
                        minimumDate={nowDate}
                        // onChange={(event, date) => getDatePicker(date,tempID)}
                        onChange={(event, date) => handlerDatePicker(date, 'start')}
                        show={showStart}
                        toggle={toggleStart}
                    />

                    <DateTimePickerModal
                        value={nowDate}
                        mode={'time'}
                        display="spinner"
                        is24Hour={true}
                        minuteInterval={30}
                        onChange={(event, date) => handlerTimePicker(date, 'start')}
                        show={showTimeStart}
                        toggle={toggleTimeStart}
                    />
                    <DateTimePickerModal
                        value={timeStart}
                        mode={'time'}
                        is24Hour={true}
                        display="spinner"
                        minuteInterval={30}
                        onChange={(event, date) => handlerTimePicker(date, 'end')}
                        show={showTimeEnd}
                        toggle={toggleTimeEnd}
                    />

                </View>
               
                {chkFileUpload === '1' &&
                    <View style={{ flex: 1, padding:20}}>
                        <Button
                            // icon="clock"
                            mode="outlined"
                            color="#0580FF"
                            // style={style.btnStyle}
                            style={{ backgroundColor: '#FEFEFE', marginVertical: 10, paddingVertical: 10 }}
                            onPress={_pickDocument}
                        // onPress={() => { showTimePicker('end') }}
                        >
                            อัพโหลดไฟล์
                        </Button>
                    </View>
                }
                {chkFileUpload === '2' &&
                    <View style={{ flex: 1, alignItems: 'center'}}>
                        {callBackData.params ?
                            <View
                                style={{ flex: 1, borderRadius: 10, borderColor: '#d4d4d4', width: '80%' }}
                            >
                                <Image
                                    style={style.checkInImgSize}
                                    source={{ uri: callBackData.params.photo.uri }}
                                />
                                <Button
                                    // icon="clock"
                                    mode="outlined"
                                    color="#0580FF"
                                    // style={style.btnStyle}
                                    style={{ backgroundColor: '#FEFEFE', marginVertical: 10, paddingVertical: 10 }}
                                    onPress={() => navigation.navigate('UploadFile')}
                                // onPress={() => { showTimePicker('end') }}
                                >
                                    ถ่ายรูป
                                </Button>
                            </View>
                            :
                            <TouchableHighlight
                                underlayColor={'#8a8a8a'}
                                onPress={() => { navigation.navigate('UploadFile') }}
                                // style={style.checkInBoxImg}
                                style={{
                                    flex: 1,
                                    borderWidth: 1,
                                    borderRadius: 10,
                                    borderColor: '#d4d4d4',
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    width: '80%'
                                }}
                            >
                                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', height:400 }}>
                                    <MaterialIcons name="add-a-photo" size={50} color="#d4d4d4" />
                                    <Text style={style.txtHeadInBoxCamera}>กรุณา กด</Text>
                                    <Text style={style.txtContentInBoxCamera}>เพื่อเปิดใช้งานกล้องถ่ายรูป</Text>
                                </View>
                            </TouchableHighlight>
                        }
                    </View>
                }
                <View style={{ flex: 1, paddingHorizontal:20}}>
                <TextInput
                    theme={style.styleInput}
                    style={style.inputStyle}
                    // mode="outlined"
                    label="หมายเหตุ*"
                    onChangeText={text => valuePost.leave_remark = text}
                />
                <Button
                    disabled={dateStart&&valuePost.leave_type ?false:true}
                    // icon="save"
                    mode="outlined"
                    color="#0580FF"
                    // style={style.btnStyle}
                    style={{ backgroundColor: '#FEFEFE', marginVertical: 10, paddingVertical: 10 }}
                    onPress={() => { postRequest('UploadFile') }}
                >
                    ยื่นเรื่องขอลา
                    {/* {timeStart ? 'เวลาเริ่มต้น : ' + moment(timeStart).format('HH:mm น.') : 'เลือกเวลาเริ่มต้น'} */}
                </Button>
                </View>
            </ScrollView>
        </View>
    );
}
