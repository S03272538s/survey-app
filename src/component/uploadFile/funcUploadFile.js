// =======================================================================
// Init/State Componant
// =======================================================================

import React, { useState, useEffect, useRef } from 'react';
import { Camera } from 'expo-camera';
import { View, Text, Button, TouchableOpacity } from 'react-native';
import { FontAwesome, MaterialCommunityIcons } from '@expo/vector-icons'; 
import { StyleContain } from '../style/funcStyle';
const style = StyleContain();


// =======================================================================
// Post Data Img Device
// =======================================================================

import {files} from '../../repository/funcPostData'

// =======================================================================
// Render Camera Device
// =======================================================================

export default function CameraRequestLeave({navigation}) {
  const [hasPermission, setHasPermission] = useState(null);
  const [type, setType] = useState(Camera.Constants.Type.front);
  const [onCamera, setOnCamera] = useState(true);
  const ref = useRef();
  let camera;
  useEffect(() => {
    (async () => {

      const { status } = await Camera.requestPermissionsAsync();
      setHasPermission(status === 'granted');
    })();
  }, []);

  if (hasPermission === null) { return <View /> }
  if (hasPermission === false) { return <Text>No access to camera</Text> }

  const SwitchCamera = () => {
    setType(
      type === Camera.Constants.Type.front
        ? Camera.Constants.Type.back
        : Camera.Constants.Type.front
    );
  }
 
  const snap = async () => {
    // let onReady = await camera.onCameraReady();
    if (camera) {
      let photoCallBack = await camera.takePictureAsync();
      navigation.navigate('MenuRequestLeave',{photo:photoCallBack})
    }
  }
  return (
    <View style={{ flex: 1 }}>
      <View style={{ flex: 0.5, backgroundColor: '#000', }}>

      </View>
      <View style={{ flex: 2, backgroundColor: 'red', }}>
        <Camera
          style={style.cameraView}
          type={type}
          ref={ref => { camera = ref; }}
        >
          <View />
        </Camera>
      </View>
      <View style={{ flex: 1, backgroundColor: '#000'}}>
        
        <TouchableOpacity
          style={style.cameraBtnSwitchCamera}
          onPress={() => { SwitchCamera() }}>
            <MaterialCommunityIcons name="camera-retake" size={35} color="#FFFFFF" />
        </TouchableOpacity>

        <TouchableOpacity
          style={style.cameraBtnTakePhoto}
          onPress={() => { snap() }}>
            <FontAwesome name="camera" size={55} color="#FFFFFF" />
          {/* <Text style={style.cameraBtnText}> TEST </Text> */}
        </TouchableOpacity>
      </View>
    </View>
    // 
  );
};