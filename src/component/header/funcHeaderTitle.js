// =======================================================================
// Init/State Componant
// =======================================================================

import React from 'react';
import Logout from '../../login-out/funcLogout';
import { Text, View, Image } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { SwipeRow } from 'react-native-swipe-list-view';
import { StyleContain } from '../style/funcStyle';
const style = StyleContain();

// =======================================================================
// Render Header index
// =======================================================================

export default function Header({ dataUser, handleClickLogout }){
    return (
        <View style={style.viewHeaderStyle}>
            {dataUser !== undefined &&
                <SwipeRow rightOpenValue={-130}>
                    <View style={style.standaloneHeaderRowBack}>
                        <LinearGradient
                            colors={style.LinearColor}
                            style={style.LinearStyle}
                        />
                        <Logout dataUser={dataUser} handleClickLogout={handleClickLogout} />
                    </View>
                    <View style={style.standaloneHeaderRowFront}>
                        <LinearGradient
                            colors={style.LinearColor}
                            style={style.LinearStyle}
                        />
                        <View style={style.viewIconStyle}>
                            <Image
                                style={style.iconStyle}
                                source={{ uri: 'http://1.2.175.220/public/img_default_mobile/icon_user_show.png' }}
                            />
                        </View>
                        <View style={style.headerViewRight}>
                            <Text style={style.txtHeaderStyle}>
                                {dataUser.name}
                            </Text>
                        </View>
                    </View>
                </SwipeRow>
            }
        </View>
    );
};