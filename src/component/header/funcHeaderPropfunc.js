// =======================================================================
// Init/State Componant
// =======================================================================

import React from 'react';
import { Text, View, TouchableHighlight } from 'react-native';
import { StyleContain } from '../style/funcStyle';
import { AntDesign } from '@expo/vector-icons';
const style = StyleContain();

//========================================================================
// NavigatorMenu Componant
// =======================================================================

export default function HeaderPropFunc({ navigation, namePage}) {
  return (
    <View style={{ flex: 0.1, flexDirection: 'row', alignItems: 'center', paddingHorizontal: 10, backgroundColor: '#2653A5', marginTop:30}}>
        <TouchableHighlight
          underlayColor={'#2E64CA'}
          onPress={() => { navigation.goBack()}} 
          style={{ flex: 0.6, alignItems: 'center', padding: 5, borderRadius: 10, borderBottomWidth: 0.5, borderBottomColor: '#ffe774' }}
        >
          <AntDesign name="left" size={24} color="#FAFAFA"/>
        </TouchableHighlight>
        <View style={{ flex: 3, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
          <Text style={style.txtHeaderStyle}>{namePage}</Text>
        </View>
      </View>
  );
}