// =======================================================================
// Init/State Componant
// =======================================================================

import React from 'react';
import Logout from '../../login-out/funcLogout';
import { Text, View, Image, TouchableHighlight } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { SwipeRow } from 'react-native-swipe-list-view';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { StyleContain } from '../style/funcStyle';
const style = StyleContain();

// =======================================================================
// Render Header Child
// =======================================================================

export default function Header({ navigation, dataUser, handleClickLogout }) {
    return (
        <View style={{ flex: 0.1, flexDirection: 'row', alignItems: 'center', paddingHorizontal: 10, backgroundColor: '#2653A5', marginTop: 30 }}>
            <TouchableHighlight
                underlayColor={'#2E64CA'}
                onPress={() => { navigation.toggleDrawer() }}
                style={{ flex: 0.6, alignItems: 'center', padding: 5, borderRadius: 10, borderBottomWidth: 0.5, borderBottomColor: '#ffe774' }}
            >
                <MaterialCommunityIcons name="menu" size={35} color="#FAFAFA" />
            </TouchableHighlight>
            <View style={{ flex: 3, alignItems: 'flex-end', padding: 10, }}>
                {/* <View style={style.viewIconStyle}> */}

                    <TouchableHighlight
                        underlayColor={'#4c669f'}
                        activeOpacity={0.2}
                        onPress={() => { navigation.navigate("Home") }}
                        style={style.viewIconStyle}
                    >
                        <Image
                            style={style.iconStyle}
                            source={{ uri: 'http://1.2.175.220/public/img_default_mobile/icon_user_show.png' }}
                        />
                    </TouchableHighlight>



                {/* </View> */}
            </View>
        </View>
    );
};

// <View style={style.viewHeaderStyle}>
//             {dataUser !== undefined &&
//                     <View style={style.standaloneHeaderRowFront}>
//                         <LinearGradient
//                             colors={style.LinearColor}
//                             style={style.LinearStyle}
//                         />
//                        <View style={{flex:1}}>

//                         <TouchableHighlight
//                             underlayColor={'#2E64CA'}
//                             onPress={() => { navigation.toggleDrawer()}} 
//                             style={{ 
//                                 flex: 0.6, 
//                                 alignItems: 'center', 
//                                 justifyContent:'flex-end', 
//                                 padding: 5 , 
//                                 top:10, 
//                                 borderRadius:10 ,
//                                 borderWidth:1 , 
//                                 // borderBottomWidth:0.5 , 
//                                 // borderBottomColor:'#ffe774'
//                             }}
//                             >
//                             <MaterialCommunityIcons name="menu" size={35} color="#FAFAFA"/>
//                             </TouchableHighlight>
//                                 {/* {/* <MaterialCommunityIcons name="menu" size={35} color="#FAFAFA"  onPress={() => { navigation.toggleDrawer()}} /> */}
//                         </View> 
//                         <View style={style.viewIconStyle}>
//                         <Image
//                                 style={style.iconStyle}
//                                 source={{ uri: 'http://1.2.175.220/public/img_default_mobile/icon_user_show.png' }}
//                             />
//                         </View>
//                     </View>
//             }
//         </View>