// =======================================================================
// Init/State Componant
// =======================================================================

import React, {useState, useEffect} from 'react';
import { View, Text, ScrollView, Animated, Dimensions, ActivityIndicator, TouchableHighlight } from 'react-native';
import moment from 'moment';
import { FontAwesome, MaterialCommunityIcons, MaterialIcons } from '@expo/vector-icons';
import { StyleContain } from '../style/funcStyle';
const style = StyleContain();
const { width } = Dimensions.get('window');

// =======================================================================
// Render Home Page
// =======================================================================

export default function FuncLeave({navigation, dataDetail, typeTab}) {
    console.log('typeTab');
    console.log(typeTab);
    return typeTab === '1'?(
        <View>
        <View style={style.viewFeedImgContent}>
            <Text>แสดงข้อมูล สุขภาพการทำงานพนักงาน</Text>
        </View>
       
    </View>
       
    ):
    typeTab === '2'&&(
        <ScrollView
        onScroll={(event) => {
            const scrolling = event.nativeEvent.contentOffset.y;
            console.log(scrolling);
            if (scrolling > 80) {
                setHeaderShown(true);
            } else {
                setHeaderShown(false);
            }
        }}
        // onScroll will be fired every 16ms
        scrollEventThrottle={16}
        style={{ flex: 1,}}
    >
    <View style={{ flex: 1}}>
    {dataDetail.map((feed, idx) => (
            <TouchableHighlight
                key={idx}
                activeOpacity={0.6}
                style={style.touchFeed}
                underlayColor={'#8a8a8a'}
                // onPress={() => { navigation.navigate('FeedDetail', { dataFeed: feed }) }}
            >
                <View>
                    <View style={style.viewFeedImgContent}>
                        <Text>{feed.picture_name}</Text>
                    </View>
                    <View style={style.viewFeedTitleContent}>
                        <Text style={style.txtFeedTitle}>งานที่ต้องทำต่อเดือน {feed.title_name}</Text>
                    </View>
                    <View style={style.viewFeedIntrolContent}>
                        <Text style={style.txtFeedIntrol}>บทนำ {feed.introduction}</Text>
                    </View>
                    <View style={style.viewFeedMore}>
                        <Text style={style.txtFeedIntrol}>เพิ่มเติม...</Text>
                    </View>
                </View>
            </TouchableHighlight>
        ))
        } 
    </View>
    </ScrollView>
    )
}

