// =======================================================================
// Init/State Componant
// =======================================================================

import React, {useState, useEffect} from 'react';
import { View, Text, ScrollView, Animated, Dimensions, ActivityIndicator } from 'react-native';
import moment from 'moment';
import { FontAwesome, MaterialCommunityIcons, MaterialIcons } from '@expo/vector-icons';
import FuncLeave from './funcLeave';
import { StyleContain } from '../style/funcStyle';
const style = StyleContain();
const { width } = Dimensions.get('window');

import HeaderMenu from '../header/funcHeaderMenu';
// =======================================================================
// Repository Data
// =======================================================================

import { getData, getAccessRight, getPosition, getLeave, getLeaveType } from '../../repository/funcGetData';

// =======================================================================
// Navigations
// =======================================================================
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
const topTab = createMaterialTopTabNavigator();

// =======================================================================
// Render Home Page
// =======================================================================

export default function NavigationLeave({ navigation, dataUser }) {
    console.log(dataUser);
    const [leave, setLeave] = useState([{totalLeave:'asdasdasdasdasd'}]);
    const [healt, setHealt] = useState(
        [
            {title:'สุขภาพการทำงาน',id:'1'},
            {title:'ตัวชี้วัด',id:'2'},
        ]
        );
    const [leaveType, setLeaveType] = useState();
    let arrDeatil = [];
    useEffect(() => {
        (async () => {
            setLeave( await getLeave(dataUser, 'getLeave', ''));
            setLeaveType( await getLeaveType('getLeaveType', ''));
        })();
    },[])
    const [feedNews, setFeedNews] = useState(
        [{ picture_name:'งานที่  1' },{ picture_name:'งานที่  2' },{ picture_name:'งานที่  3' }]
        )
    return (
        <View style={style.viewFlex}>
            <View style={style.viewFlex}>
                <HeaderMenu navigation={navigation} namePage={'ตัวชี้วัด'} />
                {leaveType?
                <topTab.Navigator
                    initialRouteName={moment(new Date).format('dddd')}
                    tabBarOptions={{
                        labelStyle: { fontSize: 15, fontWeight: 'bold' },
                        tabStyle: { width: width / 2 },
                        activeTintColor: '#ffd506',
                        inactiveTintColor: '#FFF9DB',
                        indicatorStyle: { backgroundColor: '#ffe774' },
                        style: { backgroundColor: '#3865b7', borderTopWidth: 1, borderTopColor: '#ffe774' },
                        scrollEnabled: true,
                        pressColor: '#ffe774'
                    }}
                >
                     {healt.map((ele, idx) => ( 
                        <topTab.Screen
                            key={idx}
                            name={ele.title}
                            // name='MenuLeave'
                            options={{
                                 tabBarLabel: ele.title,
                                //  tabBarLabel: 'วันลา',
                            }} >
                            {/* {(props) => jobTimeline ? */}
                            {(props) =>
                                <FuncLeave navigation={props.navigation} dataDetail={feedNews} typeTab={ele.id} />
                                // :
                                // <View style={{ flex: 1, backgroundColor: '#FEFEFE', alignItems: 'center', justifyContent: 'center' }}>
                                //     <Text style={{ color: '#292929', fontWeight: 'bold', fontSize: 16, paddingBottom: 20 }}> กำลังดึงข้อมูล... </Text>
                                //     <ActivityIndicator size={70} color={'#0580FF'} />
                                // </View>
                            }
                        </topTab.Screen>
                    ))} 
                </topTab.Navigator>
            : 
            <View style={{ flex: 1, backgroundColor: '#FEFEFE', alignItems: 'center', justifyContent: 'center' }}>
                 <Text style={{ color: '#292929', fontWeight: 'bold', fontSize: 16, paddingBottom: 20 }}> กำลังดึงข้อมูล... </Text>
             <ActivityIndicator size={70} color={'#0580FF'} />
             </View>
            }
            </View>
        </View>
    )
};