// =======================================================================
// Init/State Componant
// =======================================================================

import React, { useState, useEffect } from 'react';
import { View, ScrollView } from 'react-native';
import ListShotMenu from './funcListShotMenu';
import { FontAwesome5, MaterialCommunityIcons, MaterialIcons } from '@expo/vector-icons';
const iconStyle = { size: 24, color: '#2653a5' };



import { getAccessRight, getAccessRightDev, getAccessJobWeek } from '../../repository/funcGetData';
// =======================================================================
// Render Home Page
// =======================================================================

export default function ViewShotMenu({ navigation, dataUser }) {
    const [access, setAccess] = useState(true)
    const [accessWeekJob, setAccessWeekJob] = useState(true)
    const [accessDev, setAccessDev] = useState(true)
    useEffect(() => {
        (async () => {
            setAccess(await getAccessRight(dataUser, 'getAccessRights', ''));
            setAccessDev(await getAccessRightDev(dataUser, 'getAccessRights', ''));
            setAccessWeekJob(await getAccessJobWeek(dataUser, 'getJobTimeline', ''));

        })();
    }, [])
    return (
        <View style={{ flex: 1 }}>
            
            <View style={{ flex: 1, flexDirection: 'row',  }}>
                <ScrollView 
                horizontal
                showsHorizontalScrollIndicator={false}
                style={{paddingVertical:10}}
                >
                 <ListShotMenu
                    navigation={navigation}
                    navigationName={'MenuProfile'}
                    dataUser={dataUser}
                    txtBtn={'ข้อมูลพนักงาน'}
                    IconBtn={<MaterialCommunityIcons name="account" size={iconStyle.size} color={iconStyle.color} />}
                />
                <ListShotMenu
                    navigation={navigation}
                    navigationName={'CheckIn'}
                    dataUser={undefined}
                    txtBtn={'เข้า/ออก งาน'}
                    IconBtn={<MaterialCommunityIcons name="map-marker-check" size={iconStyle.size} color={iconStyle.color} />}
                />
                <ListShotMenu
                    navigation={navigation}
                    navigationName={'NavigationLeave'}
                    dataUser={undefined}
                    txtBtn={'ตัวชี้วัด'}
                    IconBtn={<MaterialCommunityIcons name="speedometer-medium" size={iconStyle.size} color={iconStyle.color} />}
                />
                 {accessWeekJob === true&&
                <ListShotMenu
                    navigation={navigation}
                    navigationName={'MenuTimeline'}
                    dataUser={undefined}
                    txtBtn={'ตารางงาน'}
                    IconBtn={<MaterialCommunityIcons name="timetable" size={iconStyle.size} color={iconStyle.color} />}
                />
                }
                {access === false&&
                
                <ListShotMenu
                    navigation={navigation}
                    navigationName={'Survey'}
                    dataUser={undefined}
                    txtBtn={'สำรวจร้าน'}
                    IconBtn={<MaterialCommunityIcons name="walk" size={iconStyle.size} color={iconStyle.color} />}
                />
                }
                {access === false&&
                
                <ListShotMenu
                    navigation={navigation}
                    navigationName={'Home'}
                    dataUser={undefined}
                    txtBtn={'สำรวจพื้นที่'}
                    IconBtn={<FontAwesome5 name="search-location" size={iconStyle.size} color={iconStyle.color} />}
                />
                }
                </ScrollView>
            </View>
            {/* <View style={{ flex: 0.5, flexDirection: 'row', padding: 10 }}>
               
                
            </View> */}
           
        </View>
    );
};