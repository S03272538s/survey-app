// =======================================================================
// Init/State Componant
// =======================================================================

import React from 'react';
import { Text, View, ScrollView, Image } from 'react-native';
import HeaderMenu from '../header/funcHeaderMenu';
import { StyleContain } from '../style/funcStyle';
import moment from 'moment';
const style = StyleContain();

// =======================================================================
// Repository Data
// =======================================================================

// =======================================================================
// NavigatorMenu Componant
// =======================================================================

export default function ViewFeedDetail({navigation, param }){
    const feed = param.dataFeed;
    console.log(feed);
    return (
        <View style={{ flex: 1 }}>
            <HeaderMenu navigation={navigation} namePage={'หัวเรื่อง : ' + feed.title_name} />
            <ScrollView style={{ flex: 1 }}>
                <View style={{ flex: 1, alignItems: 'center', borderWidth: 1 }}>
                    <Text>รูป {feed.picture_name}</Text>
                    <Image
                        style={style.logoLogin}
                        source={{ uri: 'http://1.2.175.220/public/img_default_mobile/logo-login.png' }}
                    />
                </View>
                <View style={{ paddingLeft: 10, paddingTop: 10 }}>
                    <Text style={{ fontSize: 16 }}>หัวเรื่อง :  {feed.title_name}</Text>
                </View>
                <View style={{ padding: 5 , marginBottom:20}}>
                    <Text>บทนำ : {feed.introduction}</Text>
                </View>
                <View style={{  paddingLeft: 10 }}>
                    <Text>เนื้อหาข่าวสาร : {feed.content}</Text>
                </View>
            </ScrollView>
        </View>
    );
}