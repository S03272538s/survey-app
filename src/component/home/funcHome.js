// =======================================================================
// Init/State Componant
// =======================================================================

import React, { useState, useEffect, useRef } from 'react';
import { View, Text, ScrollView, Animated } from 'react-native';
import { Button } from 'react-native-paper';
import { FontAwesome, MaterialCommunityIcons, MaterialIcons } from '@expo/vector-icons';
import ViewFeedNews from './viewFeedNews';
import ViewEmtyFeed from './viewEmtyFeed';
import ViewShotMenu from './viewShotMenu';
import { StyleContain } from '../style/funcStyle';
const style = StyleContain();
import VersionInfo from 'react-native-version-info';

// =======================================================================
// Repository Data
// =======================================================================

import { getData, getAccessRight, getPosition } from '../../repository/funcGetData';

// =======================================================================
// Render Home Page
// =======================================================================

export default function Home({ navigation, currentPosition, dataUser, handleClickLogout }) {
  const [feedNews, setFeedNews] = useState(
      // [{ picture_name:'เทส Feed 1' },{ picture_name:'เทส Feed 2' },{ picture_name:'เทส Feed 3' }]
      )
  useEffect(() => {
      (async () => {
        // setFeedNews(await getData('getFeedNews',''));
      })();
  }, [])
  return(
      <View style={style.viewFeedContent}>
          <View style={{ flex:1,  borderWidth:1 }}>
              {feedNews ?
                  <ViewFeedNews navigation={navigation} dataFeed={feedNews} dataUser={dataUser} />
                  :
                  <ViewEmtyFeed navigation={navigation} dataUser={dataUser}/>
              }
          </View>
      </View>
  )
};
