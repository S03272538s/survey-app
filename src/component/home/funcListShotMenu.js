// =======================================================================
// Init/State Componant
// =======================================================================

import React, { useState, useEffect } from 'react';
import { View, Text, Image, ScrollView, TouchableHighlight, Dimensions } from 'react-native';
import { FontAwesome, MaterialCommunityIcons, MaterialIcons } from '@expo/vector-icons';
import { StyleContain } from '../style/funcStyle';
const style = StyleContain();
const iconStyle = { size: 24, color: '#292929' };
// =======================================================================
// Render Home Page
// =======================================================================

export default function ListShotMenu({ navigation, navigationName, dataUser, txtBtn, IconBtn }) {

    return (
        <View style={{ flex: 1, alignItems: 'center',}}>
            <TouchableHighlight
            activeOpacity={0.6}
            style={{ flex: 1, alignItems: 'center', justifyContent: 'center' ,marginHorizontal:20, borderWidth: 1, borderRadius: 50, height:60, width:60 ,borderColor:'#4c669f',backgroundColor:'#FEFEFE'}}
            underlayColor={'#617ab3'}
            onPress={() => { dataUser !== undefined ? navigation.navigate(navigationName, { dataUser }) : navigation.navigate(navigationName) }}
        >
            {/* <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' ,marginHorizontal:20, borderWidth: 1, borderRadius: 50, height:60, width:60 ,borderColor:'#4c669f',backgroundColor:'#FEFEFE'}}> */}
            {IconBtn}
            {/* </View> */}
        </TouchableHighlight>
            <Text style={{textAlign:'center',color:'#2653a5'}}>{txtBtn}</Text>
        </View> 
    );
};
