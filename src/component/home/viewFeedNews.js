// =======================================================================
// Init/State Componant
// =======================================================================

import React, { useState, useEffect, useRef } from 'react';
import { View, Text, Image, ScrollView, TouchableHighlight, Animated } from 'react-native';
import ViewShotMenu from './viewShotMenu';

import { StyleContain } from '../style/funcStyle';
const style = StyleContain();
// =======================================================================
// Render Home Page
// =======================================================================

export default function ViewFeedNews({ navigation, dataFeed, dataUser }) {
    const [headerShown, setHeaderShown] = useState(false);
    const translation = useRef(new Animated.Value(-100)).current;
    useEffect(() => {
        Animated.timing(translation, {
            toValue: headerShown ? -210 : 0,
            duration: 200,
            useNativeDriver: true,
        }).start();
    }, [headerShown]);
    return (
        <>
             <Animated.View
                style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    right: 0,
                    height: 110,
                    zIndex:1,
                    transform: [
                        { translateY: translation },
                    ],
                }}
            >
                <View style={{ flex: 1, paddingVertical: 5, backgroundColor: '#c6d5f1',}}>
                     <ViewShotMenu navigation={navigation} dataUser={dataUser} />
                 </View>
            </Animated.View>
            
            <ScrollView
                onScroll={(event) => {
                    const scrolling = event.nativeEvent.contentOffset.y;
                    console.log(scrolling);
                    if (scrolling > 80) {
                        setHeaderShown(true);
                    } else {
                        setHeaderShown(false);
                    }
                }}
                // onScroll will be fired every 16ms
                scrollEventThrottle={16}
                style={{ flex: 1,}}
            >
            <View style={{ flex: 1, paddingTop: 110, }}>
            {dataFeed.map((feed, idx) => (
                    <TouchableHighlight
                        key={idx}
                        activeOpacity={0.6}
                        style={style.touchFeed}
                        underlayColor={'#8a8a8a'}
                        onPress={() => { navigation.navigate('FeedDetail', { dataFeed: feed }) }}
                    >
                        <View>
                            <View style={style.viewFeedImgContent}>
                                <Text>รูป : {feed.picture_name}</Text>
                                <Image
                                    style={style.logoLogin}
                                    source={{ uri: 'http://1.2.175.220/public/img_default_mobile/logo-login.png' }}
                                />
                            </View>
                            <View style={style.viewFeedTitleContent}>
                                <Text style={style.txtFeedTitle}>หัวเรื่อง {feed.title_name}</Text>
                            </View>
                            <View style={style.viewFeedIntrolContent}>
                                <Text style={style.txtFeedIntrol}>บทนำ {feed.introduction}</Text>
                            </View>
                            <View style={style.viewFeedMore}>
                                <Text style={style.txtFeedIntrol}>เพิ่มเติม...</Text>
                            </View>
                        </View>
                    </TouchableHighlight>
                ))
                } 
            </View>
            </ScrollView>
        </>
    );
    // const [headerShown, setHeaderShown] = useState(false);
    // const translation = useRef(new Animated.Value(-100)).current;
    // useEffect(() => {
    //     Animated.timing(translation, {
    //         toValue: headerShown ? 0 : -100,
    //         duration: 250,
    //         useNativeDriver: true,
    //     }).start();
    // }, [headerShown]);
    // return (
    //     <View style={style.viewFeedContent}>
    //         <ScrollView
    //             onScroll={(event) => {
    //                 const scrolling = event.nativeEvent.contentOffset.y;

    //                 if (scrolling > 100) {
    //                     setHeaderShown(true);
    //                 } else {
    //                     setHeaderShown(false);
    //                 }
    //             }}
    //             // onScroll will be fired every 16ms
    //             scrollEventThrottle={16}
    //             style={{ flex: 1 }}
    //         >

    //             <View style={{ flex: 0.4, paddingVertical: 10, backgroundColor: '#c6d5f1' }}>
    //                 <ViewShotMenu navigation={navigation} dataUser={dataUser} />
    //             </View>

    //             {dataFeed.map((feed, idx) => (
    //                 <TouchableHighlight
    //                     key={idx}
    //                     activeOpacity={0.6}
    //                     style={style.touchFeed}
    //                     underlayColor={'#8a8a8a'}
    //                     onPress={() => { navigation.navigate('FeedDetail', { dataFeed: feed }) }}
    //                 >
    //                     <View>
    //                         <View style={style.viewFeedImgContent}>
    //                             <Text>รูป : {feed.picture_name}</Text>
    //                             <Image
    //                                 style={style.logoLogin}
    //                                 source={{ uri: 'http://1.2.175.220/public/img_default_mobile/logo-login.png' }}
    //                             />
    //                         </View>
    //                         <View style={style.viewFeedTitleContent}>
    //                             <Text style={style.txtFeedTitle}>หัวเรื่อง {feed.title_name}</Text>
    //                         </View>
    //                         <View style={style.viewFeedIntrolContent}>
    //                             <Text style={style.txtFeedIntrol}>บทนำ {feed.introduction}</Text>
    //                         </View>
    //                         <View style={style.viewFeedMore}>
    //                             <Text style={style.txtFeedIntrol}>เพิ่มเติม...</Text>
    //                         </View>
    //                     </View>
    //                 </TouchableHighlight>
    //             ))
    //             }
    //         </ScrollView>
    //     </View>
    // );
};
