// =======================================================================
// Init/State Componant
// =======================================================================

import React from 'react';
import { View, Text} from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import { StyleContain } from '../style/funcStyle';
import ViewShotMenu from './viewShotMenu';

const style = StyleContain();
const IconStyle={ size:80, color:"#d4d4d4"}
// =======================================================================
// Render Home Page
// =======================================================================

export default function ViewEmtyFeed({navigation, dataUser}) {
    return (
        <>
        <View style={{ flex: 0.16, paddingVertical: 10, backgroundColor: '#c6d5f1' }}>
            <ViewShotMenu navigation={navigation} dataUser={dataUser} />
        </View>
        <View style={style.viewFeedEmtyContent}>
            <FontAwesome name="newspaper-o" size={IconStyle.size} color={IconStyle.color} />
            <Text style={style.txtFeedEmty}>ขณะนี้</Text>
            <Text style={style.txtFeedEmty}>ไม่มีข้อมูลข่าวสาร</Text>
        </View>
        </>
    );
};
