// =======================================================================
// Init/State Componant
// =======================================================================

import React, { useState, useEffect } from 'react';
import { Button } from 'react-native-paper';
import ChkBoxList from './funcCheckLists';
import { MaterialIcons } from '@expo/vector-icons';
import ModalAlertLogin from '../../login-out/funcModalAlertLogin';
import { View, Text, Image, ScrollView, TouchableHighlight } from 'react-native';
import { chkLocationsCheckIn } from './funcCheckIn';
import { StyleContain } from '../style/funcStyle';
const style = StyleContain();

// =======================================================================
// Repository Data
// =======================================================================

import { getCurrentPosition } from '../../repository/funcCurrentPosition';
import { postAttendData } from '../../repository/funcPostData';
import * as SQLite from 'expo-sqlite';
let db = SQLite.openDatabase('Survey.db');
import { addSessionChekIn, updateSessionChekIn } from '../../repository/funcSQLite';

// =======================================================================
// Navigations
// =======================================================================

import { CommonActions } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
const Stack = createStackNavigator();

// =======================================================================
// CheckInScreen Page Navigation
// =======================================================================

export default function CheckInPage({ navigation, callBackData, currentPosition, dataUser, handleClickLogout, chkStatusChkIn, statusCheck, txtBtn, resetChkBtn }) {
    const [nearLocations, setNearLocations] = useState()
    const [chkData, setChkData] = useState({ show: true })
    const [statusVisible, setStatusVisible] = useState(false);
    const [statusVisibleDone, setStatusVisibleDone] = useState(false);
    const [data, setData] = useState();

    const [dataChkIn, setDataChkIn] = useState({
        id_application: dataUser.id_application,
        name: dataUser.name,
        imgChkIn: '',
        statusCheck: statusCheck
    });

    // Get Data On Base Device
    const fetchData = () => {
        db.transaction(tx => {
            tx.executeSql('SELECT * FROM DataChkIn', null,
                (tx, { rows: { _array } }) => {
                    setData(_array[0]);
                }, (tx, error) => console.warn('Error :: ', error)
            )
        })
    }
    useEffect(() => {
        fetchData();
        CallBackChkIn(currentPosition);
    }, []);

    if (callBackData.params) {
        dataChkIn.imgChkIn = [{ name: dataUser.name + '||chekIn||' + new Date, type: 'image/jpg', uri: callBackData.params.photo.uri }];
    }
    const handleOnClickError = () => { setStatusVisible(false) }
    const handleOnClickDone = () => {
        setStatusVisibleDone(false)
        navigation.dispatch(
            CommonActions.reset({
                index: 1,
                routes: [{
                    name: 'Home',
                }],
            })
        )
    }

    const CallBackChkIn = async () => {
        const callBackNearLocations = await chkLocationsCheckIn(currentPosition);
        // console.log(callBackNearLocations);
        setDataChkIn({ currentPosition: currentPosition, ...dataChkIn })
        setNearLocations(callBackNearLocations);
    }

    const getPosition = async () => {
        const callBackCurrentPosition = await getCurrentPosition();
        setDataChkIn({ currentPosition: callBackCurrentPosition, ...dataChkIn })

        const callBackNearLocations = await chkLocationsCheckIn(callBackCurrentPosition);
        setNearLocations(callBackNearLocations);
    }

    const CallBackChkBox = async (select) => {
        setDataChkIn({ cmpName: select.cmpName, cmpId: select.id, ...dataChkIn })
    }
    const chkAllData = () => {
        // console.log(value);
    }
    console.log(callBackData.params);
    console.log(dataChkIn);
    if (callBackData.params && dataChkIn.cmpName) {
        
    console.log(callBackData.params);
    console.log(dataChkIn.cmpId);
        chkData.show = false
    }

    
    
    const postData = async () => {
        
        if (callBackData.params && dataChkIn.cmpId) {
            if (data) {
                data.id_application = dataUser.id_application
                data.name = dataUser.name
                if (JSON.parse(data.statusChekIn) == true) {
                    data.statusChekIn = false;
                    updateSessionChekIn(data)
                } else {
                    data.statusChekIn = true;
                    updateSessionChekIn(data)
                }
            } else {
                addSessionChekIn(dataChkIn)
            }
            postAttendData(dataChkIn, statusCheck);
            setStatusVisibleDone(true);
            chkStatusChkIn(data);
        } else {
            setStatusVisible(true);
        }
    }

    return (
        <>
            <ScrollView style={style.ScrollViewStyle}>
                <View style={style.navigationsCheckInView}>
                    <View style={style.CheckInHeader}>
                        <Text style={style.checkInHeaderTxt}>กรุณาเลือก สถานที่ ที่ต้องการลงชื่อ{txtBtn}</Text>
                    </View>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <TouchableHighlight
                            underlayColor={'#ffd506'}
                            activeOpacity={0.5}

                            onPress={() => { getPosition() }}
                            style={style.touchReset}
                        >
                            <View style={{ paddingTop: 10, paddingBottom: 10 }}>
                                <Text>รีเซ็ตตำแหน่งปัจจุบัน</Text>
                            </View>
                        </TouchableHighlight>
                    </View>
                    {nearLocations !== undefined ?
                        <ChkBoxList nearLocations={nearLocations} CallBackChkBox={CallBackChkBox} />
                        :
                        <View style={{ alignItems: 'center', paddingTop: 20, paddingBottom: 20 }}>
                            <MaterialIcons name="location-off" size={40} color="#ff4a4a" style={{ paddingBottom: 20 }} />

                            <Text style={{ color: '#ff4a4a' }}>ไม่พบสถานที่ใกล้เคียงในระยะ 100 เมตร</Text>
                            <Text style={{ color: '#ff4a4a' }}>กรุณา รีเซ็ตตำแหน่งปัจจุบัน</Text>
                        </View>
                    }

                    <View style={{ height: 400, alignItems: 'center', marginBottom: 20 }}>
                        {callBackData.params ?
                            <View style={style.checkInBoxImg}>
                                <Image
                                    style={style.checkInImgSize}
                                    source={{ uri: callBackData.params.photo.uri }}
                                />
                            </View>
                            :
                            <TouchableHighlight
                                underlayColor={'#8a8a8a'}
                                onPress={() => { navigation.navigate('CameraCheckInPage') }}
                                style={style.checkInBoxImg}
                            >
                                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                                    <MaterialIcons name="add-a-photo" size={50} color="#d4d4d4" />
                                    <Text style={style.txtHeadInBoxCamera}>กรุณา กด</Text>
                                    <Text style={style.txtContentInBoxCamera}>เพื่อเปิดใช้งานกล้องถ่ายรูป</Text>
                                </View>
                            </TouchableHighlight>
                        }
                    </View>
                    <Button
                        disabled={chkData.show}
                        // icon="content-save"
                        color="#4c669f"
                        mode="contained"
                        // onPress={() => { navigation.navigate('CameraCheckInPage') }}
                        onPress={() => { postData() }}
                        style={style.btnStyle}
                    >
                        <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: 16 }}>{txtBtn}</Text>
                    </Button>

                </View>

                <ModalAlertLogin logAlert={'ข้อมูลไม่ครบถ้วน'} statusVisible={statusVisible} toggle={handleOnClickError} />
                <ModalAlertLogin logAlert={'ลงชื่อสำเร็จ'} statusVisible={statusVisibleDone} toggle={handleOnClickDone} />


            </ScrollView>
        </>
    );
}