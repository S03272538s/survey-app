// =======================================================================
// Init/State Componant
// =======================================================================

import React, { useState, useEffect } from 'react';
import { ScrollView, Text, View } from 'react-native';
import { DataTable } from 'react-native-paper';
import { Ionicons } from '@expo/vector-icons';
import moment from 'moment';
import { StyleContain } from '../style/funcStyle';
const style = StyleContain();

// =======================================================================
// Repository Data
// =======================================================================

import { getData } from '../../repository/funcGetData';

// =======================================================================
// Navigations
// =======================================================================

import { createStackNavigator } from '@react-navigation/stack';
const Stack = createStackNavigator();

// =======================================================================
// ShowDetailDataChkInScreen Page Navigation
// =======================================================================

export default function DetailChkInPage({ navigation, dataUser, dataprop }) {
    const [dataShow, setDataShow] = useState()
console.log(navigation);
    const getAllDataChkOnDate = async () => {
        const getDataChkIn = await getData('getDataChkIn', dataUser.id_application);
        const getDataChkOut = await getData('getDataChkOut', dataUser.id_application);
        let arrChkIn = [], arrChkOut = [], arrGroup = [];
        getDataChkIn.forEach((dataChekIn, idx) => {
            if (dataChekIn.create_date === dataprop.dataRow.date) {
                arrChkIn.push({
                    cmpName: dataChekIn.cmpName,
                    chkIn: moment(dataChekIn.dateTime).format('HH:mm')
                });
            }
        });
        getDataChkOut.forEach((dataChkOut, idx) => {
            if (dataChkOut.create_date === dataprop.dataRow.date) {
                arrChkOut.push({
                    cmpName: dataChkOut.cmpName,
                    chkOut: moment(dataChkOut.dateTime).format('HH:mm')
                });
            }
        });
        arrChkIn.forEach((data, idx) => {
            arrGroup.push({
                chkInCmp: data.cmpName,
                chkOutCmp: arrChkOut[idx] ? arrChkOut[idx].cmpName : 'ไม่มีข้อมูล',
                chkIn: data.chkIn,
                chkOut: arrChkOut[idx] ? arrChkOut[idx].chkOut : 'ไม่มีข้อมูล'
            });
        });
        setDataShow(arrGroup);
    }

    useEffect(() => {
        getAllDataChkOnDate();
    }, [])

    return (
        <ScrollView style={style.ScrollViewStyle}>
            <View style={style.CheckInHeader}>
                <View style={{flex:0.1}}>
                    <Ionicons 
                    name="chevron-back-outline" 
                    size={24} 
                    color="black" 
                    onPress={() => { navigation.goBack('DetailDataChkIn')}}

                    />
                </View>
                <View style={{flex:1}}>
                    <Text style={style.checkInHeaderTxt}>ข้อมูลการ เข้า/ออกงาน วันที่ {dataprop.dataRow.date}</Text>
                </View>
            </View>
            {dataShow ?
                <DataTable style={style.viewFlex}>
                    <DataTable.Header
                        style={{ height: 60, fontWeight: 'bold' }}
                    >
                        {/* <DataTable.Title >วันที่</DataTable.Title> */}
                        <DataTable.Title style={{ fontWeight: 'bold' }}>เข้างาน </DataTable.Title>
                        <DataTable.Title style={{ fontWeight: 'bold' }}>ออกงาน </DataTable.Title>
                        <DataTable.Title style={{ fontWeight: 'bold' }}>เข้างานที่ </DataTable.Title>

                        {/* <DataTable.Title style={{ fontWeight: 'bold' }}>สถานที่ออกงาน </DataTable.Title> */}

                    </DataTable.Header>

                    {dataShow.map((data, index) => (
                        <DataTable.Row
                            style={{ borderBottomWidth: 1, borderBottomColor: '#4c669f', height: 60 }}
                            key={index}
                        // onPress={() => { onHandlerTouchRow(data) }}
                        // onLongPress={() => { console.log('Hold Row'); }}
                        >

                            <DataTable.Cell >
                                {data.chkIn + 'น.'}
                            </DataTable.Cell>
                            <DataTable.Cell >
                                {data.chkOut + 'น.'}
                            </DataTable.Cell>
                            <DataTable.Cell >
                                {data.chkInCmp}
                            </DataTable.Cell>
                            {/* <DataTable.Cell numeric>{data.data_phone}</DataTable.Cell>  */}
                        </DataTable.Row>

                    ))}

                </DataTable>
                :
                <Text>Null Data</Text>
            }
        </ScrollView>


    );
}