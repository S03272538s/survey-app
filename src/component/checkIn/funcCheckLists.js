// =======================================================================
// Init/State Componant
// =======================================================================

import React, { useState } from 'react';
import CheckBox from '@react-native-community/checkbox';
import { View, Text, TouchableHighlight } from 'react-native';
import { StyleContain } from '../style/funcStyle';
const style = StyleContain();

// =======================================================================
// CheckList Screen
// =======================================================================

export default function CheckListScreen({ nearLocations, CallBackChkBox}) {
    const [toggleCheckBox, setToggleCheckBox] = useState(nearLocations);
    const handleCheckList = (newValue, index) => {
        CallBackChkBox(toggleCheckBox[index]);
        nearLocations.forEach((element, idx)=>{
            if (idx !== toggleCheckBox[index].id) {
                toggleCheckBox[idx].checkStatus = false;
            }
            toggleCheckBox[index].checkStatus = newValue;
        });
        setToggleCheckBox({ ...toggleCheckBox });
    }
    return (
        nearLocations.map((data, index) => (
            <TouchableHighlight
                key={index}
                activeOpacity={0.6}
                underlayColor="#DDDDDD"
                onPress={() => handleCheckList(true, index)}
            >
                <>
                    {data.distance >= 50 ?
                        <Text style={style.checkListTxtAlpha}> อีก {data.distance} เมตร จะถึง</Text>
                        :
                        <Text style={style.checkListTxtAlpha}> คุณอยู่ที่ </Text>
                    }
                    <View key={index} style={style.checkListContent}>
                        <CheckBox
                            key={index}
                            disabled={false}
                            // boxType={'circle'}
                            value={toggleCheckBox[index].checkStatus}
                            onValueChange={(newValue) => handleCheckList(newValue, index)}
                        />
                        <Text>{data.cmpName}({data.cmpAbbreviation})</Text>
                    </View>
                </>
            </TouchableHighlight>
        ))
    );
}