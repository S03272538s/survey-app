// =======================================================================
// Init/State Componant
// =======================================================================

import React, { useState, useEffect } from 'react';
import CheckInPage from './viewCheckIn';
import CheckInDataPage from './funcChekinData';
import { View } from 'react-native';
import moment from 'moment';
import { StyleContain } from '../style/funcStyle';
const style = StyleContain();

// =======================================================================
// Navigations
// =======================================================================

import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { createStackNavigator } from '@react-navigation/stack';
const Tab = createMaterialTopTabNavigator();
const Stack = createStackNavigator();

// =======================================================================
// Repository Data
// =======================================================================

import * as SQLite from 'expo-sqlite';
const db = SQLite.openDatabase('Survey.db');
import { addSessionChekIn, updateSessionChekIn} from '../../repository/funcSQLite';

// =======================================================================
// CheckInScreen Page Navigation
// =======================================================================

function CheckInScreen({ navigation, callBackData, currentPosition, dataUser, handleClickLogout }) {
    const [statusChkIn, setStatusChkIn] = useState(false)
    const chkStatusChkIn = (data) => {
        setStatusChkIn(data.statusChekIn, { ...statusChkIn });
        callBackData.params = undefined; 
    }
    const [dataOnDevice, setDataOnDevice] = useState();

    // Get Data On Base Device
    const fetchData = () => {
        db.transaction(tx => {
            tx.executeSql('SELECT * FROM DataChkIn', null,
                (tx, { rows: { _array } }) => {
                    setDataOnDevice(_array[0]);
                    if(_array[0] !== undefined){
                        if(_array[0].date !== moment(new Date).format('YYYY-MM-DD')){
                            _array[0].statusChekIn = false;
                            _array[0].date = moment(new Date).format('YYYY-MM-DD');
                            updateSessionChekIn(_array[0]);
                        }
                        setStatusChkIn(JSON.parse(_array[0].statusChekIn))
                    }
                }, (tx, error) => console.warn('Error :: ', error)
            )
        })
    }

    useEffect(() => {
        fetchData();
    }, []);

    return statusChkIn == false ? (
        <CheckInPage
            navigation={navigation}
            callBackData={callBackData}
            dataUser={dataUser}
            currentPosition={currentPosition}
            handleClickLogout={handleClickLogout}
            chkStatusChkIn={chkStatusChkIn}
            statusCheck={true}
            txtBtn={'เข้างาน'}
            resetChkBtn={true}
        />
    ) : (
        <CheckInPage
            navigation={navigation}
            callBackData={callBackData}
            dataUser={dataUser}
            currentPosition={currentPosition}
            handleClickLogout={handleClickLogout}
            chkStatusChkIn={chkStatusChkIn}
            statusCheck={false}
            txtBtn={'ออกงาน'}
            resetChkBtn={true}
        />
    );
}
// =======================================================================
// CheckInDataScreen Page Navigation
// =======================================================================

function CheckInDataScreen({ navigation, callBackData, currentPosition, dataUser, handleClickLogout }) {
    return(
        <CheckInDataPage
        navigation={navigation}
        callBackData={callBackData}
        dataUser={dataUser}
        currentPosition={currentPosition}
        handleClickLogout={handleClickLogout}
        />
    );
}

// =======================================================================
// Render Home Page
// =======================================================================

export default function CheckIn({ callBackData, currentPosition, dataUser, handleClickLogout }) {
    return (
        <View style={style.viewFlex}>
            <View style={style.viewFlex}>
                <Tab.Navigator
                    // initialRouteName={callBackData.params ? callBackData.params.page : "CheckInIndex" }
                >
                    <Tab.Screen
                        name="CheckInIndex"
                        options={{
                            header: (props) => (null),
                            tabBarLabel: 'เข้างาน/ออกงาน',
                        }}
                    >
                        {(props) => <CheckInScreen navigation={props.navigation} callBackData={callBackData} dataUser={dataUser} currentPosition={currentPosition} handleClickLogout={handleClickLogout} />}
                    </Tab.Screen>
                    <Tab.Screen
                        // initialRouteName='CheckInData'
                        name="CheckInData"
                        options={{
                            header: (props) => (null),
                            tabBarLabel: 'ข้อมูล เข้า/ออก งาน',
                        }}
                    >
                        {(props) => <CheckInDataScreen navigation={props.navigation} callBackData={callBackData} dataUser={dataUser} currentPosition={currentPosition} handleClickLogout={handleClickLogout} />}
                    </Tab.Screen>
                </Tab.Navigator>
            </View>
        </View>

    );
};