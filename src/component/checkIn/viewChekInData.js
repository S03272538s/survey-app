// =======================================================================
// Init/State Componant
// =======================================================================

import React, { useState, useEffect } from 'react';
import { ScrollView, Text} from 'react-native';
import { DataTable } from 'react-native-paper';
import moment from 'moment';
import { StyleContain } from '../style/funcStyle';
const style = StyleContain();

// =======================================================================
// Repository Data
// =======================================================================

import { getData } from '../../repository/funcGetData';

// =======================================================================
// ShowDataChkInScreen Page Navigation
// =======================================================================

export default function DataChkInPage({ navigation, dataUser }) {
    const dateNow = moment(new Date).format('YYYY-MM-DD');
    const [dataShow, setDateShow] = useState();
    const getChekInLog = async () => {
        let arrDate = [];
        const attendUserData = await getData('getAttendUserData', dataUser.id_application);
        attendUserData.forEach((dataUserRow) => {
            arrDate.push({
                date: dataUserRow.create_date,
                chkInFrist: moment(dataUserRow.chkInFrist).utc().format('HH:mm'),
                chkOutLast: dataUserRow.chkOutLast === '0' ? '0' : moment(dataUserRow.chkOutLast).utc().format('HH:mm')
            });
        });
        setDateShow(arrDate);
    }

    useEffect(() => {
        getChekInLog();
    }, [])

    const onHandlerTouchRow = (data) => {
        navigation.navigate('DetailDataChkIn',{ dataRow:data })
    }
    return (
        <ScrollView style={style.ScrollViewStyle}>
            <DataTable style={style.viewFlex}>
                <DataTable.Header
                    style={{ height: 60, fontWeight: 'bold' }}
                >
                    <DataTable.Title >วันที่</DataTable.Title>
                    <DataTable.Title style={{ fontWeight: 'bold' }}>เข้างานครั้งแรก </DataTable.Title>
                    <DataTable.Title style={{ fontWeight: 'bold' }}>ออกงานล่าสุด </DataTable.Title>
                </DataTable.Header>
                {dataShow ?
                    dataShow.map((data, index) => (
                        <DataTable.Row
                            style={{ borderBottomWidth: 1, borderBottomColor: '#4c669f', height: 60 }}
                            key={index}
                            onPress={() => { onHandlerTouchRow(data) }}
                            onLongPress={() => { console.log('Hold Row'); }}
                        >
                            <DataTable.Cell >
                                {data.date === dateNow ?
                                    <Text style={style.txtAlphaDataTable}>วันนี้</Text>
                                    :
                                    data.date
                                }
                            </DataTable.Cell>
                            <DataTable.Cell >
                                {data.chkInFrist} น.
                            </DataTable.Cell>
                            <DataTable.Cell >
                                {data.chkOutLast === '0' ?
                                    <Text style={style.txtAlphaDataTable}>ไม่มีข้อมูล</Text>
                                    :
                                    data.chkOutLast + 'น.'
                                }
                            </DataTable.Cell>
                            {/* <DataTable.Cell numeric>{data.data_phone}</DataTable.Cell>  */}
                        </DataTable.Row>

                    ))
                    :
                    <DataTable.Row>
                        <DataTable.Cell>No Data</DataTable.Cell>
                    </DataTable.Row>
                }
            </DataTable>
        </ScrollView>
    );
}