// =======================================================================
// Init/State Componant
// =======================================================================

import { getDistance, orderByDistance } from 'geolib';

// =======================================================================
// Repository Data
// =======================================================================

import { getData } from '../../repository/funcGetData';

// =======================================================================
// function Check Shop Nearby
// =======================================================================

export async function chkLocationsCheckIn(currentPosition) {
    let arrDistanceLocate = [{
        id: 0,
        cmpName: 'บันทึกเวลานอกสถานที่',
        cmpAbbreviation: 'Work Out/Work From Home',
        distance: 0,
        checkStatus: false
    }];
    const dataApi = await getData('locationsCompany', '');
    dataApi.forEach((element) => {
        let multiDistnce = getDistance(currentPosition,
            {
                latitude: element.latitude,
                longitude: element.longitude
            });
        if (multiDistnce <= 100) {
            arrDistanceLocate.push({
                id: element.id_pri,
                cmpName: element.cmp_name,
                cmpAbbreviation: element.cmp_abbreviation,
                distance: multiDistnce,
                checkStatus: false
            })
        }
    });
    if (Array.isArray(arrDistanceLocate) && arrDistanceLocate.length){        
        return arrDistanceLocate;
    }
}