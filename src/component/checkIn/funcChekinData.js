// =======================================================================
// Init/State Componant
// =======================================================================

import React from 'react';
import { View } from 'react-native';
import DetailChkInPage from './viewDetailChekIn';
import DataChkInPage from './viewChekInData';
import { StyleContain } from '../style/funcStyle';
const style = StyleContain();

// =======================================================================
// Navigations
// =======================================================================

import { createStackNavigator } from '@react-navigation/stack';
const Stack = createStackNavigator();

// =======================================================================
// CheckInScreen Page Navigation
// =======================================================================

export default function CheckInDataPage({ navigation, callBackData, currentPosition, dataUser, handleClickLogout, chkStatusChkIn, statusCheck, txtBtn, resetChkBtn }) {
    return (
        <View style={style.viewInputStyle}>
            <Stack.Navigator
            >
                <Stack.Screen
                    name="ShowDataChkIn"
                    options={{ header: (props) => (null) }}
                >
                    {(props) =>
                        <DataChkInPage
                            navigation={props.navigation}
                            dataUser={dataUser}
                            dataProp={props}
                        />
                    }
                </Stack.Screen>
                <Stack.Screen
                    name="DetailDataChkIn"
                    options={{ header: (props) => (null) }}
                >
                    {(props) =>
                        <DetailChkInPage
                            navigation={props.navigation}
                            dataUser={dataUser}
                            dataprop={props.route.params}
                        />
                    }
                </Stack.Screen>

            </Stack.Navigator>
        </View>
    );
}