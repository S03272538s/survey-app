export default class Ajax {
    constructor() {
        this.url = '';
    }
    getUrl() {
        return this.url;
    }
    get(mode, id = '', r = '') {
        fetch(this.url + `${mode}/${id}`)
            .then(response => response.json())
            .then(data => {
                r(data);
            })
            .catch(err => {
                console.log(err);
                r(false);
            })
    }
    post(){
        const obj = {
            'user': 'Suntorn.d',
            'pass': '123456'
        }
        fetch(this.url, {
            method: 'POST',
            body: JSON.stringify(obj)
        })
            .then(response => response.json())
            .then(data => {
                sessionStorage.setItem('token', data.token);
            })
            .catch(err => {
                alert(err)
            })
    }
    exec() {
    }
    set(data, mode, id = '', r = '') {
        fetch(this.url + `${mode}/${id}`, {
            method: 'POST',
            body: JSON.stringify(data)
        })
            .then(response => response.text())
            .then(data => {
                console.log(data);
                r(data)
            })
            .catch(err => {
                alert(err)
            })
    }
    postLogin(data, mode, id = '', r = '') {
        console.log(this.url + `${mode}/${id}`);
        fetch(this.url + `${mode}/${id}`, {
            method: 'POST',
            body: JSON.stringify(data)
        })
            .then(response => response.text())
            .then(data => {
                
                console.log('dataLogin');
                console.log(data);
                 var output = JSON.parse(data);
                 r(output);
            })
            .catch(err => {
                console.log('Error : '+err);
            })
    }
    delete(data, mode, id = '', r = '') {
        fetch(this.url + `${mode}/${id}`, {
            method: 'POST',
            body: JSON.stringify(data)
        })
            .then(response => response.text())
            .then(data => {
                r(data)
            })
            .catch(err => {
                alert(err)
            })
    }
    fileUpload(datas, mode, id = '', r = '') {
        const data = new FormData();
        datas.forEach((item,index) => {
           data.append('fileName[]',JSON.stringify(item.name+'||'+index));
           data.append('fileToUpload[]',{
                uri: item.uri,
                type: item.type,
                name: item.name
            }) ;
        });
        fetch(this.url + `${mode}/${id}`,{
            method: 'POST',
            headers:{
                "Content-Type": "multipart/form-data",
                "otherHeader": "foo",
                },
              body: data,
        }).then(response => response.text()
        ).then(success => {
            console.log('TEST');
            console.log(success);
            r(success)
        }
        ).catch(
            error => console.log(error)
        );
    }
    setPassword(data, mode, id = '', r = '') {
        console.log('setPassword');
        console.log(data);
        // fetch(this.url + `${mode}/${id}`, {
        //     method: 'POST',
        //     body: JSON.stringify(data)
        // })
        //     .then(response => response.text())
        //     .then(data => {
        //         r(data)
        //     })
        //     .catch(err => {
        //         alert(err)
        //     })
    }
}

